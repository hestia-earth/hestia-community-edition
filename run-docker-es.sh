#!/bin/sh

set -e

docker build --progress=plain \
  -t hestia-ce-es:latest \
  -f es/Dockerfile \
  .

docker run --rm \
  --name hestia-ce-es \
  -p 9200:9200 \
  hestia-ce-es:latest
