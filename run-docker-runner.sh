#!/bin/sh

set -e

NAME_ID=${1:-"1"}

docker build --progress=plain \
  -t hestia-ce-runner:latest \
  -f api/runner/Dockerfile \
  .

docker run --rm \
  --name "hestia-ce-runner-${NAME_ID}" \
  --env AMQP_URL=amqp://guest:guest@host.docker.internal:5672/ \
  --env DATA_FOLDER=/app/data \
  --env EARTH_ENGINE_KEY_FILE=/app/ee/credentials.json \
  --env ECOINVENT_V3_FILEPATH=/app/data/ecoinventV3_excerpt.csv \
  -v ${PWD}/data:/app/data \
  -v ${PWD}/ee:/app/ee \
  hestia-ce-runner:latest
