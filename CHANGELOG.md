# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [4.45.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.44.0...v4.45.0) (2025-03-04)


### Features

* **requirements:** update models to `0.67.0` ([0051d34](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0051d346da9294db269e57b0342311c47ccf4f5a))

## [4.44.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.43.0...v4.44.0) (2025-02-18)


### Features

* **requirements:** update models to `0.66.0` ([4707ce9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4707ce9337896a9e3c034f21aeab4dfe9f05d59b))

## [4.43.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.42.0...v4.43.0) (2025-02-05)


### Features

* **requirements:** update models to `0.65.11` and validation to `0.32.20` ([07adfee](https://gitlab.com/hestia-earth/hestia-community-edition/commit/07adfee72863b84f80fe65360d8e44e699c90daf))
* **requirements:** update models to `0.65.7` ([85e98e0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/85e98e042ecd26a195c658c76dac5bb47a369259))
* **requirements:** update models to `0.65.9` ([78d43b7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/78d43b7a6e8b000fe1af1577b6f78ba2b93a69c5))


### Bug Fixes

* **drafts:** fix validate button disabled after validation complete ([829620d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/829620d933abc57049658c8541d5486e22edd837)), closes [#125](https://gitlab.com/hestia-earth/hestia-community-edition/issues/125)
* **new-version:** hide modal when versions not fetched yet ([0c49b96](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0c49b96f7ed704b9fc91026176a5b99b99d5f2e4)), closes [#124](https://gitlab.com/hestia-earth/hestia-community-edition/issues/124)

## [4.42.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.41.0...v4.42.0) (2025-01-21)


### Features

* **requirements:** update models to `0.65.6` ([20ee36b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/20ee36b9a9c551a0acdf353eaacfa7f6737177f2))
* **requirements:** update validation to `0.32.18` ([c9fed83](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c9fed835ed8a32fc927c8fc73768ae787db7a541))


### Bug Fixes

* **drafts-edit:** fix missing loader ([dc87c61](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dc87c61e7c8779437e94b5921784cdc464968e1c))
* **runner:** set default environment variables for validation ([5101ab6](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5101ab60796d7e4bce40d0005b52d40eac68cf87))

## [4.41.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.40.0...v4.41.0) (2025-01-07)


### Features

* **node-show-page:** review page style to match main HESTIA platform ([5618f57](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5618f570212f7325fb17da41521b94a91eeee5df))
* **requirements:** update models to `0.65.4` ([57d6187](https://gitlab.com/hestia-earth/hestia-community-edition/commit/57d6187ad653f40909cc7c5f6dab3cbd9f91a400))

## [4.40.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.39.0...v4.40.0) (2024-12-24)


### Features

* **drafts:** run validation asynchronously ([ae98678](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ae98678ac4dff9b296a277f05327b1aa3deeb96c))
* **requirements:** update models to `0.65.0` and validation to `0.32.16` ([dec50c9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dec50c98f874178633b9d6364a8b72bc9979b68f))

## [4.39.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.38.4...v4.39.0) (2024-12-10)


### Features

* **requirements:** update models to `0.64.13` ([1f2f405](https://gitlab.com/hestia-earth/hestia-community-edition/commit/1f2f405bb4bea8646974ec23bf48d246ea4daacd))

### [4.38.4](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.38.3...v4.38.4) (2024-12-03)


### Bug Fixes

* fix error saving int64 as JSON ([7ae98ce](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7ae98ce6a1672639e869df1d2335cbd58bac064a))
* **mocking:** mock `related_cycles` function to find nodes in local folder ([4fc47d1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4fc47d16abfdd65043c6acc9820ac54153034bb2))

### [4.38.3](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.38.2...v4.38.3) (2024-12-02)


### Bug Fixes

* **calculate:** fix incorrect response types ([876dc24](https://gitlab.com/hestia-earth/hestia-community-edition/commit/876dc24af00169bcef9eb4089f5b8f87e7bbd5bc))

### [4.38.2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.38.1...v4.38.2) (2024-12-02)


### Bug Fixes

* **drafts:** fix error copy source file to missing folder ([e335a52](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e335a521d66903af19be6e1565061d163df60bfe))
* **results:** fix error displayed on the wrong node after page 1 ([318031e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/318031e633542e2c5f51b957b648595ac1ea10d7)), closes [#111](https://gitlab.com/hestia-earth/hestia-community-edition/issues/111)

### [4.38.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.38.0...v4.38.1) (2024-11-27)


### Bug Fixes

* **api:** fix calculation issues with Indicator ([0840e5e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0840e5e0b7509533eaba58c8375f2bc536a202b9))

## [4.38.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.37.0...v4.38.0) (2024-11-26)


### Features

* **api:** add method to validate a draft ([82adfbf](https://gitlab.com/hestia-earth/hestia-community-edition/commit/82adfbf3216675107b2c44a50c63670daccb642b))
* **drafts:** add calculate method using stored nodes ([408cc92](https://gitlab.com/hestia-earth/hestia-community-edition/commit/408cc92abc4855f7c85bbda0e239720e371b61db))
* **drafts:** calculate async ([7835cfa](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7835cfaeb6887b75fb4bddf5d6c63db23f544249))
* **requirements:** update models to `0.64.10` ([6ee67d6](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6ee67d6799140f7bf598b7630f7cc6646a903f34))
* **requirements:** update validation to `0.32.10` ([9c341fe](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9c341fe9ae587bd4fc848664b2aebae26fcd21fb))
* **results-new:** update draft content before calculating ([b471c72](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b471c721cf2dc42e81a26f19b0f1a5443f0230e2))


### Bug Fixes

* **drafts:** fix function name ([3a93603](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3a936039d44480545200e6564ae8128c6ff56b9a))

## [4.37.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.36.0...v4.37.0) (2024-11-19)


### Features

* **requirements:** update validation to `0.32.9` ([ff2c35b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ff2c35baece8b4c3c1f499c6598db5013b2564d3))
* **results-new-form:** toggle to show only nodes in error ([c29a027](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c29a027c19a2df7b5b771063a9e17e62c12961c2))


### Bug Fixes

* **files-new-form:** fix schema errors only shown on first node ([4353265](https://gitlab.com/hestia-earth/hestia-community-edition/commit/435326538173b8a27df35aeabae12568a0d9b945)), closes [#110](https://gitlab.com/hestia-earth/hestia-community-edition/issues/110)

## [4.36.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.35.0...v4.36.0) (2024-11-12)


### Features

* **requirements:** udpate models to `0.64.9` ([ff730d8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ff730d8ff74cad8dbfd0bf0b5624fbf708f35f8b))
* **requirements:** update validation to `0.32.8` ([ece49e5](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ece49e5e994970ff8c62ce6d995e78de6296e614))
* **ui:** update schema to `30.3.2` ([e75c45a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e75c45a04ad5a9243fcb8170798d9676fd0a9dfe))


### Bug Fixes

* **app:** disable geospatial validation using Earth Engine ([5aa18b8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5aa18b8306bc87375e28f1670b3ede9a1b33ab35)), closes [#108](https://gitlab.com/hestia-earth/hestia-community-edition/issues/108)
* **results-new:** fix migration error displayed with no matching migration ([3c75dc9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3c75dc9ec35fba1b4d4f561d491619e6d99c716a))
* **results:** find terms by ID first if provided ([81f6395](https://gitlab.com/hestia-earth/hestia-community-edition/commit/81f6395c047b229486f5be29c9fc7fd32da32412))

## [4.35.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.34.2...v4.35.0) (2024-10-29)


### Features

* **api:** run calculations by stage ([2bc6e9b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2bc6e9bcb44ff97d182405b1da09ecaea57b377e))
* **files-new:** calculate nodes one by one when no dependencies ([f8ab771](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f8ab771bc71840cc44ad43729e97bbdce2281dcf))
* **requirements:** update models to `0.64.8` ([0fd108b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0fd108b221248620f9167872edbc5a6dc0aaac3c))
* **requirements:** update validation to `0.32.7` ([805c5ab](https://gitlab.com/hestia-earth/hestia-community-edition/commit/805c5abeb896324acb1709722b58fd284cdf0697))


### Bug Fixes

* **ui:** fix modals no backdrop ([ba39574](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ba39574a7d1c76cbfceee80c6776a3b2bcfdb6e3))

### [4.34.2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.34.1...v4.34.2) (2024-10-18)


### Bug Fixes

* **api:** fix logging errors ([7c228bc](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7c228bcf74ec98fcacaaa2ae7d80b2f0e40570d5))

### [4.34.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.34.0...v4.34.1) (2024-10-15)

## [4.34.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.33.0...v4.34.0) (2024-10-15)


### Features

* **requirements:** update models to `0.64.7` ([9367a30](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9367a303c46d87b3c8dc183714391b5c91b41ff0))
* **requirements:** update validation to `0.32.5` ([e25ba59](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e25ba598646c52422ac8927e3f2d1c78c214cde6))


### Bug Fixes

* **results-upload:** display conversion to HESTIA format errors ([df03417](https://gitlab.com/hestia-earth/hestia-community-edition/commit/df03417ac4dec877def3fb767ce3e630e4b17952))
* **results-upload:** use local schema files instead of fetching latest ([754a472](https://gitlab.com/hestia-earth/hestia-community-edition/commit/754a472684d826e965ddfae191ead9bdcdff823b))

## [4.33.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.32.0...v4.33.0) (2024-10-03)


### Features

* **requirements:** update validation to `0.32.3` ([ac8345b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ac8345b7044cb12153e49b2dee7c9e31c3a372ef))

## [4.32.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.31.0...v4.32.0) (2024-10-01)


### Features

* **api:** add debug logging when fetching nodes ([21ed32c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/21ed32cadebe4cfcba5b8bd49403636b7d4cba26))
* **api:** add logging ([f0c5fc9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f0c5fc9ba4154aecb211511786ed48517e27304f))
* **requirements:** update models to `0.64.4` ([66ccb92](https://gitlab.com/hestia-earth/hestia-community-edition/commit/66ccb92685eec77a12c80b51b874544ac9694f1d))

## [4.31.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.30.0...v4.31.0) (2024-09-17)


### Features

* **requirements:** update models to `0.64.2` ([16d0efa](https://gitlab.com/hestia-earth/hestia-community-edition/commit/16d0efadd97e055b74a7d031931b4308ddb84a38))


### Bug Fixes

* **ee:** fix incorrect parameters to run coordinates ([f723cf4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f723cf4ed56bd285b5b8b76fa3a998339947e32c))

## [4.30.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.29.0...v4.30.0) (2024-09-03)


### Features

* **requirements:** update models to `0.64.0` ([63c807a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/63c807a81f67e980ea33ec82670341992b6e18b6))

## [4.29.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.28.0...v4.29.0) (2024-08-20)


### Features

* **requirements:** update models to `0.62.5` ([3d0ef2e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3d0ef2e52bb3074f7c341ed4f345e361e921108c))

## [4.28.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.27.0...v4.28.0) (2024-08-06)


### Features

* **requirements:** update models to `0.62.1` ([3c13fe2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3c13fe2ac5bebb8a45f5ea0650851220cea2001c))
* **requirements:** update validation to `0.31.1` ([1927d94](https://gitlab.com/hestia-earth/hestia-community-edition/commit/1927d949029c86b00de49e337ab825809b2d45dd))
* **ui:** update to angular 17 and schema 29 ([a37253c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a37253cdf00d400d6a5292720e90f62afb9e70f3))

## [4.27.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.26.0...v4.27.0) (2024-07-23)


### Features

* **requirements:** update models to `0.62.0` ([3365736](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3365736a2889e742fc77194d18d12a9c5caf17a2))

## [4.26.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.25.0...v4.26.0) (2024-07-09)


### Features

* **requirements:** update models to `0.61.8` ([cc039b1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/cc039b1f9bfd1535c4721e1ba764ac68b2f56451))

## [4.25.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.24.0...v4.25.0) (2024-06-27)


### Features

* **requirements:** update models to `0.61.6` ([8c75f60](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8c75f60e3ea93a719f16f27dea70a65b7d267a92))

## [4.24.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.23.0...v4.24.0) (2024-06-11)


### Features

* **requirements:** update models to `0.60.1` and orchestration to `0.6.13` ([4199c25](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4199c2534fe7ec9fa3f1e1adb5c2a9c503be0897))
* **requirements:** update validation to `0.29.8` ([49c1766](https://gitlab.com/hestia-earth/hestia-community-edition/commit/49c17662d1174eb0d05ca6421f8cd44347afa26a))

## [4.23.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.22.0...v4.23.0) (2024-05-28)


### Features

* **requirements:** update models to `0.59.7` ([26e89ab](https://gitlab.com/hestia-earth/hestia-community-edition/commit/26e89abc72444356c8581ca95f9e3e0997f8010c))

## [4.22.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.21.0...v4.22.0) (2024-05-14)


### Features

* **requirements:** use models version `0.59.6` ([c83311a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c83311ab32f31beed05d26763c93b2aa54d016f1))

## [4.21.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.20.0...v4.21.0) (2024-04-30)


### Features

* **requirements:** update schema to `27.2.0` and models to `0.59.5` ([e2f9e52](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e2f9e52f92d07622c1a781aee22d91ec2c2c5beb))

## [4.20.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.19.0...v4.20.0) (2024-04-16)


### Features

* **requirements:** use models version `0.59.3` ([d92de23](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d92de2365e354b24db40bf7ae53e2d77449720df))

## [4.19.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.18.0...v4.19.0) (2024-04-02)


### Features

* **requirements:** use models version `0.59.0` ([cf9b057](https://gitlab.com/hestia-earth/hestia-community-edition/commit/cf9b05701b131a418a94e81648ac964e9572b1ea))
* **requirements:** use validation version `0.29.4` ([6680249](https://gitlab.com/hestia-earth/hestia-community-edition/commit/66802498d60aff56bd1c7b7d6c5ec36c54569d77))
* **ui:** update style following latest Hestia version ([2aeaa07](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2aeaa074ac7b147ae010fdef7df74daea7d9394c))

## [4.18.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.17.0...v4.18.0) (2024-03-19)


### Features

* **requirements:** update models to `0.58.0` ([f32c5bb](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f32c5bb02b3d62baf0f4cddbf453fe9a560b7687))


### Bug Fixes

* **app:** fix path when base path is empty ([8a9ada0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8a9ada0cdeddaa94d69aaac17fb477a96d6201ce))

## [4.17.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.16.0...v4.17.0) (2024-03-05)


### Features

* **requirements:** update models to `0.57.2` ([9e381c3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9e381c38fd78b0f7c6fdb1c2790983a846e3ed45))

## [4.16.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.15.0...v4.16.0) (2024-02-20)


### Features

* **requirements:** update models to `0.57.1` ([6c253b1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6c253b192e616827c3c5add9938217e7b14df46c))

## [4.15.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.14.0...v4.15.0) (2024-02-06)


### Features

* **requirements:** update models to `0.56.2` ([5df4bd2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5df4bd226cf07f9fed2958adcc8c7e0212e615f8))

## [4.14.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.13.0...v4.14.0) (2024-01-23)


### Features

* **requirements:** update models to `0.56.1` ([874bd27](https://gitlab.com/hestia-earth/hestia-community-edition/commit/874bd279e31758674201a4cd9ce7c8b61d78c0ab))

## [4.13.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.12.0...v4.13.0) (2024-01-10)


### Features

* **requirements:** update models to `0.56.0` ([c6d59b7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c6d59b795040005bec1e4b0620c45c2420b3b2bf))

## [4.12.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.11.0...v4.12.0) (2023-12-12)


### Features

* **navbar:** update links to open main hestia website ([4591364](https://gitlab.com/hestia-earth/hestia-community-edition/commit/45913649d9e6b9d9d46fb72df66d0a5cc539b1f2))
* **requirements:** update models to `0.54.3` ([e787f63](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e787f63c156f3333a09aaada771dec7e89c513f6))

## [4.11.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.10.0...v4.11.0) (2023-12-06)


### Features

* **requirements:** update models to `0.54.2` and schema to `24.3.0` ([67c5da5](https://gitlab.com/hestia-earth/hestia-community-edition/commit/67c5da50862b796dfd2739330865cb919864e6b9))

## [4.10.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.9.0...v4.10.0) (2023-11-21)


### Features

* **requirements:** update models to `0.54.0` ([62b5517](https://gitlab.com/hestia-earth/hestia-community-edition/commit/62b5517dae56533ba2e0127470069705a33372cf))

## [4.9.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.8.0...v4.9.0) (2023-11-07)


### Features

* **requirements:** update models to `0.53.11` ([ce4317c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ce4317c5a21622bde75ea7fbcc7d993e18bdd248))
* **requirements:** update schema to `23.7.0` ([c05ab17](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c05ab1729ec1a2b2f158c74e67e32bbe66852cee))
* **requirements:** update validation to `0.25.2` ([deb8e2c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/deb8e2cc745f1df9af68424f02d7338798eb4c00))

## [4.8.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.7.0...v4.8.0) (2023-10-24)


### Features

* **requirements:** update data validation to `0.25.0` ([d405dc6](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d405dc65c41528ecfdb2726544fa22f502fd6d65))
* **requirements:** update models to `0.53.1` ([2a71be4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2a71be460822d2c7dcef41d4f9e2d22bdc7eb070))
* **requirements:** update models to `0.53.6` ([48d1795](https://gitlab.com/hestia-earth/hestia-community-edition/commit/48d1795a7165da2621d7b150582aba533402fe3d))
* **requirements:** update schema to `23.6.0` ([b80554c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b80554c7da1ee899bf276a941f05f4e63f3bc176))

## [4.7.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.6.0...v4.7.0) (2023-10-11)


### Features

* **requirements:** update models to `0.52.0` and data-validaiton to `0.24.0` ([e7289e7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e7289e7967e4fd59a5efb4f03f9b48126e67733d))
* **requirements:** use schema `23.4.0` ([9e4e34b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9e4e34bf1bc8f2083ac64991dcc5b6b81125178d))
* **results:** handle upload JSON in Hestia format ([7a7581d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7a7581d29942e575717759d1f9667c7c851406e2))


### Bug Fixes

* **api:** fix error on indexing new data ([b7aab0a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b7aab0a439ce955a2dcba1f82ff5198802e35605))
* **ui:** fix error in navigation ([df1a7f0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/df1a7f0bc6a5bda16cfd01b994ffb79e15a76176))

## [4.6.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.5.0...v4.6.0) (2023-09-26)


### Features

* **requirements:** update models to `0.51.1` ([727e88f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/727e88f5ff5bd89af1b40cdb28a36b7bc995ea03))

## [4.5.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.4.0...v4.5.0) (2023-09-05)


### Features

* **requirements:** update validation to `0.23.1` ([0ecccd4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0ecccd44e7f1c267a809f81f1d928ea17c98bc31))
* **requirements:** use models version `0.50.0` ([daa5cd0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/daa5cd072c027399aaf162e46d7a755a27724ef3))
* use schema version `22.4.0` ([8332309](https://gitlab.com/hestia-earth/hestia-community-edition/commit/833230989f8458b990c1cb335331064534e26c64))

## [4.4.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.3.0...v4.4.0) (2023-08-15)


### Features

* **requirements:** update models to `0.48.0` and validation to `0.23.0` ([35fa3f9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/35fa3f9d8a33af81c5b6ca011108dc0997bb28ce))
* **requirements:** use models version `0.49.0` ([7883c66](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7883c66d52764c58e220b30bb8cfa120fa7e6495))
* **ui:** use schema version `22.3.0` ([e52bd2d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e52bd2dcc6ce6b60b3905e2431910fefdeba13c8))

## [4.3.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.2.0...v4.3.0) (2023-08-01)


### Features

* **requirements:** update models to `0.47.5` ([5f26f99](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5f26f994a381c4f9e6852898bd35762c37e873d5))
* **ui:** update poore nemecek converter to `0.6.1` ([b360d16](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b360d16083fab191b25faac877805735ba63732d))

## [4.2.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.1.1...v4.2.0) (2023-07-18)


### Features

* **app:** add page to query the "Glossary of Terms" ([4b1fbe2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4b1fbe2618efbdcb988e087cef47d3e18f0abad5))
* **glossary:** add link to migrations page ([1cc7022](https://gitlab.com/hestia-earth/hestia-community-edition/commit/1cc70228c552f482e3c1221d3582fcc28616d4e6))
* **requirements:** update models to `0.47.3` ([ac6434a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ac6434a6a6595794bcc85c795460657d56c654b6))
* **requirements:** update validation to `0.22.7` ([dc59552](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dc5955218fbb8fb2161157f529362442aa9fc69d))
* **results upload:** display current schema version and instructions ([fd2d086](https://gitlab.com/hestia-earth/hestia-community-edition/commit/fd2d086b30eacdb07b4aa5f678aefc6d2efb59d2))
* **results:** detect missing Terms and migrations in Wizard ([4798eda](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4798eda8cd4e3b2b5a4c62d2ce86b2dab3db3ba8))
* **results:** set nodes as private on upload ([0c6ef0a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0c6ef0a0801cfcc54ab987dedb9a5524b35b8dfa))
* **ui:** update schema to `22.0.0` ([03d092f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/03d092f4bff13f667c08fb6083dfefaa933dcdad))

### [4.1.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.1.0...v4.1.1) (2023-07-11)


### Bug Fixes

* **api:** fix recalculate synchronously multiple nodes ([f954663](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f9546636182b5a8f32794705632639023f1da913)), closes [#96](https://gitlab.com/hestia-earth/hestia-community-edition/issues/96)

## [4.1.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v4.0.0...v4.1.0) (2023-07-04)


### Features

* **requirements:** update models to `0.47.2` and validation to `0.22.6` ([9ecd5eb](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9ecd5eb8229aa85c6d87ceceb8f7f1737b271b57))
* **results details:** add `Animals` for Cycle ([f4e4649](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f4e4649a9db3bd609f3f7c24ee311514b94b3d39))
* **results edit:** add button to download Nodes as JSON ([dd9e6c7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dd9e6c73cb094ce226396584e70f3c89c591ce63))


### Bug Fixes

* **api migrations:** fix error no draft folder ([ada14b1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ada14b141ea68b9e9ec1ada92b0897d6a1db85a8))

## [4.0.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v3.3.0...v4.0.0) (2023-06-20)


### ⚠ BREAKING CHANGES

* min schema version is `21.2.0`
* **api:** Calculations are now stored by version.
Please make sure to keep a copy of your data directory to revert to older version.

### Features

* **api migrations:** add migration to fix nodes with incorrect @type/[@id](https://gitlab.com/id) ([d20b509](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d20b509442ef92eac2477c456e51d7cc58a8603f))
* **api:** store calculations per version ([202b65b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/202b65b70a72abf4f6cde75eee304a7f3fbc91d7))
* **requirements:** update models to `0.45.0` ([54c12d8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/54c12d8ae645a81cad365200d359f42dd05e6556))
* **requirements:** update models to `0.47.1` and validation to `0.22.3` ([4207f85](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4207f85abeb89b7210413572fecd4f67f7e8fd20))
* **requirements:** use models version `0.47.0` ([e479471](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e47947160c0a8e771c613c9ed8d8dbc52b49f55d))
* **results details:** include `inputs production` values ([073d9de](https://gitlab.com/hestia-earth/hestia-community-edition/commit/073d9de971758e05f152f825d0d74797781b1488)), closes [#81](https://gitlab.com/hestia-earth/hestia-community-edition/issues/81)
* **results edit:** add submit feedback button ([9aed5d0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9aed5d0c2f1d23357b1f7e6013e9cfeebd525967)), closes [#86](https://gitlab.com/hestia-earth/hestia-community-edition/issues/86)
* **results:** allow sorting on `createdOn` and `id` only ([5135502](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5135502de66cd852bc58f44a8a2be0a23adb93c7)), closes [#84](https://gitlab.com/hestia-earth/hestia-community-edition/issues/84)
* **results:** compare multiple versions at once ([f3020d8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f3020d8e019a97c35352e49bb808a71fdc3cc212)), closes [#74](https://gitlab.com/hestia-earth/hestia-community-edition/issues/74)
* **results:** improve display on mobile ([f662c82](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f662c82b5b15ca3e9b5d6a7033908b6e20e90baa))
* **ui results:** switch between available versions ([420d72a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/420d72acb5289f2cf1998e66f6060a9bfa36b374))
* update schema to `21.2.0` ([acdfd39](https://gitlab.com/hestia-earth/hestia-community-edition/commit/acdfd3987ef398f533af3d3a2846761530aa8100))
* **upload:** select Excel sheet when converting to CSV ([6f2895d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6f2895d57a775aa4e04b4c67a86df88bb25a71f4))


### Bug Fixes

* **calculate:** load latest node version before calculating ([dd3b347](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dd3b3472f215198d3e451c3e8040940865dcd75a))
* **results details:** fix error when switching tabs ([285bd06](https://gitlab.com/hestia-earth/hestia-community-edition/commit/285bd063d10265dff62789345656b9e9137256c2))
* **results upload:** update supported extensions ([db3c625](https://gitlab.com/hestia-earth/hestia-community-edition/commit/db3c6252eb2b24b29219bf7c02e4cc19faf7a09e))
* **results:** handle on linked Cycle or Site ([f975bf5](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f975bf5d405c68698431eeb73a4f48d131ab5f7e)), closes [#85](https://gitlab.com/hestia-earth/hestia-community-edition/issues/85)
* **results:** remove dependencies that are not included in upload ([aee020f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/aee020fdaa9f041bedd1fe6d133e0ea683739c6f))
* **utils:** make sure node folder exists ([0c6df4c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0c6df4cd978d9dfa11ede36732c47c1d0bc03aa3)), closes [#77](https://gitlab.com/hestia-earth/hestia-community-edition/issues/77)

## [3.3.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v3.2.0...v3.3.0) (2023-05-23)


### Features

* **requirements:** update orchestrator to `0.5.3` ([0298f73](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0298f73d739ea8bd31f40ddff35c5be93edfa6d3))
* **runner:** create runner connected with RabbitMQ ([ee9ab5e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ee9ab5e31da6388b8b97ee64daf749d60f48198a))
* **settings runners:** default sort by `active` ([dcee294](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dcee2945a44655eb3ce6c57f7239641bf82e3d1d))

## [3.2.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v3.1.0...v3.2.0) (2023-05-11)


### Features

* **api:** handle run calculations without queuing system ([329f1d8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/329f1d8e43bd52516683ba17968b92d1cb80e9b8))

## [3.1.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v3.0.1...v3.1.0) (2023-05-09)


### Features

* **requirements:** update models to `0.44.7` ([5adec3e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5adec3e4b1a5d3149624b25fbe51fef6367852a9))


### Bug Fixes

* **calculate:** handle calculating by a single Node ([be0f78b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/be0f78b97747f50565646b2e8efe7d9a2fa2c492))
* **results:** remove non-ascii char from generated `id` ([e949681](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e949681091d356aa816e21a07838ebe9f22757f4)), closes [#68](https://gitlab.com/hestia-earth/hestia-community-edition/issues/68)

### [3.0.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v3.0.0...v3.0.1) (2023-04-26)


### Bug Fixes

* **api migrations:** handle no config existing for Cycle ([a550fac](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a550fac8cb2da19fccb75113c47132621f31ec1a))

## [3.0.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.6.2...v3.0.0) (2023-04-26)


### ⚠ BREAKING CHANGES

* Data is now split by Node Type.
Automatic migration runs at server start to migrate old data.

### Features

* **calculate:** run calculations in a specific order in the background ([4dec4bc](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4dec4bcd1de823584846943f78b048d6dfe40a95))
* **drafts:** display version when editing ([f2cd4f3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f2cd4f3d28456849d8c145f977dfb2329700832a))
* **drafts:** store `version` used to create the Draft data ([245512e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/245512ec98810cc3df6c1c202c3a1ac469a05162))
* **requirements:** update models to `0.44.0` ([403204c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/403204c384bfca5f8971aed6365f98240c71a61c))
* **requirements:** update models to `0.44.1` ([7397c2a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7397c2a91688ae89915a8d39aa99946e702fd266))
* **requirements:** update models to `0.44.2` ([7a4f679](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7a4f679a927e7959450a5758a803fefab29f136b))
* **requirements:** update models to `0.44.6` ([986d4af](https://gitlab.com/hestia-earth/hestia-community-edition/commit/986d4af062824da17fa49d1878496e1ef87388b0))
* **requirements:** update validation to `0.20.0` ([b5c49d9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b5c49d9a519582f09de9052c5eeb1196d0afdab2))
* **requirements:** update validation to `0.20.1` ([12d44b1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/12d44b1404e01451597f19939adfc44aa36bce44))
* **requirements:** update validation to `0.20.3` ([dd36017](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dd3601768b42adcdfbe3c4e7c7717524307bd6b0))
* **results details:** add endpoint indicators ([81077de](https://gitlab.com/hestia-earth/hestia-community-edition/commit/81077deeccd688a1e52e547216d23efb20834c2a))
* **results details:** show indicator if using latest models version ([c0c7c91](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c0c7c91f17a137527b49b80b2bc987b19768e02e)), closes [#56](https://gitlab.com/hestia-earth/hestia-community-edition/issues/56)
* **results details:** switch between original/recalculated states ([4d1a088](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4d1a0886af53e6825057b4574a00993ecd1a280b))
* **results list:** add re-calculate and delete actions ([df9f12c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/df9f12c2fa5ef37bf5e9459ce1f6a2a713b3656b))
* **results:** handle errors while calculating ([4ce52d6](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4ce52d6c8782c5d6754e2b698c45884ebad81b8f))
* **results:** update upload button match Hestia platform ([7ee5a36](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7ee5a362c537fdf2767b0dff5bbc0ad888033c9d))
* **ui results:** improve report error include more details ([ebf9e8a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ebf9e8adb685aaf77393cead7bc044d0a1ea5aca))
* **ui:** show recalculation logs in cycle completeness ([ecbae34](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ecbae34830c27ead8be09111e1952360dc5fda86))


* organise data by ImpactAssessment ([0c11244](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0c112440c501734b8b7637d71bc602489a9801e4)), closes [#50](https://gitlab.com/hestia-earth/hestia-community-edition/issues/50) [#51](https://gitlab.com/hestia-earth/hestia-community-edition/issues/51) [#52](https://gitlab.com/hestia-earth/hestia-community-edition/issues/52)

### [2.6.2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.6.1...v2.6.2) (2023-03-20)


### Bug Fixes

* **ui:** update poore-nemecek converter to `0.4.3` ([8dbdb1e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8dbdb1efa9bf68a4f41d135053544792258118c2))

### [2.6.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.6.0...v2.6.1) (2023-03-17)


### Bug Fixes

* **requirements:** update orchestrator to `0.5.1` ([6c2f828](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6c2f8284ca5f9a8f1f5daace481b3bf044074be8))

## [2.6.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.5.0...v2.6.0) (2023-03-15)


### Features

* **requirements:** update `validation` to `0.17.0` ([0e62d9a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0e62d9a2ee2f1d7801829d4cf16be521f915546b))
* **requirements:** update data-validation to `0.19.0` ([e7bf764](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e7bf764c9e40709b66876f1cfa09c1bcd9da6547))
* **requirements:** update models to `0.43.0` ([f814f13](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f814f13f2068901588fb2b21d4367e04aefb2aaf))
* **requirements:** update orchestrator to `0.5.0` ([7c52c81](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7c52c811ffe6738a9ca923ecdd37463902c02fac))
* **ui:** show all pagination pages ([ec52f34](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ec52f34339c62f8ddad332dd2d0a1bbf9aef7982))


### Bug Fixes

* **results details:** fix overflow on missing lookups ([a0e3907](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a0e3907da335a9908953196fb16fe0107bd1b694)), closes [#54](https://gitlab.com/hestia-earth/hestia-community-edition/issues/54)

## [2.5.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.4.0...v2.5.0) (2023-02-28)


### Features

* **requirements:** update models to `0.42.0` ([12f7448](https://gitlab.com/hestia-earth/hestia-community-edition/commit/12f7448429d03cbf147c5647f9f6cff7fe84e154))
* **results upload:** show formatted error and instructions on `Hestia` format ([2d4d1c1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2d4d1c115f858b30d0f45e6e292c003aa18d8099)), closes [#45](https://gitlab.com/hestia-earth/hestia-community-edition/issues/45)
* **results upload:** show instructions link for `Hestia` format ([68f03ec](https://gitlab.com/hestia-earth/hestia-community-edition/commit/68f03ec3b3d6779ea17c453557d50aef580e182b))


### Bug Fixes

* **api:** disable distribution validation ([24c93e3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/24c93e3d220d04637a63c54bab215a62f161fd61))
* **results upload:** replace terms by name from `Hestia` format ([f17caab](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f17caab9ea3c1807941e00b141a0f115fe66039a)), closes [#45](https://gitlab.com/hestia-earth/hestia-community-edition/issues/45)
* **ui:** display correct UI version in settings ([b32d614](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b32d6143077a85cfe3a2cd4c370d74ff03047596))

## [2.4.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.3.0...v2.4.0) (2023-02-14)


### Features

* **requirements:** update models to `0.41.2` and validation to `0.16.1` ([dcad984](https://gitlab.com/hestia-earth/hestia-community-edition/commit/dcad984d486a8a98669c3dec540a655bad8629f8))


### Bug Fixes

* **api calculate:** fix get Node configuration ([e7a4442](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e7a4442a0c2605bcb8dd93f8509ea21c5e438264))
* **results:** update unit for ecosystems in download CSV ([2c78d3e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2c78d3e6e62dbfe0932db1eecaa79101878f6a98))

## [2.3.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.2.1...v2.3.0) (2023-01-31)


### Features

* **requirements:** update models to `0.41.0` and validation to `0.16.0` ([adab9ee](https://gitlab.com/hestia-earth/hestia-community-edition/commit/adab9ee90b45be6154a4a9a2b752df17f9c8b2ab))
* **settings:** show ui version ([14303f6](https://gitlab.com/hestia-earth/hestia-community-edition/commit/14303f6721da8f4f916499116df85568b136ab70))
* **ui recalculate:** handle error when recalculating ([4eaa5b2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4eaa5b2805d9abb1f48bca42d4fb7c4e5f0aef4a))
* **ui:** use schema `15` ([8416c07](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8416c07634461ebbf448157046b19df78aba216b))


### Bug Fixes

* **calculate:** return local config in `GET /config` endpoint ([b98b8b1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b98b8b1aca61a6194d7e63ae80f70b6e5dd892cf)), closes [#40](https://gitlab.com/hestia-earth/hestia-community-edition/issues/40)
* **results details:** use new terms for biodiversity loss ([d6c1ef7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d6c1ef7474216c9756db98607baa39523821834f))

### [2.2.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.2.0...v2.2.1) (2023-01-17)


### Bug Fixes

* **results upload:** fix error setting to private ([355d9ac](https://gitlab.com/hestia-earth/hestia-community-edition/commit/355d9ac11cc299a52af3141be7b887985809f7ce))

## [2.2.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.1.0...v2.2.0) (2023-01-17)


### Features

* add proxy to get orchestrator configuration from gitlab ([37bd724](https://gitlab.com/hestia-earth/hestia-community-edition/commit/37bd724dab89e0ce66f2e266450807b5b9f97b86))
* **requirements:** update models to `0.40.1` and orchestrator to `0.4.5` ([e817239](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e817239c0016e0b0f1a2031f4c2f2da301c65b66))
* **requirements:** update validation to `0.14.3` ([aecba31](https://gitlab.com/hestia-earth/hestia-community-edition/commit/aecba3112cada1e6e362a6c31284ce9439cb9c3d))
* **requirements:** update validation to `0.15.0` ([b98105d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b98105d41de84d6b1f5b59d23f388650ec20853d))
* **results upload:** set `dataPrivate=true` by default ([f86a037](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f86a037eeb8f8eb3aa58d45ef347acb66a8fe7fe))
* **ui:** use schema `14.4.0` ([9055899](https://gitlab.com/hestia-earth/hestia-community-edition/commit/905589949d2e8ffcf54942b7012a8897a97b13ab))


### Bug Fixes

* **ui:** add missing dependencies for color generation ([29b3069](https://gitlab.com/hestia-earth/hestia-community-edition/commit/29b30693cb6d2d9fcf5fab5cf428116bf4f7302a))

## [2.1.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v2.0.0...v2.1.0) (2022-12-13)


### Features

* **requirements:** update data validation to `0.14.0` ([1075473](https://gitlab.com/hestia-earth/hestia-community-edition/commit/10754734b2c7a3d3b0a5eef45eb8c2ea35f728fd))
* **requirements:** update models to `0.39.1` and orchestrator to `0.4.1` ([2acc5e9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2acc5e9b9a272d261e80a52a5b19d2a7dea7b731))
* **requirements:** update models to `0.39` and orchestrator to `0.4` ([20e6407](https://gitlab.com/hestia-earth/hestia-community-edition/commit/20e6407650f9a100c51525c908bd98091e90dc9b))
* **ui:** update schema to `14.2.0` and ui-components to `0.10.2` ([bd23db4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/bd23db424c0520142d80bf6ee32df3375ab15420))

## [2.0.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.27.0...v2.0.0) (2022-11-29)


### ⚠ BREAKING CHANGES

* **requirements:** `ecoinventV3` model requires a valid license.
Please read the instructions in the README if you want to use the model.

### Features

* **requirements:** update models to `0.37.0` ([97e1f01](https://gitlab.com/hestia-earth/hestia-community-edition/commit/97e1f01b252e7b1f6fa2d018082a1a8b83de03f6))
* **requirements:** update models to `0.38.0` and orchestrator to `0.3.7` ([6ecd847](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6ecd8475f8d267c30068ba6c632ba10f646e831b))
* **ui:** add Driver Chart ([80c9303](https://gitlab.com/hestia-earth/hestia-community-edition/commit/80c93036eced1fdba0999b50627cb3bc583f281d))
* **ui:** update to ui-framework version 3 ([06ae381](https://gitlab.com/hestia-earth/hestia-community-edition/commit/06ae3817eb41d3d9d1a1fc82028d93a5ad98f02c))

## [1.27.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.26.0...v1.27.0) (2022-11-15)


### Features

* **requirements:** update models to `0.36.2` ([73eedf8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/73eedf87f666b6ad93130cc99b342c3ebb6c2c6b))
* **requirements:** update validation to `0.13.0` ([65a1f73](https://gitlab.com/hestia-earth/hestia-community-edition/commit/65a1f731cc1c93819f42fb63f2791e7b7c113b42))

## [1.26.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.25.0...v1.26.0) (2022-11-08)


### Features

* **requirements:** use models version `0.36.0` and orchestrator version `0.3.6` ([d0b878f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d0b878fcb36bcc8f7d6122781de756734002d442))
* **results details:** invert Inputs and Products ([9e2d8c7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9e2d8c728adb0755784a3475b38161a87d4ff1bd))


### Bug Fixes

* **navbar:** restrict to container size as on the main Hestia website ([75d8050](https://gitlab.com/hestia-earth/hestia-community-edition/commit/75d80502644c2a7304da91e4e83ef8257a9fae22))

## [1.25.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.24.0...v1.25.0) (2022-10-18)


### Features

* **api cycles:** create Cycle using JSON ([b839540](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b8395403b10cbccace1cc64f586aae9de9911986))
* **requirements:** update models to `0.35.0` and validation to `0.12.0` ([cd2cc3c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/cd2cc3c901da5823f4605406539d62ffa3ffe5a5))
* **requirements:** use models `0.35.1` ([b30496d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b30496d4138c8edf3eb21672fc6161f096f2d5e4))

## [1.24.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.23.0...v1.24.0) (2022-10-04)


### Features

* **requirements:** use models `0.34.1` ([439ed3d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/439ed3d458e7f1473db34e347cf8d30ad2a8311f))
* **requirements:** use validation `0.11.9` ([5c6760b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5c6760b79033616a1c8a2d2088b4f72c8273b68b))

## [1.23.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.22.0...v1.23.0) (2022-09-20)


### Features

* **requirements:** use models `0.34.0` and validation `0.11.4` ([a30a199](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a30a199790b300c8bbf32fa85bcd581705083746))
* **ui:** update schema to `11` and ui-components ([89b10ca](https://gitlab.com/hestia-earth/hestia-community-edition/commit/89b10cacba34a12971aa250da17b87cf55aac886))

## [1.22.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.21.0...v1.22.0) (2022-09-06)


### Features

* **requirements:** use models `0.33.0` ([247fa93](https://gitlab.com/hestia-earth/hestia-community-edition/commit/247fa9386bc9f73dcdf2ee8aa5647548b4e285f3))
* **requirements:** use validation `0.11.3` ([2fa3351](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2fa33513ba3ef4670fe4bd7edff6eea3aa0db305))

## [1.21.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.20.0...v1.21.0) (2022-08-23)


### Features

* **requirements:** use data-validation version `0.11.0` ([c6f77c7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c6f77c767af081c6c37fdb3da1517968aedb01d1))
* **requirements:** use models `0.32.2` and orchestrator `0.3.3` ([d7027e8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d7027e828a488d810a054e2e52d0a606040e8b3e))

## [1.20.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.19.1...v1.20.0) (2022-08-10)


### Features

* **es:** build pre-indexed version of ElasticSearch ([f89a754](https://gitlab.com/hestia-earth/hestia-community-edition/commit/f89a754331e998ee1bd76aa3ab37f0159c677645))
* **ui:** update `schema` to `9.7.1` and `ui-components` to `0.3.3` ([90796e3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/90796e3aeb032cebc1761fa4ae55b0492578232e))

### [1.19.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.19.0...v1.19.1) (2022-07-26)


### Bug Fixes

* **ui:** fix errors impact data missing ([328ebbd](https://gitlab.com/hestia-earth/hestia-community-edition/commit/328ebbd8f43b8e7647129456cc90f12c62241c8a))

## [1.19.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.18.0...v1.19.0) (2022-07-26)


### Features

* **requirements:** use data-validation version `0.10.14` ([9c2424f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9c2424f54721f0454d02fc367144793b92765153))
* **requirements:** use models `0.30.2` and orchestrator `0.3.2` ([81bcfda](https://gitlab.com/hestia-earth/hestia-community-edition/commit/81bcfda8cfc5f512c3ea39a0e37c7446ea384e8b))
* **ui:** add page to accept terms of use before using ([772ef87](https://gitlab.com/hestia-earth/hestia-community-edition/commit/772ef87a1dd876241fb65ac579c2a248956749a0))


### Bug Fixes

* **api:** include all blank nodes logs ([282b060](https://gitlab.com/hestia-earth/hestia-community-edition/commit/282b060eb982735d9e00ff33beb1437b62665aa0))

## [1.18.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.17.0...v1.18.0) (2022-07-12)


### Features

* **api:** handle `run_required` in logs ([831e8ca](https://gitlab.com/hestia-earth/hestia-community-edition/commit/831e8ca7d9ebda40cc64fdb2c7cd8d11d5355bfa))
* **requirements:** use data-validation version `0.10.11` ([b04cf09](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b04cf09b7cc29edcae3e34b3e27e0e48c858c8e5))
* **requirements:** use models `0.28.0` and orchestrator `0.3.0` ([d2bd03f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d2bd03f35717f110d02a33a1926ec5468fd6f0fd))
* **requirements:** use models `0.29.0` and orchestrator `0.3.1` ([c586b86](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c586b8689923b30409473b83ba9ebe6439d2068d))
* **ui:** update schema to `9.0.0` ([5434c84](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5434c84707b634049f0cb9fd92da62127322443a))
* **ui:** update schema to `9.2.0` and ui-components to `0.2.4` ([4597ff7](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4597ff780d820687273ebd055d0280ecc6dc7176))

## [1.17.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.16.0...v1.17.0) (2022-06-21)


### Features

* **ui:** update `ui-components` to `0.1.1` ([3142d64](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3142d647035d0db9171bbfbc0053ddcde2de0d61))

## [1.16.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.15.0...v1.16.0) (2022-06-07)


### Features

* **api:** add routes to get model requirements ([101b7e2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/101b7e225b48e949b75a0af44276b76dca4464c3))
* **node:** handle `Transformation` ([5be745b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5be745b41a947ea973e8c42c04c83a20cdd1579f))

## [1.15.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0...v1.15.0) (2022-05-24)


### Features

* **api:** use python `3.9` ([94da8f8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/94da8f887ff44ff136a240d17d770faaba27aa78))
* **requirements:** use data-validation version `0.10.6` ([89555f1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/89555f131ad284297c814132418b4e907c99b6e6))
* **ui settings:** show configuration in better format ([70cf274](https://gitlab.com/hestia-earth/hestia-community-edition/commit/70cf274ac995eebd39339df7d36e031700dfff3e))

## [1.14.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0-2...v1.14.0) (2022-04-23)


### Features

* **results new:** set default cycle as private ([b89965d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b89965dfb9dcc85fd55d774e2fafbf2a5f9652c6))


### Bug Fixes

* **ui:** handle pre-release in version data ([e0f0b18](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e0f0b18aaaa46cba5fe46a112bd11618140f618c))

## [1.14.0-2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0-1...v1.14.0-2) (2022-04-17)


### Features

* **settings:** edit default format for upload ([a350d8f](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a350d8ff939ad4591c1ea5ffa443abdcae480264))
* **version:** show notification when new version is available ([e094074](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e094074ff038e5ed263f2aa437cedcde0fd7bc52))

## [1.14.0-1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.14.0-0...v1.14.0-1) (2022-04-13)

## [1.14.0-0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.13.0...v1.14.0-0) (2022-04-13)


### Features

* **api:** handle using subpath ([55d9139](https://gitlab.com/hestia-earth/hestia-community-edition/commit/55d91396e0726a84766aaff84e870c2031338b51))
* **ui:** add navigation menu in results page ([2532a68](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2532a6851b885499d125fa0ea1cbd966eda54fd1))
* **ui:** show config used and auto-select on recalculate confirm ([a5c7e8c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a5c7e8c9955e3f87ae0ccf164a3fdc0d4bfa4d7e))
* **ui:** support upload of Excel files ([71f00c1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/71f00c1463239513d13608549e8d0bc6405f9b89))

## [1.13.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.12.0...v1.13.0) (2022-04-12)


### Features

* **requirements:** use data-validation version `0.10.5` ([ae221ca](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ae221cab93ec304b2953494b2329419fe9624d73))
* **requirements:** use models version `0.25.0` ([1031b19](https://gitlab.com/hestia-earth/hestia-community-edition/commit/1031b195bc19c2ae6bd3e16ad126161207bfbadf))
* **requirements:** use orchestrator version `0.2.20` ([c900bfb](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c900bfb24ab2d3022604bbafe1ab4a9e0a8ccd26))
* **requirements:** use validation version `0.10.4` ([b9e7f18](https://gitlab.com/hestia-earth/hestia-community-edition/commit/b9e7f18f6f00ed154e67c5319c1caea54da49359))
* **results:** display cycles practices ([e883334](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e883334ad128805fdff85d65e69e40fd3d11cb29))
* **styles:** hide `Data Aggregated` notice site-wide ([6cca6e9](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6cca6e99b87f3785430821c965c43597d54cdbb8))
* **ui results:** change `Emissions` title for `ImpactAssessment` ([0aa6395](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0aa6395aa01994fbcc8159b6d33713b53ba99899))

## [1.12.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v1.11.0...v1.12.0) (2022-03-29)


### Features

* **api config:** handle config not found ([6703a0d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/6703a0d49a9ef4903c3514d33447565d04be6d1d))
* **api:** override api docs url with `BASE_URL` env variable ([66d7ceb](https://gitlab.com/hestia-earth/hestia-community-edition/commit/66d7ceb1c71b68a897577ea30592fe83921c72c2))
* **api:** return `X-API-VERSION` header ([52bf372](https://gitlab.com/hestia-earth/hestia-community-edition/commit/52bf37297de7c6a4267a4a73f8b0e086739ef4a5))
* **config:** add ability to create multiple configurations ([d327b0b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d327b0bea046c3de0258d0a904673e5834405706))
* **requirements:** use models version `0.24.10` ([ce00d7d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ce00d7daa28ac4e91d2eb752896e1521a019006d))
* **requirements:** use validation version `0.9.3` ([8a84290](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8a84290d21d40649c33c69cd4c1066957cb37ecf))
* **ui results upload:** add link to P&N documentation ([0e18a8b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0e18a8b4a49a4e768c15bff50a8501dc8609ca90))
* **ui results:** prevent validation when submitting ([5590688](https://gitlab.com/hestia-earth/hestia-community-edition/commit/5590688b08a723eb95f02ab141d98769933ec1f7))
* **ui results:** rename submit button to calculate ([4fc1bd4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/4fc1bd470e7becf9be2064c653fbffbdddaca03c))
* **ui results:** show error summary ([05733d2](https://gitlab.com/hestia-earth/hestia-community-edition/commit/05733d2dcb5993e183c29835729dd2c52a832749))
* **ui:** display API version in settings ([76996dc](https://gitlab.com/hestia-earth/hestia-community-edition/commit/76996dc75c944fb906993311594a154644ac4ab4))
* **ui:** move code from hestia private frontend repo ([a63a0c8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a63a0c825572161dbef93921ae65bbb065936dcd))
* **ui:** validate schema on draft before calculating ([bc3f774](https://gitlab.com/hestia-earth/hestia-community-edition/commit/bc3f774bc20abfd8c11536fb137bcb62860cdc76))


### Bug Fixes

* **api:** use `root_path` fix Swagger API not showing ([7830539](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7830539eeea67e4031d3624e0f10b1b18200bf6e))
* **config:** make sure `config` folder exists ([c452a67](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c452a6705a1a130a70391b4bf987c808da774b67))
* **cycles:** fix sorting error ([a45db9b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a45db9b5cc9709b01efc622188280ee09f2496ae))
* **drafts:** fix sorting error ([9ca577a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9ca577a7815d240e83247b382098272d4584dfda))
* **ui results:** remove delete node from list in Draft mode ([c0c858e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c0c858e05279ba6a2921e9181d7da290039937bf))
* **ui results:** show loader while validating the data ([2d20ac4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2d20ac47572bab7d2ff4fe0fe4a5243e9458d46f))

## [1.11.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.9...v1.11.0) (2022-03-15)


### Features

* **requirements:** use models version `0.24.8` ([7d07972](https://gitlab.com/hestia-earth/hestia-community-edition/commit/7d079722419a349dca7a4848c17361ef3efad8a7))

### [0.1.9](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.8...v0.1.9) (2022-03-11)


### Features

* **requirements:** use models version `0.24.7` ([9ccd7e0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9ccd7e0f73df1ff98259093e1da5b321d284a7bf))


### Bug Fixes

* **drafts:** handle draft without source on submit ([033f758](https://gitlab.com/hestia-earth/hestia-community-edition/commit/033f758b963593ad4b8f6c5c83ba412ac92f1e68))

### [0.1.8](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.7...v0.1.8) (2022-03-10)


### Features

* **requirements:** use models version `0.24.6` ([fbfa824](https://gitlab.com/hestia-earth/hestia-community-edition/commit/fbfa824978ac1e51a9363cd11130217e21d10e27))

### [0.1.7](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.6...v0.1.7) (2022-03-09)


### Bug Fixes

* **calculate:** delete log file before re-running calculations ([234bab0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/234bab0f691a19de7451164103283aa287494103))
* **calculate:** remove duplicated logs in impact-run.log ([54f2d7a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/54f2d7ab812858cb68797dd3565a756635484cef))
* **validate:** validate a list of nodes ([63549f1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/63549f1e306a4574e252ef50465a6efafb5b0966))

### [0.1.6](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.5...v0.1.6) (2022-03-08)


### Features

* **requirements:** use models version `0.24.5` ([3e1ff2a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/3e1ff2a9a5a6e2d5a41e922d7dc1893d7e17b0c0))
* **validate:** add endpoint to validate a node ([9443f41](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9443f41b22401798bd12ad67db7171ec0318e744))

### [0.1.5](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.4...v0.1.5) (2022-03-07)


### Bug Fixes

* **ee:** fix run no type argument ([d0ecf3b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d0ecf3b48820561a7367f29d886d24b0c9d4a6ce))

### [0.1.4](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.3...v0.1.4) (2022-03-07)


### Bug Fixes

* **api:** change log level for saving calculation logs ([be14c1b](https://gitlab.com/hestia-earth/hestia-community-edition/commit/be14c1b9625de59e6fd189fa0d594b331d81e1d6))

### [0.1.3](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.2...v0.1.3) (2022-03-07)


### Features

* **calculate:** save errors to file ([bcd6b3a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/bcd6b3aad8b32792084b798e1b5bd61654d62fc4))
* **drafts:** create, update and list drafts ([be18605](https://gitlab.com/hestia-earth/hestia-community-edition/commit/be186057c4099f3be79c406bb04ffa00e56950c0))
* handle read file errors return empty ([2d02d2a](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2d02d2ac39bfab3e5183b753805472f6afe0bdc2))
* **requirements:** use models version `0.24.4` ([2f1b0a3](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2f1b0a3a2a8b6a54df19094926b5c93de4799a8a))

### [0.1.2](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.1...v0.1.2) (2022-02-23)


### Features

* **requirements:** use models version `0.24.1` ([20a634c](https://gitlab.com/hestia-earth/hestia-community-edition/commit/20a634c16c1f8777d2dd47d7033e85e5b1b98174))
* **requirements:** use models version `0.24.3` ([61703aa](https://gitlab.com/hestia-earth/hestia-community-edition/commit/61703aad631f149de02733e14a272f8c2fc41b5e))

### [0.1.1](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.1.0...v0.1.1) (2022-02-21)


### Features

* **config:** add routes to get/update/delete ([8ce8dad](https://gitlab.com/hestia-earth/hestia-community-edition/commit/8ce8dadd316e911adce82865d1b9933b5d89fa10))

## [0.1.0](https://gitlab.com/hestia-earth/hestia-community-edition/compare/v0.0.1...v0.1.0) (2022-02-18)


### ⚠ BREAKING CHANGES

* **results:** `/results` prefix routes is now `/cycles`
* **calculate:** `POST /calculate` is now `POST /recalculate`

### Features

* **cycles:** add filter/pagination to list ([d2616ae](https://gitlab.com/hestia-earth/hestia-community-edition/commit/d2616aee19de6a5af0c2825dc4d4b0b177a11f9a))
* **cycles:** add route get logs by model ([2dd4318](https://gitlab.com/hestia-earth/hestia-community-edition/commit/2dd43187d3918d4512c5e4cc9ffd25de7e1a444e))
* **impact assessments:** add get impact and logs routes ([9fde559](https://gitlab.com/hestia-earth/hestia-community-edition/commit/9fde5598e17fc49907bd7cdec0e768b2e74ff645))
* **search:** redirect search requests to Hestia API ([0e92ce0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/0e92ce0ec6fb5521822aaf3d5230efaeb465efa1))


### Bug Fixes

* **app:** only require `EARTH_ENGINE_KEY_FILE` env variable ([ce7989d](https://gitlab.com/hestia-earth/hestia-community-edition/commit/ce7989df89b0266cacf1c160ae90e2aa7f994f85))
* remove trailing slash in routes ([fa2222e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/fa2222e2e08222f02b9dd5973e438d2a3ac20b59))


* **calculate:** change prefix to `/recalculate` ([e0d34ac](https://gitlab.com/hestia-earth/hestia-community-edition/commit/e0d34aca27d650e44e079479471e075d0fdd2ddc))
* **results:** moved to cycles ([1faabe4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/1faabe4617f8963321d858218e4af78b65c99daa))

### 0.0.1 (2022-02-16)


### Features

* handle site calculations ([97543a8](https://gitlab.com/hestia-earth/hestia-community-edition/commit/97543a8742c2c35b78c899afa440b338026bc79f))
* **ui:** add download summary for recalculated data ([20212a0](https://gitlab.com/hestia-earth/hestia-community-edition/commit/20212a020ca3b1e53877039faecd9e7217719f54))
* **ui:** add frontend ([c3cc3f1](https://gitlab.com/hestia-earth/hestia-community-edition/commit/c3cc3f1426b7d66e9e35a836be34917c10a507bb))
* **ui:** add geospatial search by coordinates ([a743d4e](https://gitlab.com/hestia-earth/hestia-community-edition/commit/a743d4e791f53349247d7b603c4722db08d21c2f))


### Bug Fixes

* **ui:** make it clearer how to calculate data ([aaef3b4](https://gitlab.com/hestia-earth/hestia-community-edition/commit/aaef3b49eb7cd4b3609a24922e21d015b22dbd72))
