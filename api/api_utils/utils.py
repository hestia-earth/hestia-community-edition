import os
import json
from natsort import natsorted
import numpy as np
from enum import Enum

from .version import VERSION
from .log import logger

DATA_FOLDER = os.getenv('DATA_FOLDER', 'data')
SOURCE_FILENAME = 'source.csv'


class DataState(str, Enum):
    original = 'original'
    recalculated = 'recalculated'


class Status(str, Enum):
    queued = 'queued'
    started = 'started'
    completed = 'completed'
    error = 'error'


# fix error "Object of type int64 is not JSON serializable"
class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)


def get_param(params: dict, key: str, default=None):
    value = params.get(key, default)
    return default if value is None else value


def get_required_param(params, key: str):
    if key not in params:
        raise KeyError(f"Missing required '{key}'")
    return params.get(key)


def create_folders(filepath: str): return os.makedirs(os.path.dirname(filepath), exist_ok=True)


def read_file(filepath: str):
    if not os.path.exists(filepath):
        return None
    try:
        with open(filepath, 'r') as f:
            return f.read()
    except Exception as e:
        logger.warning(f"file not found {filepath}: {str(e)}")
        return None


def read_json(filepath: str):
    if not os.path.exists(filepath):
        return None
    try:
        with open(filepath, 'r') as f:
            return json.load(f)
    except Exception:
        logger.warning(f"file not found {filepath}")
        return {}


def write_json(filepath: str, data: dict):
    create_folders(filepath)
    with open(filepath, 'w') as f:
        f.write(to_string(data))


def to_string(data: dict): return json.dumps(data, indent=2, ensure_ascii=False, cls=NpEncoder)


def get_created_at(filepath: str):
    return int(os.path.getctime(filepath) * 1000) if os.path.exists(filepath) else None


def get_modified_at(filepath: str):
    return int(os.path.getmtime(filepath) * 1000) if os.path.exists(filepath) else None


def sort_versions(versions: list[str]): return list(reversed(natsorted(versions)))


def node_type(node: dict) -> str: return node.get('@type', node.get('type'))


def node_id(node: dict) -> str: return node.get('@id', node.get('id'))


def node_type_folder(node: dict):
    folder = os.path.join(DATA_FOLDER, node_type(node))
    os.makedirs(folder, exist_ok=True)
    return folder


def node_folder(node: dict, version: str = ''):
    return os.path.join(node_type_folder(node), node_id(node), version).rstrip('/')


def node_exists(node: dict): return os.path.exists(node_folder(node))


def node_versions(node: dict):
    folder = node_folder(node)
    return sort_versions([
        v for v in os.listdir(folder) if os.path.isdir(os.path.join(folder, v))
    ]) if os.path.exists(folder) else []


def node_latest_version(node: dict):
    versions = node_versions(node)
    return versions[0] if len(versions) > 0 else VERSION


def node_config_path(node: dict, version: str = ''):
    return os.path.join(node_folder(node, version), 'config.json')


def node_config(node: dict, version: str = ''): return read_json(node_config_path(node, version))


def update_node_config(node: dict, values: dict, version: str = VERSION):
    filepath = node_config_path(node, version)
    create_folders(filepath)
    config = read_json(filepath) or {}
    write_json(filepath, config | values)


def set_node_config(node: dict, key: str, value: str, version: str = VERSION):
    update_node_config(node, {key: value}, version)


def get_node_config(node: dict, key: str, version: str = ''): return (node_config(node, version) or {}).get(key)


def log_filename(node: dict, version: str = ''):
    return os.path.join(node_folder(node, version or node_latest_version(node)), 'run.log')


def error_filename(node: dict, version: str = ''):
    return os.path.join(node_folder(node, version or node_latest_version(node)), 'error.log')


def node_data_path(node: dict, dataState: DataState = DataState.original, version: str = VERSION):
    return os.path.join(node_folder(node, version), f"{dataState.value}.jsonld")


def node_source_path(node: dict, version: str = ''): return os.path.join(node_folder(node, version), SOURCE_FILENAME)


def _node_status_path(node: dict, version: str = ''): return os.path.join(node_folder(node, version), 'status.txt')


def set_node_status(node: dict, status: Status):
    filepath = _node_status_path(node, VERSION)
    create_folders(filepath)
    with open(filepath, 'w') as f:
        f.write(status.value)


def get_node_status(node: dict, version: str = ''):
    filepath = _node_status_path(node, version or node_latest_version(node))
    if os.path.exists(filepath):
        with open(filepath, 'r') as f:
            return f.read()
    return Status.completed.value


def is_equal(a: dict, b: dict): return json.dumps(a) == json.dumps(b)


def safe_delete(path: str): return os.remove(path) if os.path.exists(path) else None


def init_earth_engine():
    # init earth engine if present
    if os.getenv('EARTH_ENGINE_KEY_FILE'):
        from hestia_earth.earth_engine import init_gee
        try:
            init_gee()
        except Exception as e:
            logger.warning(f"failed to initialise GEE, reason: {str(e)}")
