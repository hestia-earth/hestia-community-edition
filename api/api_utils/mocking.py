import os
from hestia_earth.schema import SchemaType
from hestia_earth.utils.request import api_url
from hestia_earth.utils.api import node_type_to_url, _safe_get_request
from hestia_earth.models import utils
from hestia_earth.models.utils import site

from .log import logger
from .utils import DataState, read_json, node_folder, node_data_path, node_id


def _download_hestia_node(id: str, type: SchemaType, data_state='recalculated'):
    base_url = f"{api_url()}/{node_type_to_url(type)}/{id}"
    logger.debug(f"Downloading Node from HESTIA: {base_url}")
    return _safe_get_request(f"{base_url}?dataState={data_state}") or _safe_get_request(base_url)


def _download_local_node(node: dict):
    original_filepath = node_data_path(node, DataState.original)
    recalculated_filepath = node_data_path(node, DataState.recalculated)
    return read_json(recalculated_filepath) if os.path.exists(recalculated_filepath) else read_json(original_filepath)


def _load_calculated_node(node: dict, type: SchemaType, data_state='recalculated'):
    # only return the data if it is present in the CE, otherwise assume it is from the HESTIA platform
    is_local_node = os.path.exists(node_folder(node))
    return _download_local_node({'@type': type.value, '@id': node_id(node)}) if is_local_node else (
        _download_hestia_node(node.get('@id'), type, data_state)
    )


def _site_related_cycles(site: dict):
    return [_load_calculated_node({'@type': SchemaType.CYCLE.value, '@id': node_id(site)}, SchemaType.CYCLE)]


def enable_mock():
    utils._load_calculated_node = _load_calculated_node
    site.related_cycles = _site_related_cycles
