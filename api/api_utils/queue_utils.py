import os
import json
from enum import Enum
import pika
import requests

QUEUE_URL = os.getenv('AMQP_URL')
QUEUE_CHANNEL = os.getenv('AMQP_CHANNEL', 'hestia-ce-runner')
QUEUE_ADMIN_POST = os.getenv('AMQP_ADMIN_PORT', '15672')


class QueueAction(str, Enum):
    calculate_nodes = 'calculate_nodes',
    validate_draft = 'validate_draft'


def enabled(): return all([QUEUE_URL, QUEUE_CHANNEL])


def get_connection():
    return pika.BlockingConnection(pika.URLParameters(QUEUE_URL))


def get_channel(connection: pika.BlockingConnection):
    connection = connection
    channel = connection.channel()
    channel.queue_declare(queue=QUEUE_CHANNEL, durable=True)
    return channel


def _call_rabbitmq_api(path: str):
    hostname = QUEUE_URL.split('@')[1].split('/')[0].split(':')[0]
    user, password = QUEUE_URL.split('@')[0].replace('amqp://', '').split(':')
    url = f"http://{hostname}:{QUEUE_ADMIN_POST}/api/{path}"
    return requests.get(url, auth=(user, password)).json()


def consumer_details(consumer: dict):
    return {
        'id': consumer.get('consumer_tag'),
        'status': consumer.get('activity_status'),
        'active': consumer.get('active', False)
    }


def get_queue_details():
    result = _call_rabbitmq_api(f"queues/%2F/{QUEUE_CHANNEL}")
    return {
        'messages': {
            'total': result.get('messages', 0),
            'current': result.get('messages_unacknowledged'),
            'remaining': result.get('messages_ready', 0)
        }
    }


def _connection_details(connection: dict):
    channels = _call_rabbitmq_api(f"connections/{connection.get('name')}/channels")
    channel = channels[0] if len(channels) > 0 else {}
    return {
        'id': connection.get('name'),
        'status': connection.get('state'),
        'connectedAt': connection.get('connected_at'),
        'active': channel.get('messages_unacknowledged', 0) > 0
    }


def get_connections():
    return list(map(_connection_details, _call_rabbitmq_api('connections')))


def publish_message(action: QueueAction, data: dict):
    connection = get_connection()
    channel = get_channel(connection)
    channel.basic_publish(
        exchange='',
        routing_key=QUEUE_CHANNEL,
        properties=pika.BasicProperties(
            delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
        ),
        body=json.dumps({'action': action.value, 'data': data})
    )
    connection.close()
