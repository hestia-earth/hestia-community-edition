import traceback
from enum import Enum
from hestia_earth.models.preload_requests import enable_preload
from hestia_earth.models.version import VERSION as MODELS_VERSION
from hestia_earth.schema import NodeType
from hestia_earth.orchestrator import run

from .version import VERSION
from .log import logger
from .utils import (
    DataState, Status, error_filename, log_filename, node_data_path, node_type, node_id,
    set_node_config, safe_delete, set_node_status, get_node_config, read_json, write_json
)
from .config_utils import load_config


class Type(str, Enum):
    Cycle = NodeType.CYCLE.value
    ImpactAssessment = NodeType.IMPACTASSESSMENT.value
    Site = NodeType.SITE.value


Types = [e.value for e in Type]


def log_error(node: dict, error: str, stack: str):
    filepath = error_filename(node, VERSION)
    write_json(filepath, {
        'error': error,
        'stack': stack
    })


def delete_log_error(node: dict): return safe_delete(error_filename(node, VERSION))


def _setup_logger(_logger, log_to_file, log_filename: str):
    # make sure we don't add logs indefinitely
    for handler in _logger.handlers:
        _logger.removeHandler(handler)
    log_to_file(log_filename)


def _create_log_file(node: dict):
    try:
        filename = log_filename(node, VERSION)
        # set orchestrator and models so both log to the same file
        from hestia_earth.orchestrator.log import logger as orchestrator_logger, log_to_file
        _setup_logger(orchestrator_logger, log_to_file, filename)
        from hestia_earth.models.log import logger as models_logger, log_to_file as models_log_to_file
        _setup_logger(models_logger, models_log_to_file, filename)
        return True
    except Exception as e:
        logger.error(f"error creating log file: {str(e)}")
        return False


def save_node_data(node: dict, dataState: DataState = DataState.original):
    filepath = node_data_path(node, dataState)
    write_json(filepath, node)


def calculate_node(node: dict, config: str = None, stage: int = None):
    try:
        # read the data in original (first stage) or recalculated
        data_state = DataState.original if stage == 1 else DataState.recalculated
        node = read_json(node_data_path({'@type': node_type(node), '@id': node_id(node)}, data_state))

        set_node_status(node, Status.started)
        set_node_config(node, 'modelsVersion', MODELS_VERSION)

        safe_delete(log_filename(node, VERSION)) if stage == 1 else None
        delete_log_error(node) if stage == 1 else None

        _create_log_file(node)

        enable_preload(node=node)

        logger.debug(f"Calculating {node_type(node)} {node_id(node)} with config: {config}, at stage: {stage}")

        result = run(node, load_config(node_type(node), config), stage)

        save_node_data(result, DataState.recalculated)
        set_node_status(node, Status.completed)

        return result
    except Exception as e:
        stack = traceback.format_exc()
        set_node_status(node, Status.error)
        log_error(node, str(e), stack)
        logger.error(f"An error occured while calculating {node_type(node)} {node_id(node)}: {stack}")
        raise e


_STAGES_ORDER = [
    (NodeType.SITE.value, 1),
    (NodeType.CYCLE.value, 1),
    (NodeType.SITE.value, 2),
    (NodeType.CYCLE.value, 2),
    (NodeType.IMPACTASSESSMENT.value, 1)
]


def _filter_by_type(nodes: list, type: str):
    return [n for n in nodes if node_type(n) == type]


def map_nodes_to_stages(nodes: list):
    return [
        (stage, _filter_by_type(nodes, type)) for type, stage in _STAGES_ORDER
    ]


def calculate_nodes(nodes: list):
    logger.info(f"Received {len(nodes)} nodes to calculate")

    for stage, filtered_nodes in map_nodes_to_stages(nodes):
        for node in filtered_nodes:
            try:
                calculate_node(node, get_node_config(node, 'config', VERSION), stage)
            except Exception:
                pass

    logger.info('Done calculating nodes')
