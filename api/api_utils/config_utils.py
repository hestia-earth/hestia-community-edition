import os
import json
from hestia_earth.models.config import load_config as load_default_config

from .log import logger
from .utils import DATA_FOLDER, get_modified_at, is_equal
from .settings_utils import SettingKey, get_setting


DEFAULT_CONFIG = 'default'
BASE_FOLDER = os.path.join(DATA_FOLDER, 'config')
os.makedirs(BASE_FOLDER, exist_ok=True)


def config_base_folder(id: str): return os.path.join(BASE_FOLDER, id)


def config_path(type: str, id: str): return os.path.join(config_base_folder(id), f"{type}.json")


def load_config(type: str, id: str = DEFAULT_CONFIG):
    if not id or id == DEFAULT_CONFIG:
        skip_aggregated_models = not get_setting(SettingKey.enable_aggregated_models)
        logger.debug(f"Using default configuration, skip aggregated models?: {skip_aggregated_models}")
        return load_default_config(type, skip_aggregated_models=skip_aggregated_models)

    with open(config_path(type, id)) as f:
        return json.load(f)


def load_config_with_default(type: str, id: str = DEFAULT_CONFIG):
    default_config = load_config(type)
    if default_config:
        try:
            config = load_config(type, id)

            return {
                'type': type,
                'updatedOn': get_modified_at(config_path(type, id)),
                'hasOverride': not is_equal(default_config, config),
                'default': default_config,
                'config': config
            }
        except FileNotFoundError:
            pass

    return {}
