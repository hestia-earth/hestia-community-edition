import os
import logging


def _add_handler(logger: logging.Logger, formatter: logging.Formatter, handler: logging.Handler):
    handler.setFormatter(formatter)
    logger.addHandler(handler)


LOG_LEVEL = os.getenv('LOG_LEVEL', 'INFO')
logger = logging.getLogger('hestia_earth.community_edition')
logger.setLevel(logging.getLevelName('DEBUG'))

# stream logging
_formatter = logging.Formatter('%(levelname)s:    %(asctime)s - %(filename)s - %(message)s')
_handler = logging.StreamHandler()
_handler.setLevel(logging.getLevelName(LOG_LEVEL))
_add_handler(logger, _formatter, _handler)
