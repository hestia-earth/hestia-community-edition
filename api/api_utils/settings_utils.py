import os
from typing import Any, Union
from enum import Enum

from .utils import DATA_FOLDER, read_json, write_json


class SettingKey(str, Enum):
    default_format = 'defaultFormat',
    enable_queuing = 'enableQueuing',
    enable_aggregated_models = 'enableAggregatedModels'


SETTINGS_PATH = os.path.join(DATA_FOLDER, 'settings.json')
DEFAULT_SETTINGS = {
    SettingKey.default_format.value: 'HESTIA',
    SettingKey.enable_queuing.value: False,
    SettingKey.enable_aggregated_models: False
}


def load_settings(): return read_json(SETTINGS_PATH) or DEFAULT_SETTINGS


def update_all_settings(data: dict):
    settings = load_settings()
    write_json(SETTINGS_PATH, settings | data)


def update_settings(key: Union[str, SettingKey], value: Any):
    settings = load_settings()
    setting_key = key if isinstance(key, str) else key.value
    settings[setting_key] = value
    write_json(SETTINGS_PATH, settings)


def get_setting(key: Union[str, SettingKey]):
    settings = load_settings()
    setting_key = key.value if isinstance(key, Enum) else key
    return settings.get(setting_key)
