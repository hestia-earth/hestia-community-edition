import os


def set_default_envs():
    os.environ.setdefault('LOG_LEVEL', 'DEBUG')
    os.environ.setdefault('DOWNLOAD_FOLDER', '/app')
    os.environ.setdefault('DOWNLOAD_FOLDER_GLOSSARY', '/app')
    os.environ.setdefault('DISTRIBUTION_DATA_FOLDER', '/app/distribution')
    # avoid running expensive geospatial validations
    os.environ.setdefault('VALIDATE_SPATIAL', 'false')
    # disabled as distribution files need to be reviewed
    os.environ.setdefault('VALIDATE_DISTRIBUTION', 'false')
    # disabled as IA should be generated manually if missing
    os.environ.setdefault('VALIDATE_LINKED_IA', 'false')
    # disabled are not required in CE
    os.environ.setdefault('VALIDATE_SITE_LINKED_NODES', 'false')
