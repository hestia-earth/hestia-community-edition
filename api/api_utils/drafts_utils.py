import os
from typing import Any
from hestia_earth.validation.version import VERSION

from .utils import DATA_FOLDER, Status, read_json, write_json, create_folders
from .validate_utils import validate_nodes

DRAFT_FOLDER = 'drafts'
DRAFT_BASE_FOLDER = os.path.join(DATA_FOLDER, DRAFT_FOLDER)


def draft_folder(id: str):
    folder = os.path.join(DRAFT_BASE_FOLDER, id)
    os.makedirs(folder, exist_ok=True)
    return folder


def draft_filepath(id: str): return os.path.join(draft_folder(id), 'data.json')


def draft_nodes(id: str): return read_json(draft_filepath(id)).get('nodes', [])


def update_draft_data(id: str, key: str, value: Any):
    filepath = draft_filepath(id)
    data = read_json(filepath)
    data[key] = value
    write_json(filepath, data)


def _draft_status_path(id: str): return os.path.join(draft_folder(id), 'status.txt')


def set_draft_status(id: str, status: Status):
    filepath = _draft_status_path(id)
    create_folders(filepath)
    with open(filepath, 'w') as f:
        f.write(status.value)


def get_draft_status(id: str):
    filepath = _draft_status_path(id)
    if os.path.exists(filepath):
        with open(filepath, 'r') as f:
            return f.read()
    return Status.completed.value


def validate_draft(id: str):
    set_draft_status(id, Status.started)

    filepath = draft_filepath(id)
    data = read_json(filepath)
    nodes = data.get('nodes', [])
    errors = validate_nodes(nodes)
    has_error = any([
        any([v.get('level') == 'error' for v in values]) for values in errors
    ])

    data['validation'] = {
        'version': VERSION,
        'errors': errors,
        'success': not has_error
    }
    write_json(filepath, data)

    set_draft_status(id, Status.completed)
