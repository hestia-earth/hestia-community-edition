from hestia_earth.schema import SCHEMA_VERSION
from hestia_earth.models.version import VERSION as MODELS_VERSION

VERSION = '4.45.0'
VERSION_DATE = ''


def get_versions():
    return {
        'current': VERSION,
        'date': VERSION_DATE,
        'schema': SCHEMA_VERSION,
        'models': MODELS_VERSION
    }
