import shutil
from natsort import natsorted
from hestia_earth.schema import NodeType

NODE_TYPES = [
    NodeType.CYCLE,
    NodeType.IMPACTASSESSMENT,
    NodeType.SITE
]


def move_file(src: str, dest: str):
    try:
        shutil.move(src, dest)
    except Exception:
        pass


def should_migrate_version(version: str, max_version: str):
    return (version is None or natsorted([version, max_version]).index(version) == 0) and version != max_version
