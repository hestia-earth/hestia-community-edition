# migrate all existing caculations to their version folder
import os
from hestia_earth.schema import NodeType
from api_utils.utils import node_type_folder, node_folder, node_config_path, node_config, set_node_config
from api_utils.version import VERSION

from .utils import NODE_TYPES, move_file

NAME = os.path.basename(__file__)


def _should_migrate_node(node: dict):
    # only migrate when there is a `config` not in a version directory
    config_path = node_config_path(node, version='')
    return os.path.exists(config_path)


def _migrate_node(node: dict):
    print(NAME, 'Migrating node:', node_folder(node))
    config = node_config(node, version='') or {}
    version = config.get('version', VERSION)
    # create the version folder
    base_folder = node_folder(node)
    os.makedirs(os.path.join(base_folder, version), exist_ok=True)
    # move files in new folder
    [
        move_file(
            os.path.join(base_folder, filename),
            os.path.join(base_folder, version, filename)
        ) for filename in os.listdir(base_folder) if os.path.isfile(os.path.join(base_folder, filename))
    ]
    # set version in config
    set_node_config(node, 'version', version, version)


def _migrate_nodes(node_type: NodeType):
    base_folder = node_type_folder({'@type': node_type.value})
    ids = os.listdir(base_folder) if os.path.exists(base_folder) else []
    nodes = [{'@type': node_type.value, '@id': node_id} for node_id in ids]
    return list(map(_migrate_node, filter(_should_migrate_node, nodes)))


def run():
    return list(map(_migrate_nodes, NODE_TYPES))
