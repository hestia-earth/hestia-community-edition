# migrate to Schema 21.0.0.
# See https://gitlab.com/hestia-earth/hestia-schema/-/blob/v21.0.0/CHANGELOG.md
import os
from hestia_earth.schema import NodeType
from api_utils.utils import (
    DataState, node_type_folder, node_versions, node_data_path, read_json, node_type, write_json,
    set_node_config, get_node_config
)
from api_utils.version import SCHEMA_VERSION
from api_utils.drafts_utils import DRAFT_BASE_FOLDER, draft_filepath

from ..version import VERSION
from .utils import should_migrate_version

NAME = os.path.basename(__file__)
MIGRATE_CE_VERSION = '4.0.0'
MIGRATE_SCHEMA_VERSION = '21.0.0'


def _should_migrate_node_version(node: dict, version: str):
    schema = get_node_config(node, 'schema', version)
    return all([
        should_migrate_version(version, MIGRATE_CE_VERSION),
        should_migrate_version(schema, MIGRATE_SCHEMA_VERSION)
    ])


def _migrate_impact_data(data: dict):
    if 'product' in data and isinstance(data['product'], dict):
        product = {**data['product']}
        data['product'] = {'@type': 'Product', 'term': product}
    return data


def _migrate_impact_assessment_version(impact: dict, version: str):
    def migrate_by_state(state: DataState):
        data_path = node_data_path(impact, state, version)
        set_node_config(impact, 'version', VERSION, version)
        set_node_config(impact, 'schema', SCHEMA_VERSION, version)
        if os.path.exists(data_path):
            print(NAME, 'Migrating impact:', data_path)
            data = _migrate_impact_data(read_json(data_path))
            write_json(data_path, data)

    return [migrate_by_state(DataState.original), migrate_by_state(DataState.recalculated)]


def _migrate_impact_assessment(impact_id: str):
    node = {'type': NodeType.IMPACTASSESSMENT.value, 'id': impact_id}
    return [
        _migrate_impact_assessment_version(node, version)
        for version in node_versions(node) if _should_migrate_node_version(node, version)
    ]


def _migrate_impact_assessments():
    base_folder = node_type_folder({'type': NodeType.IMPACTASSESSMENT.value})
    ids = os.listdir(base_folder)
    return list(map(_migrate_impact_assessment, ids))


def _migrate_transformations(transformations: list):
    def migrate(values: tuple):
        index, transformation = values

        transformation['transformationId'] = f"transformation-{index + 1}"

        if 'previousTransformationTerm' in transformation:
            tr_index = [
                transformation.get('term', {}).get('@id') == tr.get('term', {}).get('@id')
                for tr in transformations
            ].index(True)
            if tr_index > 0:
                transformation['previousTransformationId'] = f"transformation-{tr_index + 1}"

            del transformation['previousTransformationTerm']

        return transformation

    return list(map(migrate, enumerate(transformations)))


def _migrate_cycle_data(data: dict):
    if len(data.get('transformations', [])) > 0:
        data['transformations'] = _migrate_transformations(data['transformations'])
    return data


def _migrate_cycle_version(cycle: dict, version: str):
    def migrate_by_state(state: DataState):
        data_path = node_data_path(cycle, state, version)
        set_node_config(cycle, 'version', VERSION, version)
        set_node_config(cycle, 'schema', SCHEMA_VERSION, version)
        if os.path.exists(data_path):
            print(NAME, 'Migrating cycle:', data_path)
            data = _migrate_cycle_data(read_json(data_path))
            write_json(data_path, data)

    return [migrate_by_state(DataState.original), migrate_by_state(DataState.recalculated)]


def _migrate_cycle(cycle_id: str):
    node = {'type': NodeType.CYCLE.value, 'id': cycle_id}
    return [
        _migrate_cycle_version(node, version)
        for version in node_versions(node) if _should_migrate_node_version(node, version)
    ]


def _migrate_cycles():
    base_folder = node_type_folder({'type': NodeType.CYCLE.value})
    ids = os.listdir(base_folder)
    return list(map(_migrate_cycle, ids))


def _migrate_site_version(site: dict, version: str):
    set_node_config(site, 'version', VERSION, version)
    set_node_config(site, 'schema', SCHEMA_VERSION, version)


def _migrate_site(site_id: str):
    node = {'type': NodeType.SITE.value, 'id': site_id}
    return [
        _migrate_site_version(node, version)
        for version in node_versions(node) if _should_migrate_node_version(node, version)
    ]


def _migrate_sites():
    base_folder = node_type_folder({'type': NodeType.SITE.value})
    ids = os.listdir(base_folder)
    return list(map(_migrate_site, ids))


def _migrate_draft_impact(node: dict):
    node['cycle'] = _migrate_cycle_data(node.get('cycle'))
    return _migrate_impact_data(node)


def _migrate_draft(draft_id: str):
    filepath = draft_filepath(draft_id)
    if os.path.exists(filepath):
        data = read_json(filepath)
        nodes = [
            (
                _migrate_cycle_data(node) if node_type(node) == NodeType.CYCLE.value
                else _migrate_draft_impact(node) if node_type(node) == NodeType.IMPACTASSESSMENT.value
                else node
            )
            for node in data.get('nodes', [])
        ]
        write_json(filepath, {'version': VERSION, 'schema': SCHEMA_VERSION, 'nodes': nodes})


def _should_migrate_draft(draft_id: str):
    # only migrate when there is no `version` inside the data, or the version is below `3`
    filepath = draft_filepath(draft_id)
    version = (read_json(filepath) or {}).get('version')
    schema = (read_json(filepath) or {}).get('schema')
    return all([
        should_migrate_version(version, MIGRATE_CE_VERSION),
        should_migrate_version(schema, MIGRATE_SCHEMA_VERSION)
    ])


def _migrate_drafts():
    ids = os.listdir(DRAFT_BASE_FOLDER) if os.path.exists(DRAFT_BASE_FOLDER) else []
    ids = list(filter(_should_migrate_draft, ids))
    for draft_id in ids:
        try:
            _migrate_draft(draft_id)
        except Exception as e:
            print(NAME, 'Could not migrate Draft with id', draft_id, '- reason:', str(e))
            continue


def run():
    return [_migrate_impact_assessments(), _migrate_cycles(), _migrate_sites(), _migrate_drafts()]
