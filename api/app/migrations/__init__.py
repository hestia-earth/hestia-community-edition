from api_utils.log import logger

from .migrate_cycle_impacts import run as migrate_cycle_impacts
from .migrate_to_versions import run as migrate_to_versions
from .fix_invalid_type_id import run as fix_invalid_type_id
from .migrate_schema_21_0_0 import run as migrate_schema_21_0_0
from .migrate_settings_path import run as migrate_settings_path

MIGRATIONS = [
    migrate_cycle_impacts,
    migrate_to_versions,
    fix_invalid_type_id,
    migrate_schema_21_0_0,
    migrate_settings_path
]


def run():
    logger.debug('Running migrations, please wait...')
    list(map(lambda func: func(), MIGRATIONS))
    logger.debug('Finished running migrations.')
