# migrate ImpactAssessment data saved under Cycle
import os
from hestia_earth.schema import NodeType
from hestia_earth.utils.tools import non_empty_list, flatten
from api_utils.utils import (
    DATA_FOLDER, DataState, read_json, node_config_path, node_data_path, node_type_folder, node_config, safe_delete,
    write_json
)
from api_utils.drafts_utils import DRAFT_BASE_FOLDER, draft_filepath

from ..version import VERSION
from ..calculate_utils import _impact_from_cycle, _clean_node
from .utils import move_file, should_migrate_version

NAME = os.path.basename(__file__)
MIGRATE_ABOVE_VERSION = '3.0.0'


def _isVersionKey(key: str): return key in ['addedVersion', 'updatedVersion']


def _isValidValue(value):
    return value and (isinstance(value, list) or value.get('addedVersion') or value.get('updatedVersion'))


def _find_models_version(data: dict):
    versions = []

    for key, value in data.items():
        if _isVersionKey(key) and _isValidValue(value):
            versions.extend(value)
        elif isinstance(value, dict):
            versions.extend(value.get('addedVersion') or value.get('updatedVersion') or [])
        elif isinstance(value, list):
            versions.extend([_find_models_version(v) for v in value if isinstance(v, dict)])

    versions = non_empty_list(flatten(versions))
    return versions[0] if len(versions) > 0 else None


def _write_data(filepath: str, node: dict):
    write_json(filepath, _clean_node(node))


def _write_config(node: dict, config: dict = None):
    if config:
        write_json(node_config_path(node), config)


def _update_impact_linked_nodes(data: dict, id: str):
    cycle = read_json(os.path.join(DATA_FOLDER, NodeType.CYCLE.value, id, f"{DataState.original.value}.jsonld"))
    data['@id'] = id
    data['cycle'] = {'@type': NodeType.CYCLE.value, '@id': id}
    if 'site' in cycle:
        data['site'] = {'@type': NodeType.SITE.value, '@id': id}
    return data


def _migrate_impact(cycle_id: str, config: dict):
    src_folder = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id)
    dest_folder = os.path.join(DATA_FOLDER, NodeType.IMPACTASSESSMENT.value, cycle_id)
    print(NAME, 'Migrating impact:', src_folder)
    os.makedirs(dest_folder, exist_ok=True)

    original_filepath = os.path.join(src_folder, 'impact.jsonld')
    if os.path.exists(original_filepath):
        data = read_json(original_filepath)
        data = _update_impact_linked_nodes(data, cycle_id)
        _write_data(os.path.join(dest_folder, f"{DataState.original.value}.jsonld"), data)
        safe_delete(original_filepath)

    recalculated_filepath = os.path.join(src_folder, 'impact-recalculated.jsonld')
    if os.path.exists(recalculated_filepath):
        data = read_json(recalculated_filepath)
        data = _update_impact_linked_nodes(data, cycle_id)
        _write_data(os.path.join(dest_folder, f"{DataState.recalculated.value}.jsonld"), data)
        _write_config({'@type': NodeType.IMPACTASSESSMENT.value, '@id': cycle_id}, config)
        safe_delete(recalculated_filepath)

    move_file(
        os.path.join(src_folder, 'impact-run.log'),
        os.path.join(dest_folder, 'run.log')
    )
    move_file(
        os.path.join(src_folder, 'source.csv'),
        os.path.join(dest_folder, 'source.csv')
    )


def _migrate_site_data(cycle_id: str, dataState: DataState, config: dict = None):
    try:
        filepath = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id, f"{dataState.value}.jsonld")
        data = read_json(filepath).get('site') if os.path.exists(filepath) else None
        if data:
            dest_folder = os.path.join(DATA_FOLDER, NodeType.SITE.value, cycle_id)
            os.makedirs(dest_folder, exist_ok=True)
            data['@id'] = cycle_id
            _write_data(os.path.join(dest_folder, f"{dataState.value}.jsonld"), data)
            _write_config({'@type': NodeType.SITE.value, '@id': cycle_id}, config)
    except Exception:
        pass


def _migrate_site(cycle_id: str, config: dict):
    src_folder = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id)
    print(NAME, 'Migrating site:', src_folder)
    _migrate_site_data(cycle_id, DataState.original)
    _migrate_site_data(cycle_id, DataState.recalculated, config)


def _migrate_cycleDataState(cycle_id: str, dataState: DataState):
    filepath = os.path.join(DATA_FOLDER, NodeType.CYCLE.value, cycle_id, f"{dataState.value}.jsonld")
    if os.path.exists(filepath):
        data = read_json(filepath)
        if 'site' in data:
            data['site'] = {'@type': NodeType.SITE.value, '@id': cycle_id}
        _write_data(filepath, data)


def _migrate_cycleData(cycle_id: str, config: dict):
    _write_config({'@type': NodeType.CYCLE.value, '@id': cycle_id}, config)
    _migrate_cycleDataState(cycle_id, DataState.original)
    _migrate_cycleDataState(cycle_id, DataState.recalculated)


def _migrate_cycle(cycle_id: str):
    base_folder = os.path.join(DATA_FOLDER, NodeType.CYCLE.value)
    config = node_config({'@type': NodeType.CYCLE.value, '@id': cycle_id}) or {
        'config': 'default',
        'version': VERSION
    }

    # find models version from recalculated file
    recalculated_filepath = os.path.join(base_folder, cycle_id, f"{DataState.recalculated.value}.jsonld")
    version = _find_models_version(read_json(recalculated_filepath)) if os.path.exists(recalculated_filepath) \
        else None
    if version:
        config['modelsVersion'] = version

    # migrate to ImpactAssessment
    if os.path.exists(os.path.join(base_folder, cycle_id, 'impact.jsonld')):
        _migrate_impact(cycle_id, config)

    # migrate to Site
    if not os.path.exists(os.path.join(DATA_FOLDER, NodeType.SITE.value, cycle_id)):
        _migrate_site(cycle_id, config)

    # migrate Cycle
    _migrate_cycleData(cycle_id, config)


def _should_migrate_cycle(cycle_id: str):
    node = {'@type': NodeType.CYCLE.value, '@id': cycle_id}
    config = node_config(node)
    version = config.get('version') if config else None
    return all([
        os.path.exists(node_data_path(node, DataState.original, '')),
        # must have either no `config.json`, or no `version` inside, or the version is below `3`
        should_migrate_version(version, MIGRATE_ABOVE_VERSION)
    ])


def _migrate_cycles():
    ids = list(filter(_should_migrate_cycle, os.listdir(node_type_folder({'type': NodeType.CYCLE.value}))))
    for cycle_id in ids:
        try:
            _migrate_cycle(cycle_id)
        except Exception as e:
            print(NAME, 'Could not migrate Cycle with id', cycle_id, '- reason:', str(e))
            continue


def _migrate_draft_data(draft_id: str):
    filepath = draft_filepath(draft_id)
    print(NAME, 'Migrating draft:', filepath)
    nodes = read_json(filepath).get('nodes', [])
    impacts = []
    for node in nodes:
        impacts.append(node if node.get('type') == NodeType.IMPACTASSESSMENT.value else _impact_from_cycle(node))

    write_json(filepath, {'version': VERSION, 'nodes': impacts})


def _migrate_draft(draft_id: str):
    filepath = draft_filepath(draft_id)
    if os.path.exists(filepath):
        data = read_json(filepath)
        all_impacts = all([n.get('type') == NodeType.IMPACTASSESSMENT.value for n in data.get('nodes', [])])
        if not all_impacts:
            _migrate_draft_data(draft_id)


def _should_migrate_draft(draft_id: str):
    # only migrate when there is no `version` inside the data, or the version is below `3`
    filepath = draft_filepath(draft_id)
    version = (read_json(filepath) or {}).get('version')
    return should_migrate_version(version, MIGRATE_ABOVE_VERSION)


def _migrate_drafs():
    ids = os.listdir(DRAFT_BASE_FOLDER) if os.path.exists(DRAFT_BASE_FOLDER) else []
    ids = list(filter(_should_migrate_draft, ids))
    for draft_id in ids:
        try:
            _migrate_draft(draft_id)
        except Exception as e:
            print('Could not migrate Draft with id', draft_id, '- reason:', str(e))
            continue


def run():
    return [_migrate_cycles(), _migrate_drafs()]
