# make sure all Nodes have an "@type" and "@id" field
import os
from hestia_earth.schema import NodeType
from api_utils.utils import node_type_folder, node_folder, node_type, node_id, read_json, write_json

from .utils import NODE_TYPES

NAME = os.path.basename(__file__)


def _clean_node(node: dict):
    # make sure we use @type/@id notation when nodes are saved in their own folder
    node = {'@type': node_type(node), '@id': node_id(node), **node}
    if 'type' in node:
        del node['type']
    if 'id' in node:
        del node['id']
    return node


def _fix_data(filepath: str):
    if os.path.exists(filepath):
        data = read_json(filepath)

        if data.get('type'):
            print(NAME, 'Fixing data @type/@id', filepath)
            write_json(filepath, _clean_node(data))


def _fix_nodes_version(version_folder: str):
    return [
        _fix_data(os.path.join(version_folder, 'original.jsonld')),
        _fix_data(os.path.join(version_folder, 'recalculated.jsonld'))
    ]


def _fix_node(node_type: NodeType, node_id: str):
    base_folder = node_folder({'@type': node_type.value, '@id': node_id})
    versions = [
        f for f in os.listdir(base_folder) if os.path.isdir(os.path.join(base_folder, f))
    ] if os.path.exists(base_folder) and os.path.isdir(base_folder) else []
    return [_fix_nodes_version(os.path.join(base_folder, version)) for version in versions]


def _fix_nodes(node_type: NodeType):
    base_folder = node_type_folder({'@type': node_type.value})
    ids = os.listdir(base_folder) if os.path.exists(base_folder) else []
    return [_fix_node(node_type, node_id) for node_id in ids]


def run():
    return list(map(_fix_nodes, NODE_TYPES))
