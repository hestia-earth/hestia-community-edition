import os
from api_utils.utils import DATA_FOLDER

from .utils import move_file


def run():
    move_file(
        src=os.path.join(DATA_FOLDER, 'config', 'settings.json'),
        dest=os.path.join(DATA_FOLDER, 'settings.json')
    )
