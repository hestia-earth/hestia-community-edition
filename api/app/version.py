from fastapi import APIRouter
from hestia_earth.validation.version import VERSION as VALIDATION_VERSION
from api_utils.version import VERSION, get_versions

from .dockerhub import latest_version, is_prerelease

router = APIRouter(
    prefix='/version',
    tags=['Version'],
    responses={404: {'description': 'Not found'}},
)


@router.get('')
async def get_version():
    latest = latest_version()
    return get_versions() | {
        'latest': latest,
        'isPrerelease': is_prerelease(VERSION),
        'isLatest': VERSION == latest,
        'validation': VALIDATION_VERSION
    }
