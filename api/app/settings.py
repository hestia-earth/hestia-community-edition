from typing import Any, Dict
from fastapi import APIRouter, Body
from api_utils.settings_utils import load_settings, get_setting, update_all_settings, update_settings

router = APIRouter(
    prefix='/settings',
    tags=['Settings for UI'],
    responses={404: {'description': 'Not found'}},
)


@router.get('')
async def get_all():
    return load_settings()


@router.put('')
async def update_all(data: Dict[str, Any]):
    update_all_settings(data)
    return {'success': True}


@router.get('/{key}')
async def get_single(key: str):
    return get_setting(key)


@router.put('/{key}')
async def update_single(key: str, value: Any = Body(...)):
    update_settings(key, value)
    return {'success': True}
