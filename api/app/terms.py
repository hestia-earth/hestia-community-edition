import os
from fastapi import APIRouter
from api_utils.utils import read_json

TERM_DIR = 'Term'
router = APIRouter(
    prefix='/terms',
    tags=['Terms'],
    responses={404: {'description': 'Not found'}},
)


@router.get('/{id}')
async def get_term_by_id(id: str):
    path = os.path.join(TERM_DIR, f"{id}.jsonld")
    if os.path.exists(path):
        return read_json(path)
    else:
        return {'error': f"Term with id '{id}' not found"}, 404
