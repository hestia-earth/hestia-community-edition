from typing import Any, List, Optional
from enum import Enum
from pydantic import BaseModel, Field
from fastapi import APIRouter
from hestia_earth.earth_engine import run

router = APIRouter(
    prefix='/ee',
    tags=['Earth Engine'],
    responses={404: {'description': 'Not found'}},
)


class EEType(str, Enum):
    raster = 'raster'
    vector = 'vector'


class Reducer(str, Enum):
    mean = 'mean'
    mode = 'mode'
    first = 'first'
    sum = 'sum'


class Collection(BaseModel):
    collection: str
    name: Optional[str] = None
    fields: Optional[str] = None
    reducer: Optional[Reducer] = None
    band_name: Optional[str] = None
    reducer_annual: Optional[Reducer] = None
    reducer_period: Optional[Reducer] = None
    year: Optional[int] = None
    start_date: Optional[str] = None
    end_date: Optional[str] = None


class EEData(BaseModel):
    ee_type: EEType
    collections: List[Collection] = []


class GADM(EEData):
    gadm_ids: List[str] = Field(alias='gadm-ids')


@router.post('/gadm')
async def query_by_gadm_region(data: GADM):
    value = data.dict()
    value['gadm-ids'] = value.get('gadm-ids', value.get('gadm_ids'))
    return run(value)


class EECoordinates(BaseModel):
    latitude: float
    longitude: float


class Coordinates(EEData):
    coordinates: List[EECoordinates]


@router.post('/coordinates')
async def query_by_coordinates(data: Coordinates):
    return run(data.dict())


class Boundary(EEData):
    boundary: Any


@router.post('/boundary')
async def query_by_geojson(data: Boundary):
    return run(data.dict())
