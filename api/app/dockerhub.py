import requests

BASE_URL = 'https://hub.docker.com/v2/repositories/hestiae/ce-api'


def is_prerelease(version: str): return '-' in version


def _list_tags():
    results = requests.get(f"{BASE_URL}/tags?page_size=100", params={}).json().get('results', [])
    return [r.get('name') for r in results]


def latest_version():
    tags = _list_tags()
    # only keep the last tag which is not a `prerelease`
    return next((tag for tag in tags if tag != 'latest' and not is_prerelease(tag)))
