import traceback
from typing import Any, Dict, Optional
from hestia_earth.schema import NodeType
from fastapi import APIRouter, HTTPException, Header, BackgroundTasks
from fastapi.responses import PlainTextResponse
from api_utils.utils import DataState, log_filename, read_file, node_latest_version
from api_utils.calculate_utils import log_error

from .calculate_utils import calculate_in_background, calculate_async
from .node_utils import node_metadata, parse_missing_lookups, list_nodes, delete_node, get_node
from .list_utils import SortBy, SortOrder

NODE_TYPE = NodeType.SITE
router = APIRouter(
    prefix='/sites',
    tags=[NODE_TYPE],
    responses={404: {'description': 'Not found'}},
)


def _node_from_id(id: str): return {'@type': NODE_TYPE.value, '@id': id}


@router.get('')
async def list_sites(
    search: str = None, limit: int = 10, offset: int = 0,
    sortBy: SortBy = SortBy.createdOn, sortOrder: SortOrder = SortOrder.Descending
):
    return list_nodes(NODE_TYPE, search, limit, offset, sortBy, sortOrder)


@router.post('')
async def create_site(
    background_tasks: BackgroundTasks,
    data: Dict[str, Any],
    config: Optional[str] = Header(None, description='Name of the configuration for calculation.'),
    run_async: Optional[bool] = Header(default=False, description='Run calculations without using system.')
):
    try:
        if run_async:
            calculate_in_background(background_tasks, data, config)
            return {'success': True, 'tasks': background_tasks}
        else:
            return await calculate_async(data, config)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/{id}')
async def get_site(id: str, dataState: DataState = DataState.original, version: Optional[str] = ''):
    try:
        return get_node(NODE_TYPE, id, dataState, version)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/{id}/metadata')
async def get_site_metadata(id: str, version: Optional[str] = ''):
    try:
        return node_metadata(NODE_TYPE, id, version)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.delete('/{id}')
async def delete_site(id: str):
    return {'success': delete_node(id)}


@router.get('/{id}/log', response_class=PlainTextResponse)
async def get_site_log(id: str, version: Optional[str] = ''):
    try:
        return read_file(log_filename(_node_from_id(id), version))
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/{id}/log/lookups')
async def get_site_log_missing_lookups(id: str, version: Optional[str] = ''):
    try:
        logs = read_file(log_filename(_node_from_id(id), version))
        return parse_missing_lookups(logs)
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.post('/{id}/pipeline')
async def recalculate_site(
    id: str,
    background_tasks: BackgroundTasks,
    config: Optional[str] = Header(None, description='Name of the configuration for calculation.'),
    run_async: Optional[bool] = Header(default=False, description='Run calculations without using system.')
):
    node = get_node(NODE_TYPE, id, DataState.original, node_latest_version(_node_from_id(id)))
    if node:
        try:
            if run_async:
                calculate_in_background(background_tasks, node, config)
                return {'success': True, 'tasks': background_tasks}
            else:
                await calculate_async(node, config)
                return {'success': True}
        except Exception as e:
            stack = traceback.format_exc()
            log_error(node, str(e), stack)
            raise HTTPException(status_code=500, detail=stack)
    else:
        return {'success': False}
