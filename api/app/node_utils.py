import os
import json
import shutil
from functools import reduce
from hestia_earth.schema import NodeType, SchemaType, TermTermType
from api_utils.log import logger
from api_utils.calculate_utils import Types
from api_utils.utils import (
    DATA_FOLDER, DataState, Status,
    get_modified_at, error_filename, read_json, node_config, node_data_path, node_source_path, get_node_status,
    node_folder, node_versions, node_latest_version, node_exists
)

from .list_utils import SortBy, SortOrder, paginate, filter_by, order_by, remove_from_index, get_index


TERM_TYPES = [e.value for e in TermTermType]
SCHEMA_TYPES_LOWER = [e.value.lower() for e in SchemaType]
PREFIX = 'Missing lookup'


def _unique(values: list): return list({json.dumps(v): v for v in values}.values())


def _parse_filename(filepath: str):
    filename = filepath.split('.')[0]
    ext = '.xlsx' if filename in TERM_TYPES else '.csv'
    return filepath.replace('.csv', ext)


def _parse_lookup(data: dict):
    return {
        'filename': _parse_filename(data[PREFIX]),
        'termId': data.get('termid'),
        'column': data.get('column'),
        'model': data.get('model'),
        'term': data.get('term'),
        'key': data.get('key')
    }


def _csv_value(value: str): return (value or '').replace('[', '').replace(']', '')


def _parse_message_value(parts):
    try:
        [key, value] = parts.split('=')
        val = _csv_value(value)
        return {key.strip(): val} if key and val else {}
    except ValueError:
        return {}


def _parse_message(message: str):
    try:
        data = json.loads(message)
        logger = data.get('logger', '')
        parts = data.get('message', '').split(',')
        return reduce(lambda prev, curr: {**prev, **_parse_message_value(curr)}, parts, {'logger': logger})
    except json.decoder.JSONDecodeError:
        return {}


def parse_missing_lookups(data: str):
    lines = list(filter(lambda log: PREFIX in log, data.split('\n'))) if data else []
    messages = list(filter(lambda v: len(v.keys()) > 1, map(_parse_message, lines)))
    return _unique(list(map(_parse_lookup, messages)))


def _delete_node(node: dict):
    base_folder = node_folder(node)
    if os.path.exists(base_folder):
        shutil.rmtree(base_folder)
        return True
    return False


def delete_node(id: str):
    return any([
        remove_from_index(node_type, id) for node_type in Types
    ]) and any([
        _delete_node({'@type': node_type, '@id': id}) for node_type in Types
    ])


def get_node(type: NodeType, id: str, dataState: DataState, version: str = ''):
    node = {'@type': type.value, '@id': id}
    version = version or node_latest_version(node)
    return read_json(node_data_path(node, dataState, version))


def _count_progress(type: NodeType, ids: list):
    progress_statuses = [Status.queued.value, Status.started.value]
    return len([
        id for id in ids if get_node_status({'@type': type.value, '@id': id}) in progress_statuses
    ])


def node_metadata(type: NodeType, id: str, version: str = ''):
    node = {'@type': type.value, '@id': id}
    if not node_exists(node):
        return {}
    version = version or node_latest_version(node)
    source_path = node_source_path(node, version)
    return {
        'id': id,
        'status': get_node_status(node, version),
        'createdOn': get_modified_at(node_data_path(node, DataState.original, version)),
        'recalculatedOn': get_modified_at(node_data_path(node, DataState.recalculated, version)),
        'error': read_json(error_filename(node, version)),
        'config': node_config(node, version),
        'sourcePath': source_path.replace(DATA_FOLDER, '') if os.path.exists(source_path) else None,
        'versions': node_versions(node)
    }


def list_nodes(
    type: NodeType,
    search: str = None, limit: int = 10, offset: int = 0,
    sortBy: SortBy = SortBy.createdOn, sortOrder: SortOrder = SortOrder.Descending
):
    logger.debug('Getting %s from index...', type.value)
    nodes = get_index(type.value)
    logger.debug('Filtering by id')
    nodes = filter_by(nodes, 'id', search)
    # ids = [node.get('id') for node in nodes]

    count = len(nodes)

    logger.debug('Ordering results')
    nodes = order_by(nodes, sortBy, sortOrder)
    logger.debug('Paginating results')
    nodes = paginate(nodes, offset, limit)
    logger.debug('Reading metadata')
    results = [node_metadata(type, node.get('id')) for node in nodes]
    # TODO: disabled as too slow on file system
    # in_progress = _count_progress(type, ids)
    in_progress = 0
    return {'results': results, 'count': count, 'inProgress': in_progress}
