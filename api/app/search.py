import os
import aiohttp
from typing import Any, Dict
from fastapi import APIRouter

SEARCH_URL = os.getenv('SEARCH_URL')
API_SEARCH_URL = 'https://api.hestia.earth/search'
router = APIRouter(
    prefix='/search',
    tags=['Search HESTIA Terms'],
    responses={404: {'description': 'Not found'}},
)


async def proxy_search(data: Dict[str, Any]):
    async with aiohttp.ClientSession() as client:
        async with client.post(API_SEARCH_URL, json=data) as resp:
            return await resp.json()


async def es_search(data: Dict[str, Any]):
    fields = data.get('fields', [])
    params = {
        'from': int(data.get('offset', '0')),
        'size': int(data.get('limit', '10')),
        **({'_source': {'includes': fields}} if len(fields) > 0 else {}),
        'query': data.get('query', {})
    }
    async with aiohttp.ClientSession() as client:
        async with client.post(f"{SEARCH_URL}/hestia-data/_search", json=params) as resp:
            response = await resp.json()
            hits = response.get('hits', {})
            return {
                'time': response.get('took', 0),
                'count': hits.get('total', {}).get('value', 0),
                'results': [
                    {'_score': h.get('_score'), **h.get('_source')} for h in hits.get('hits', [])
                ]
            }


@router.post('')
async def search(data: Dict[str, Any]):
    return await es_search(data) if SEARCH_URL else await proxy_search(data)
