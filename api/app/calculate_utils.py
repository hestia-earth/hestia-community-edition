from concurrent.futures import ThreadPoolExecutor
import asyncio
from typing import Union, List, Dict
from fastapi import BackgroundTasks
from hestia_earth.schema import NodeType
from hestia_earth.utils.model import find_primary_product
from hestia_earth.utils.tools import non_empty_list, flatten
from api_utils.calculate_utils import (
    map_nodes_to_stages, calculate_nodes as run_calculate_nodes, calculate_node, save_node_data, delete_log_error
)
from api_utils.utils import (
    DataState, Status, node_folder, node_data_path, node_type, node_id, read_json, set_node_status, update_node_config
)
from api_utils.config_utils import DEFAULT_CONFIG
from api_utils.queue_utils import QueueAction, enabled as queue_enabled, publish_message
from api_utils.version import VERSION, SCHEMA_VERSION
from api_utils.log import logger

from .list_utils import add_to_index


def _linked_node(node): return {'@type': node_type(node), '@id': node_id(node)}


def _find_nodes_from_impact(data: dict):
    site = data.get('site')
    if site:
        data['site'] = _linked_node(site)

    cycle = data.get('cycle')
    if cycle:
        data['cycle'] = _linked_node(cycle)

    impact = data
    return non_empty_list([site, cycle, impact])


def _impact_from_cycle(cycle: dict):
    product = find_primary_product(cycle) or {}
    return {
        '@type': NodeType.IMPACTASSESSMENT.value,
        '@id': node_id(cycle),
        'cycle': _linked_node(cycle),
        'site': _linked_node(cycle.get('site')),
        'product': product.get('term'),
        'endDate': cycle.get('endDate'),
        'country': cycle.get('site', {}).get('country'),
        'functionalUnitQuantity': 1,
        'allocationMethod': 'none',
        'dataPrivate': True
    }


def _find_nodes_from_cycle(data: dict):
    site = data.get('site')
    if site:
        data['site'] = _linked_node(site)

    cycle = data

    # find the ImpactAssessment with the same id or create a new one
    impact = read_json(
        node_data_path(node_folder({'@type': NodeType.IMPACTASSESSMENT.value, '@id': node_id(data)}))
    ) or _impact_from_cycle(data)

    return non_empty_list([site, cycle, impact])


FIND_NODES_BY_TYPE = {
    NodeType.CYCLE.value: _find_nodes_from_cycle,
    NodeType.IMPACTASSESSMENT.value: _find_nodes_from_impact
}


def _find_nodes_from_data(data: dict):
    ntype = node_type(data)
    return FIND_NODES_BY_TYPE.get(ntype, lambda *args: [data])(data)


def _clean_node(node: dict):
    # make sure we use @type/@id notation when nodes are saved in their own folder
    node = {'@type': node_type(node), '@id': node_id(node), **node}
    if 'type' in node:
        del node['type']
    if 'id' in node:
        del node['id']
    return node


def _setup_calculation(node: dict, config: str, first_calculations: bool):
    node = _clean_node(node)
    save_node_data(node, DataState.original) if first_calculations else None
    add_to_index(node)
    # set the Node as in progress, will be removed when calculating
    set_node_status(node, Status.queued)
    update_node_config(node, {
        'config': config,
        'version': VERSION,
        'schema': SCHEMA_VERSION
    })
    # make sure we remove any previous errors
    delete_log_error(node)
    return node


def _map_nodes(nodes: list): return [{'@type': node_type(node), '@id': node_id(node)} for node in nodes]


def _calculate_in_background(
    background_tasks: BackgroundTasks, nodes: List[Dict], config: str, first_calculations: bool
):
    # set queued state and save config
    nodes = [_setup_calculation(node, config, first_calculations) for node in nodes]
    # only use type/id as calculations will read the Node first
    nodes = _map_nodes(nodes)

    if queue_enabled():
        logger.debug(f'Calculate {len(nodes)} nodes using runners')
        publish_message(QueueAction.calculate_nodes, {'nodes': nodes})
    else:
        logger.debug(f'Calculate {len(nodes)} nodes using background tasks')

        for stage, filtered_nodes in map_nodes_to_stages(nodes):
            for node in filtered_nodes:
                background_tasks.add_task(calculate_node, node=node, config=config, stage=stage)


def calculate_in_background(
    background_tasks: BackgroundTasks,
    data: Union[List[Dict], Dict],
    config: str = DEFAULT_CONFIG,
    ids_order: List[str] = [],
    first_calculations: bool = False
):
    """
    Calculate nodes asynchronously, using built-in background tasks or external queing system.

    Parameters
    ----------
    background_tasks : BackgroundTasks
        Built-in background tasks to use.
    data : Union[List[Dict], Dict]
        Single node or list of nodes to calculate.
    config : str, optional
        The configuration used to calculate.
    ids_order : List[str], optional
        List of node ids to calculate in a specific order. By default, calculate everything in no order.
    first_calculations : bool
        Is this the first time the calculations run. If true, data will be stored in original file.
    """
    nodes = data if isinstance(data, list) else [data]

    if len(ids_order) > 1:
        logger.debug("Running nodes in a predefined order")
        # calculate all nodes together in a specific order
        nodes_by_id = {node_id(node): node for node in nodes}
        nodes = flatten([_find_nodes_from_data(nodes_by_id.get(node_id)) for node_id in ids_order])
        _calculate_in_background(background_tasks, nodes, config, first_calculations)
    else:
        logger.debug("Running nodes in no specific order")
        for node in nodes:
            # combine with nested nodes into a single list of nodes to calculate
            nodes = _find_nodes_from_data(node)
            _calculate_in_background(background_tasks, nodes, config, first_calculations)


def _calculate_nodes(nodes: list, config: str, first_calculations: bool = False):
    logger.debug(f'Calculate {len(nodes)} nodes asynchronously')
    # set queued state and save config
    nodes = [_setup_calculation(node, config, first_calculations) for node in nodes]
    nodes = _map_nodes(nodes)
    return run_calculate_nodes(nodes)


async def calculate_async(
    data: Union[list, dict],
    config: str = DEFAULT_CONFIG,
    first_calculations: bool = False
):
    nodes = data if isinstance(data, list) else [data]
    nodes = flatten(list(map(_find_nodes_from_data, nodes)))
    with ThreadPoolExecutor() as pool:
        return await asyncio.get_event_loop().run_in_executor(pool, _calculate_nodes, nodes, config, first_calculations)
