import os
from functools import reduce
from enum import Enum
from api_utils.calculate_utils import Types
from api_utils.utils import node_type_folder, node_folder, node_type, node_id, get_created_at
from api_utils.log import logger


class SortBy(str, Enum):
    id = 'id'
    createdOn = 'createdOn'


class SortOrder(str, Enum):
    Ascending = 'asc'
    Descending = 'desc'


_INDEX = {}


def _index_node(node: dict):
    return {
        '@type': node_type(node),
        'id': node_id(node),
        'createdOn': get_created_at(node_folder(node))
    }


def _node_in_index(node: dict):
    return any([n for n in _INDEX.get(node_type(node), []) if n.get('id') == node_id(node)])


# building a simple index to list all results
def build_index():
    global _INDEX

    def build_index_type(index: dict, type: str):
        base_folder = node_type_folder({'@type': type})
        ids = os.listdir(base_folder) if os.path.exists(base_folder) else []

        nodes = [{'@type': type, 'id': id} for id in ids]
        return {**index, type: list(map(_index_node, nodes))}

    logger.debug('Building search index...')
    _INDEX = reduce(build_index_type, Types, {})
    logger.debug('Building search index done.')
    return _INDEX


def get_index(type: str) -> list: return _INDEX.get(type, [])


def add_to_index(node: dict):
    global _INDEX
    _INDEX[node_type(node)] = _INDEX.get(node_type(node), []) + (
        [] if _node_in_index(node) else [_index_node(node)]
    )
    return True


def remove_from_index(type: str, id: str):
    global _INDEX
    _INDEX[type] = [node for node in _INDEX.get(type, []) if node.get('id') != id]
    return True


def paginate(values: list, offset: int, limit: int): return values[offset:(offset+limit)]


def order_by(values: list, key: SortBy, order: SortOrder):
    values = sorted(values, key=lambda x: x.get(key) or ('' if key == SortBy.id else 0))
    return values if order == SortOrder.Ascending else list(reversed(values))


def filter_by(values: list, key: str, value: str):
    return list(filter(lambda v: value.lower() in v.get(key, '').lower(), values)) if value else values
