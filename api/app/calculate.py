import traceback
from typing import Dict, List, Union, Optional
from pydantic import BaseModel
from fastapi import APIRouter, HTTPException, Body, Header, BackgroundTasks
from api_utils.utils import node_config
from api_utils.config_utils import DEFAULT_CONFIG, load_config

from .calculate_utils import calculate_in_background, calculate_async

router = APIRouter(
    prefix='/recalculate',
    tags=['Calculation'],
    responses={404: {'description': 'Not found'}},
)


class CalculateReponse(BaseModel):
    success: bool


@router.post('')
async def recalculate(
    background_tasks: BackgroundTasks,
    nodes: Union[Dict, List[Dict]] = Body({}, description='Dict of keys (ids) and node as value.'),
    ids: Optional[str] = Header(default='', description='Comma-separated ids defining order of the calculations.'),
    config: Optional[str] = Header(DEFAULT_CONFIG, description='Name of the configuration for calculation.'),
    run_async: Optional[bool] = Header(default=False, description='Run calculations using queuing system.')
) -> CalculateReponse:
    try:
        node_ids = ids.split(',') if isinstance(ids, str) else (ids or [])

        if run_async:
            calculate_in_background(background_tasks, nodes, config, ids_order=node_ids, first_calculations=True)
        else:
            await calculate_async(nodes, config, first_calculations=True)

        return {'success': True}
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/config')
async def config(type: str, id: Optional[str], version: Optional[str] = ''):
    try:
        # id of the Node, used to get the config
        config = node_config({'@type': type, '@id': id}, version).get('config', DEFAULT_CONFIG)
        return load_config(type, config)
    except Exception:
        return load_config(type)
