from typing import Optional
from fastapi import APIRouter, HTTPException, File, Form, Header

from api_utils.version import VERSION
from api_utils.log import logger
from api_utils.utils import node_source_path
from api_utils.calculate_utils import Type

router = APIRouter(
    prefix='/upload',
    tags=['File upload'],
    responses={404: {'description': 'Not found'}},
)


@router.post('')
async def upload_file(file: bytes = File(...), type: Type = Form(...), id: str = Form(...)):
    file_location = node_source_path({'@type': type, '@id': id}, VERSION)
    logger.debug('saving file to %s', file_location)
    with open(file_location, "wb+") as f:
        f.write(file)


@router.post('/excel')
async def convert_excel_to_csv(
    file: bytes = File(...),
    sheet: Optional[str] = Header(None, description='Name of the sheet to convert to CSV. Empty for first sheet.')
):
    try:
        import pandas as pd
        return pd.read_excel(file, index_col=None, sheet_name=sheet or 0).to_csv(None, index=None)
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
