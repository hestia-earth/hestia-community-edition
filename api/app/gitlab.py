import aiohttp
from fastapi import APIRouter

_BASE_URL = 'https://gitlab.com/hestia-earth'

router = APIRouter(
    prefix='/gitlab',
    tags=['Gitlab proxy'],
    responses={404: {'description': 'Not found'}},
)


@router.get('/raw')
async def raw(repository: str, branch: str, path: str):
    url = f"{_BASE_URL}/{repository}/-/raw/{branch}/{path}"
    async with aiohttp.ClientSession() as client:
        async with client.get(url) as resp:
            return await resp.text()
