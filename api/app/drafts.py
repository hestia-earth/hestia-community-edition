import os
import json
import shutil
import traceback
from typing import Union, List, Optional
from pydantic import BaseModel
from fastapi import APIRouter, HTTPException, File, Form, UploadFile, Header, BackgroundTasks
from fastapi.responses import ORJSONResponse
from api_utils.utils import DATA_FOLDER, SOURCE_FILENAME, get_modified_at, node_folder, read_json, write_json
from api_utils.version import VERSION, SCHEMA_VERSION
from api_utils.config_utils import DEFAULT_CONFIG
from api_utils.drafts_utils import DRAFT_BASE_FOLDER, draft_folder, draft_filepath, draft_nodes, get_draft_status

from .list_utils import SortBy, SortOrder, paginate, order_by, filter_by
from .drafts_utils import validate_in_background
from .calculate_utils import calculate_in_background, calculate_async

router = APIRouter(
    prefix='/drafts',
    tags=['Drafts'],
    responses={404: {'description': 'Not found'}},
)


class SuccessReponse(BaseModel):
    success: bool


def _result(id: str):
    return {
        'id': id,
        'status': get_draft_status(id),
        'createdOn': get_modified_at(draft_filepath(id))
    }


@router.get('')
async def list_drafts(
    search: str = None, limit: int = 10, offset: int = 0,
    sortBy: SortBy = SortBy.createdOn, sortOrder: SortOrder = SortOrder.Descending
):
    ids = os.listdir(DRAFT_BASE_FOLDER) if os.path.exists(DRAFT_BASE_FOLDER) else []
    results = filter_by(list(map(_result, ids)), 'id', search)
    count = len(results)
    results = paginate(order_by(results, sortBy, sortOrder), offset, limit)
    return {'results': results, 'count': count}


@router.get('/{id}')
async def get_draft(id: str, include_nodes: Optional[bool] = True):
    filepath = draft_filepath(id)
    data = read_json(filepath)
    source_path = os.path.join(draft_folder(id), SOURCE_FILENAME)
    return _result(id) | {
        'version': data.get('version'),
        'validation': data.get('validation', {}),
        'sourcePath': source_path.replace(DATA_FOLDER, '') if os.path.exists(source_path) else None
    } | ({
        'nodes': data.get('nodes', [])
    } if include_nodes else {})


@router.get('/{id}/nodes', response_class=ORJSONResponse)
async def get_draft_nodes(id: str):
    return ORJSONResponse(draft_nodes(id=id))


@router.patch('/{id}/validate')
async def validate_draft(background_tasks: BackgroundTasks, id: str) -> SuccessReponse:
    try:
        validate_in_background(background_tasks, id)
        return {'success': True}
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.post('')
async def create_draft(file: bytes = File(...), id: str = Form(...)) -> SuccessReponse:
    file_location = os.path.join(draft_folder(id), SOURCE_FILENAME)
    with open(file_location, 'wb+') as f:
        f.write(file)
    return {'success': True}


@router.put('/{id}')
async def update_draft(id: str, file: UploadFile) -> SuccessReponse:
    filepath = draft_filepath(id)
    contents = await file.read()
    data = json.loads(contents)
    data['version'] = VERSION
    data['schema'] = SCHEMA_VERSION
    write_json(filepath, data)
    return {'success': True}


class DraftSubmit(BaseModel):
    type: str
    id: str


@router.post('/{id}')
async def submit_draft(id: str, data: Union[DraftSubmit, List[DraftSubmit]]):
    src_file = os.path.join(draft_folder(id), SOURCE_FILENAME)
    if os.path.exists(src_file):
        nodes = data if isinstance(data, list) else [data]
        for node in nodes:
            dest_folder = os.path.join(node_folder({'@type': node.type, '@id': node.id}), VERSION)
            os.makedirs(dest_folder, exist_ok=True)
            shutil.copyfile(
                src_file,
                os.path.join(dest_folder, SOURCE_FILENAME)
            )


class DraftCalculateReponse(BaseModel):
    success: bool


@router.post('/{id}/calculate')
async def calculate_draft(
    background_tasks: BackgroundTasks,
    id: str,
    ids: Optional[str] = Header(default='', description='Comma-separated ids defining order of the calculations.'),
    config: Optional[str] = Header(DEFAULT_CONFIG, description='Name of the configuration for calculation.'),
    run_async: Optional[bool] = Header(default=False, description='Run calculations using queuing system.')
) -> DraftCalculateReponse:
    try:
        nodes = draft_nodes(id=id)
        node_ids = ids.split(',') if isinstance(ids, str) else (ids or [])

        if run_async:
            calculate_in_background(background_tasks, nodes, config, ids_order=node_ids, first_calculations=True)
        else:
            await calculate_async(nodes, config, first_calculations=True)

        return {'success': True}
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.delete('/{id}')
async def delete_draft(id: str) -> SuccessReponse:
    base_folder = draft_folder(id)
    if os.path.exists(base_folder):
        shutil.rmtree(base_folder)
        return {'success': True}
    else:
        return {'success': False}
