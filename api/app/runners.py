import traceback
from fastapi import APIRouter, HTTPException
from api_utils.queue_utils import enabled, get_connections, get_queue_details

router = APIRouter(
    prefix='/runners',
    tags=['Calculation Runners'],
    responses={404: {'description': 'Not found'}},
)


@router.get('')
async def get_all():
    try:
        return get_connections()
    except Exception:
        stack = traceback.format_exc()
        raise HTTPException(status_code=500, detail=stack)


@router.get('/status')
async def get_status():
    return {'enabled': enabled()}


@router.get('/queue')
async def get_queue():
    return get_queue_details()
