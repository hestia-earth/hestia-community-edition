import os
from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from api_utils.mocking import enable_mock
from api_utils.utils import init_earth_engine

from .version import VERSION, router as version
from .migrations import run as run_migrations
from .list_utils import build_index

from .calculate import router as calculate
from .config import router as config
from .cycles import router as cycles
from .download import router as download
from .drafts import router as drafts
from .ee import router as ee
from .gitlab import router as gitlab
from .impactassessments import router as impactassessments
from .models import router as models
from .runners import router as runners
from .search import router as search
from .settings import router as settings
from .sites import router as sites
from .terms import router as terms
from .upload import router as upload
from .validate import router as validate

API_PATH = os.getenv('API_PATH', '/')  # the API path without proxy
BASE_URL = os.getenv('BASE_URL', '')  # the API path seen by client-side
VERSION_HEADER = 'X-API-VERSION'
PANDAS_VERSION_HEADER = 'X-PANDAS-VERSION'

api = FastAPI(root_path=BASE_URL)
api.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=[VERSION_HEADER, PANDAS_VERSION_HEADER]
)


@api.middleware('http')
async def add_version_header(request: Request, call_next):
    response = await call_next(request)
    response.headers[VERSION_HEADER] = VERSION
    return response


@api.middleware('http')
async def add_pandas_header(request: Request, call_next):
    response = await call_next(request)
    try:
        import pandas as pd
        response.headers[PANDAS_VERSION_HEADER] = pd.__version__
    except ImportError:
        pass
    return response


api.include_router(calculate)
api.include_router(config)
api.include_router(cycles)
api.include_router(download)
api.include_router(drafts)
api.include_router(ee)
api.include_router(gitlab)
api.include_router(impactassessments)
api.include_router(models)
api.include_router(runners)
api.include_router(search)
api.include_router(settings)
api.include_router(sites)
api.include_router(terms)
api.include_router(upload)
api.include_router(validate)
api.include_router(version)

if API_PATH != '/':
    app = FastAPI(root_path=BASE_URL)
    app.mount(API_PATH, api)
else:
    app = api


@app.on_event('startup')
async def startup_event():
    run_migrations()
    build_index()
    enable_mock()
    init_earth_engine()
