from fastapi import BackgroundTasks
from api_utils.log import logger
from api_utils.utils import Status
from api_utils.queue_utils import QueueAction, enabled as queue_enabled, publish_message
from api_utils.drafts_utils import validate_draft, set_draft_status, update_draft_data


def validate_in_background(background_tasks: BackgroundTasks, id: str):
    set_draft_status(id, Status.queued)
    update_draft_data(id, 'validation', {})

    if queue_enabled():
        logger.debug(f'Validating Draft {id} using runners')
        publish_message(QueueAction.validate_draft, {'id': id})
    else:
        logger.debug(f'Validating Draft {id} using background tasks')

        background_tasks.add_task(validate_draft, id=id)
