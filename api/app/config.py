from functools import reduce
import os
import shutil
from typing import Any, Dict
from fastapi import APIRouter
from api_utils.utils import get_modified_at, write_json
from api_utils.config_utils import (
    BASE_FOLDER, DEFAULT_CONFIG, config_path, load_config, load_config_with_default, config_base_folder
)
from api_utils.calculate_utils import Type

from .list_utils import SortBy, SortOrder, filter_by, order_by, paginate

router = APIRouter(
    prefix='/config',
    tags=['Config'],
    responses={404: {'description': 'Not found'}},
)


def list_files(folder: str): return [os.path.join(folder, f) for f in os.listdir(folder) if not os.path.isdir(f)]


def last_modified_at(files: str):
    return sorted([get_modified_at(f) for f in files])[0]


def list_folders(folder: str):
    os.makedirs(folder, exist_ok=True)
    folders = [f for f in os.listdir(folder) if os.path.isdir(os.path.join(folder, f))]
    return [
        {
            'id': f,
            'createdOn': last_modified_at(list_files(os.path.join(folder, f)))
        } for f in folders
    ]


@router.get('')
async def list_configs(
    search: str = None, limit: int = 10, offset: int = 0,
    sortBy: SortBy = SortBy.id, sortOrder: SortOrder = SortOrder.Descending
):
    results = [{'id': DEFAULT_CONFIG, 'createdOn': ''}] + list_folders(BASE_FOLDER)
    results = filter_by(results, 'id', search)
    count = len(results)
    results = paginate(order_by(results, sortBy, sortOrder), offset, limit)
    return {'results': results, 'count': count}


@router.get('/{id}')
async def get_configs(id: str):
    def load(prev: dict, type: Type):
        config = load_config_with_default(type.value, id)
        return prev | {type.value: config} if config else prev

    return reduce(load, Type, {})


@router.get('/{id}/{type}')
async def get_config(id: str, type: Type):
    return load_config_with_default(type, id)


@router.post('/{id}')
async def create_config(id: str):
    # cannot create default config
    if id == DEFAULT_CONFIG:
        return {'success': False}

    os.makedirs(config_base_folder(id), exist_ok=True)
    for type in Type:
        config = load_config(type)
        os.makedirs(config_base_folder(id), exist_ok=True)
        if config:
            write_json(filepath=os.path.join(config_base_folder(id), f"{type}.json"), data=config)
    return {'success': True}


@router.put('/{id}/{type}')
async def update_config(id: str, type: Type, data: Dict[str, Any]):
    # cannot edit default config
    if id == DEFAULT_CONFIG:
        return {'success': False}

    write_json(config_path(type, id), data)
    return {'success': True}


@router.delete('/{id}')
async def delete_config(id: str):
    folder = config_base_folder(id)
    # cannot delete default config
    if id != DEFAULT_CONFIG and os.path.exists(folder):
        shutil.rmtree(folder)
        return {'success': True}

    return {'success': False}
