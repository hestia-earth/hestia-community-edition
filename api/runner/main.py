import sys
import json
import functools
import threading
from hestia_earth.utils.tools import current_time_ms
from api_utils.log import logger
from api_utils.utils import init_earth_engine
from api_utils.mocking import enable_mock
from api_utils.queue_utils import QUEUE_CHANNEL, QueueAction, get_connection, get_channel
from api_utils.calculate_utils import calculate_nodes
from api_utils.drafts_utils import validate_draft


_PROCESS_MESSAGE = {
    QueueAction.calculate_nodes.value: lambda data: calculate_nodes(data.get('nodes', [])),
    QueueAction.validate_draft.value: lambda data: validate_draft(data.get('id'))
}


def ack_message(channel, delivery_tag):
    if channel.is_open:
        channel.basic_ack(delivery_tag)


def do_work(connection, channel, delivery_tag, body):
    message = json.loads(body)
    action = message.get('action')
    data = message.get('data', {})
    logger.info(f"Processing {action} message.")
    now = current_time_ms()
    _PROCESS_MESSAGE[action](data)
    logger.info(f"Finished processing {action} message in {current_time_ms() - now} ms.")
    cb = functools.partial(ack_message, channel, delivery_tag)
    connection.add_callback_threadsafe(cb)


def on_message(channel, method_frame, header_frame, body, args):
    (connection, threads) = args
    delivery_tag = method_frame.delivery_tag
    t = threading.Thread(target=do_work, args=(connection, channel, delivery_tag, body))
    t.start()
    threads.append(t)


def main():
    connection = get_connection()
    channel = get_channel(connection)
    channel.basic_qos(prefetch_count=1)

    init_earth_engine()
    enable_mock()

    threads = []
    on_message_callback = functools.partial(on_message, args=(connection, threads))
    channel.basic_consume(QUEUE_CHANNEL, on_message_callback=on_message_callback)

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    # Wait for all to complete
    for thread in threads:
        thread.join()

    connection.close()

    return 1


if __name__ == "__main__":
    sys.exit(main())
