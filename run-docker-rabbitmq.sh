#!/bin/sh

set -e

docker run --rm \
  --name rabbitmq \
  --env RABBITMQ_DEFAULT_VHOST="/" \
  --env RABBITMQ_DEFAULT_USER=guest \
  --env RABBITMQ_DEFAULT_PASS=guest \
  -p "5672:5672" -p "15672:15672" \
  rabbitmq:3.11-management
