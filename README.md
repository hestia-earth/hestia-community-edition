# HESTIA Community Edition

> Community Edition of the HESTIA Calculations

Provides a self-hosted installation of everything you need to run calculations on your server or computer.

## Getting started

### Prerequisites

You will need to install the following softwares:
1. Python `3` (we recommend using Python `3.6` minimum)
2. Make sure the following tools are installed:
    - [curl](https://www.tecmint.com/install-curl-in-linux/)
    - [unzip](https://www.tecmint.com/install-zip-and-unzip-in-linux/)
    - [jq](https://stedolan.github.io/jq/download/)

### Install

Run the `install.sh` script and choose your answer when prompted:
```bash
$ ./install.sh
```

**Note**: you will be prompted if you want to use the [HESTIA Earth Engine library](https://gitlab.com/hestia-earth/hestia-earth-engine).
This is an additional tool that will allow you to gap-fill geographical information about your [Site](https://www.hestia.earth/schema/Site).
If you want to use it, please follow the [Getting Started instructions](https://gitlab.com/hestia-earth/hestia-earth-engine#getting-started) after installation is complete, and add the `credentials.json` file under `ee` folder.

### Running calculations using a Queuing System

By default, calculations are running without a queuing system, which means:
- Long running calculations may be terminated by the server if they exceed the request timeout
- When an upload contain multiple calculations, these calculations would need to be executed manually one after another to guarantee dependencies.

To resolve those issues, you can enable "Queuing System" in the Settings of your Community Edition instance which will by default use the integrated [Background Tasks](https://fastapi.tiangolo.com/tutorial/background-tasks/), that queue up the calculations in the order they have been triggered.

However we strongly recommend using [RabbitMQ](https://www.rabbitmq.com/) and starting multiple [runners](./api/runner), each able to calculate nodes in a specific order.

For a complete example, please look at the [docker-compose.yml](docker-compose.yml) file.

#### RabbitMQ and runner setup

1. Install [RabbitMQ management version 3](https://www.rabbitmq.com/download.html) and make sure the ports `5672` and `15672` are exposed to the API and runners.
2. Connect your API to RabbitMQ using the following env variable:
```
# replace "localhost" with the correct host
AMQP_URL=amqp://guest:guest@localhost:5672/
```
3. Copy the env variables use by the API to the runners, as they will need to access the same files.

### Using Docker and Docker Compose

1. Create a `.env` file with the following content:
```
# change this to set the maximum size of the site that can be gap-filled. Using a very high value will slow down calculations considerably or may even result in errors
MAX_AREA_SIZE=100000
# this variable is only needed when using the HESTIA Earth Engine library
# points to the credentials.json file retrieve from Google Cloud, could be placed anywhere a mount point is available
EARTH_ENGINE_KEY_FILE=/app/ee/credentials.json
```
2. Run `docker-compose up --build`
3. Open a browser on http://localhost

Alternatively, you can also use the [HESTIA's official images](https://hub.docker.com/u/hestiae) by running:
```bash
$ docker-compose -f docker-compose.prod.yml up
```

#### Using the latest tag

We provide [stable releases](https://gitlab.com/hestia-earth/hestia-community-edition/-/releases) on a regular basis. Both `hestiae/ce-api` and `hestiae/ce-ui` need to be used with the same tag.

If you want to use unstable releases that are published as soon as new features are added, you can pull the `latest` tag instead, but we can not guarantee everything will work smoothly. Thefore we do **not** recommend this in a production environment.

### Using the ecoinventV3 model

ecoinvent is a consistent, transparent, and well validated life cycle inventory database.
We use ecoinvent data to ascertain the environmental impacts of activities that occur outside of our system boundary, for example data on the environmental impacts of extracting oil and producing diesel, or the impacts of manufacturing plastics.

The `ecoinventV3` model requires a valid license to run. We are currently working on a way to enable community edition users with a licence to run these models themselves, but for now, these models are only available on the public platform.

## Contributing

If you wish to contribute, please read our [contributing guidelines](./CONTRIBUTING.md) first.
