#!/bin/bash

set -e

# use staging which has the latest terms
API_URL="https://api-staging.hestia.earth"
BACKUP_DATE="${1}"

DEST_DIR="Term"
mkdir -p $DEST_DIR

# Download required files
RES=$(curl -s -X GET "${API_URL}/terms/backups?date=${BACKUP_DATE}" -H "Content-Type: application/json")
BACKUPS=$(jq -rc '.[]' <<< "${RES}")

for BACKUP in $BACKUPS
do
  TERM_TYPE=$(jq -rc '.termType' <<< "${BACKUP}")
  URL=$(jq -rc '.url' <<< "${BACKUP}")
  ZIP_FILE="$TERM_TYPE.zip"
  if [ "$URL" == "null" ]; then
    echo "Error: could not download required termType: ${TERM_TYPE} on ${API_URL}/terms/backups/${TERM_TYPE}"
  else
    curl -s -L "${URL}" -o $ZIP_FILE
    unzip -o -qq $ZIP_FILE -d .
    rm -f $ZIP_FILE
  fi
done
