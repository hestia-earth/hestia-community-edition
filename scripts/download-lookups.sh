#!/bin/bash

set -e

WEB_URL="https://www-staging.hestia.earth"
BACKUP_DATE="${1}"

DEST_DIR="glossary"
mkdir -p $DEST_DIR
cd $DEST_DIR

if [ "$BACKUP_DATE" = "" ]
then
  URL="${WEB_URL}/${DEST_DIR}/lookups/all.zip"
else
  URL="${WEB_URL}/${DEST_DIR}/lookups/${BACKUP_DATE}/all.zip"
fi

ZIP_FILE="all.zip"
curl -s -L $URL -o $ZIP_FILE
unzip -o -qq $ZIP_FILE -d .
rm -f $ZIP_FILE
