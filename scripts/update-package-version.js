const { readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const [suffix, date] = process.argv.slice(2);
const versionDate = date ? date.substring(0, 10) : '';

const ROOT = resolve(join(__dirname, '../'));
const packageVersion = require(join(ROOT, 'package.json')).version;
const version = [
  packageVersion,
  suffix,
  versionDate
].filter(Boolean).join('-');

// api
const PYTHON_VERSION_PATH = resolve(join(ROOT, 'api', 'api_utils', 'version.py'));
let content = readFileSync(PYTHON_VERSION_PATH, 'UTF-8');
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
// update version date
content = content.replace(/VERSION_DATE\s=\s\'[\d\-]+\'/, `VERSION_DATE = '${versionDate}'`);
writeFileSync(PYTHON_VERSION_PATH, content, 'UTF-8');

// ui
const JS_VERSION_PATH = resolve(join(ROOT, 'ui', 'package.json'));
content = require(JS_VERSION_PATH);
content.version = `${version}`;
writeFileSync(JS_VERSION_PATH, JSON.stringify(content, null, 2), 'UTF-8');
const JS_LOCK_VERSION_PATH = resolve(join(ROOT, 'ui', 'package-lock.json'));
content = require(JS_LOCK_VERSION_PATH);
content.version = `${version}`;
writeFileSync(JS_LOCK_VERSION_PATH, JSON.stringify(content, null, 2), 'UTF-8');
const ENV_PROD_PATH = resolve(join(ROOT, 'ui', 'src', 'environments', 'environment.prod.ts'));
content = readFileSync(ENV_PROD_PATH, 'UTF-8');
content = content.replace(/version: \'(.*)\'/, `version: '${version}'`);
writeFileSync(ENV_PROD_PATH, content, 'UTF-8');
const ENV_PATH = resolve(join(ROOT, 'ui', 'src', 'environments', 'environment.ts'));
content = readFileSync(ENV_PATH, 'UTF-8');
content = content.replace(/version: \'(.*)\'/, `version: '${version}'`);
writeFileSync(ENV_PATH, content, 'UTF-8');
