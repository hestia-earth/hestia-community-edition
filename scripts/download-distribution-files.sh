#!/bin/bash

set -e

# use staging which has the latest terms
API_URL="https://api-staging.hestia.earth"

DEST_DIR="distribution"
rm -rf $DEST_DIR
mkdir -p $DEST_DIR

RES=$(curl -s -X GET "$API_URL/distribution/files" -H "Content-Type: application/json")

download_files() {
  FILES=$(jq -rc ".$1[]" <<< "${RES}")
  FILES_DIR="$2"

  arr=(${FILES})
  echo "Downloading ${#arr[@]} files to $2 folder"

  for file in $FILES
  do
    NAME=$(jq -rc '.name' <<< "${file}")
    URL=$(jq -rc '.url' <<< "${file}")
    mkdir -p "${DEST_DIR}/${FILES_DIR}"
    curl -s -L "${URL}" -o "${DEST_DIR}/${FILES_DIR}/${NAME}"
  done
}

download_files 'priorFiles' 'prior_files'
download_files 'posteriorFiles' 'posterior_files'
