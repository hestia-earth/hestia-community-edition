const { readdirSync, readFileSync, writeFileSync } = require('fs');
const { join } = require('path');

const [folder] = process.argv.slice(2);

const files = readdirSync(folder).map(f => join(folder, f));

const indexableFileContent = (file) => {
  const data = JSON.parse(readFileSync(file, 'utf8'));
  const indexableContent = {
    ...data,
    nameSearchAsYouType: data.name,
    nameNormalized: data.name,
    ...(data.latitude && data.longitude ? { location: { lat: data.latitude, lon: data.longitude } } : {})
  };
  writeFileSync(file, JSON.stringify(indexableContent, null, 2), 'utf8');
};

files.map(indexableFileContent);
