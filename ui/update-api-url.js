const { readFileSync, writeFileSync } = require('fs');

const [ filePath, apiUrl ] = process.argv.slice(2);

const env = readFileSync(filePath, 'utf8');
writeFileSync(filePath, env.replace('http://localhost:3000', apiUrl), 'utf8');
