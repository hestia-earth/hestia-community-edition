const { readdirSync, readFileSync, writeFileSync } = require('fs');
const { join, resolve } = require('path');

const ROOT = resolve(__dirname);
const encoding = 'utf8';
const fixturesFolder = join(ROOT, '../', 'fixtures', 'results');

const randomizeFields = [
  'value'
];

const removeFields = [
  'latitude', 'longitude', 'region', 'boundary'
];

const randomizeValue = (value) =>
  Array.isArray(value) ? [10] : 10;

const randomizeData = (content) =>
  typeof content === 'object'
    ? Array.isArray(content)
      ? content.map(randomizeData)
      : Object.fromEntries(
        Object.entries(content).map(([key, value]) => [
          key,
          randomizeFields.includes(key)
            ? randomizeValue(value)
            : removeFields.includes(key)
              ? undefined
              : typeof value === 'object'
                ? Array.isArray(value)
                  ? value.map(randomizeData)
                  : randomizeData(value)
                : value
        ])
      )
    : content;

const randomizeFile = (filepath) => {
  try {
    const { nodes } = JSON.parse(readFileSync(filepath, encoding));
    writeFileSync(filepath, JSON.stringify({ nodes: nodes.map(randomizeData) }, null, 2), encoding);
  }
  catch (err) {
    console.error('Could not randomize', filepath, 'error: ', err.message);
  }
};

const run = () => {
  const folders = readdirSync(fixturesFolder);
  return folders.map(folder => [
    randomizeFile(join(fixturesFolder, folder, 'source.json')),
    randomizeFile(join(fixturesFolder, folder, 'results.json')),
  ]);
};

run();
