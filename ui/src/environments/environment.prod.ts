import { IEnv } from './environment.interface';

export const environment: IEnv = {
  production: true,
  version: '4.45.0',
  apiUrl: '/api'
};
