export interface IEnv {
  production: boolean;
  version: string;
  apiUrl: string;
}
