import { IEnv } from './environment.interface';

export const environment: IEnv = {
  production: false,
  version: '4.45.0',
  apiUrl: 'http://localhost:3000'
};
