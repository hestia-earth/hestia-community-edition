import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { NodeType } from '@hestia-earth/schema';
import { filterParams } from '@hestia-earth/ui-components';
import { IOrchestratorConfig } from '@hestia-earth/engine-models';

import { environment } from '../environments/environment';
import { ISearchParams } from './api.service';

const path = `${environment.apiUrl}/config`;
export const defaultConfigId = 'default';

export interface IConfigListResult {
  id: string;
  createdOn: string;
}

export class ConfigListResult {
  results: IConfigListResult[] = [];
  count = 0;
}

export interface IConfig {
  type: NodeType;
  hasOverride: boolean;
  updatedOn?: number;
  config: IOrchestratorConfig;
  default: IOrchestratorConfig;
}

export interface IConfigs {
  [NodeType.Cycle]: IConfig;
  [NodeType.ImpactAssessment]: IConfig;
  [NodeType.Site]: IConfig;
}

@Injectable({
  providedIn: 'root'
})
export class ApiConfigService {
  private readonly http = inject(HttpClient);

  public listConfigs$(params: ISearchParams = {}) {
    return this.http.get<ConfigListResult>(path, {
      params: filterParams(params)
    });
  }

  public async listConfigs(params: ISearchParams = {}) {
    return await this.listConfigs$(params).toPromise();
  }

  public getConfigs$(id: string) {
    return this.http.get<IConfigs>(`${path}/${id}`);
  }

  public async getConfigs(id: string) {
    return await this.getConfigs$(id).toPromise();
  }

  public getConfig$(id: string, type = NodeType.Cycle) {
    return this.http.get<IConfig>(`${path}/${id}/${type}`);
  }

  public async getConfig(id: string, type = NodeType.Cycle) {
    return await this.getConfig$(id, type).toPromise();
  }

  public async createConfig(id: string) {
    return await this.http.post<{ success: boolean }>(`${path}/${id}`, {}).toPromise();
  }

  public updateConfig$(id: string, type = NodeType.Cycle, config: IOrchestratorConfig) {
    return this.http.put<{ success: boolean }>(`${path}/${id}/${type}`, config);
  }

  public async updateConfig(id: string, type = NodeType.Cycle, config: IOrchestratorConfig) {
    return await this.updateConfig$(id, type, config).toPromise();
  }

  public async deleteConfig(id: string) {
    return await this.http.delete<{ success: boolean }>(`${path}/${id}`).toPromise();
  }
}
