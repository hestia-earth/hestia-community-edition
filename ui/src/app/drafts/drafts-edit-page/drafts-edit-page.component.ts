import { Component, computed, inject, signal } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { distinctUntilChanged, filter, map, mergeMap, tap } from 'rxjs/operators';
import { EllipsisPipe } from '@hestia-earth/ui-components';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbDropdownModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../../../environments/environment';
import { ApiVersionService, versionUrl } from '../../api-version.service';
import { ApiDraftsService } from '../api-drafts.service';
import { TitleComponent } from '../../shared/title/title.component';
import { DraftsNewFormComponent } from '../drafts-new-form/drafts-new-form.component';
import { DraftsDeleteConfirmComponent } from '../drafts-delete-confirm/drafts-delete-confirm.component';

@Component({
  selector: 'app-drafts-edit-page',
  templateUrl: './drafts-edit-page.component.html',
  styleUrls: ['./drafts-edit-page.component.scss'],
  standalone: true,
  imports: [
    FormsModule,
    RouterLink,
    FaIconComponent,
    NgbDropdownModule,
    EllipsisPipe,
    TitleComponent,
    DraftsNewFormComponent
  ]
})
export class DraftsEditPageComponent {
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly draftsService = inject(ApiDraftsService);
  private readonly versionService = inject(ApiVersionService);
  private readonly modalService = inject(NgbModal);

  protected readonly loading = signal(true);

  private readonly id$ = this.route.params.pipe(map(({ id }) => id as string));
  protected readonly id = toSignal(this.id$);
  protected readonly draft = toSignal(
    this.id$.pipe(
      distinctUntilChanged(),
      filter(v => !!v),
      tap(() => this.loading.set(true)),
      mergeMap(id => this.draftsService.get$(id)),
      tap(() => this.loading.set(false))
    )
  );

  private readonly sourcePath = computed(() =>
    this.draft()?.sourcePath ? encodeURIComponent(this.draft()!.sourcePath!) : null
  );
  protected readonly sourceUrl = computed(() =>
    this.sourcePath() ? `${environment.apiUrl}/download?path=${this.sourcePath()}` : undefined
  );

  protected readonly nodes = computed(() => this.draft()?.nodes ?? []);
  protected readonly nodesUrl = computed(() =>
    this.nodes()?.length ? `${environment.apiUrl}/drafts/${this.id()}/nodes` : undefined
  );

  private readonly currentVersion = toSignal(this.versionService.versions$.pipe(map(({ current }) => current)));
  protected readonly version = computed(() => this.draft()?.version);
  protected readonly isCurrentVersion = computed(() => this.version() === this.currentVersion());

  protected readonly versionUrl = versionUrl;

  protected showDelete() {
    const instance = this.modalService.open(DraftsDeleteConfirmComponent);
    const component = instance.componentInstance as DraftsDeleteConfirmComponent;
    component.id.set(this.id());
    instance.closed.subscribe((confirmed: boolean) => confirmed && this.router.navigate(['/drafts']));
  }
}
