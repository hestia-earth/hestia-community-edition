import { Component } from '@angular/core';
import { RouterLink } from '@angular/router';
import { v4 as uuidv4 } from 'uuid';
import {
  JSON as HestiaJson,
  Cycle,
  ImpactAssessment,
  SchemaType,
  Site,
  ImpactAssessmentAllocationMethod
} from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';

import { IDraft } from '../api-drafts.service';
import { TitleComponent } from '../../shared/title/title.component';
import { DraftsNewFormComponent } from '../drafts-new-form/drafts-new-form.component';

const defaultSiteData = (id: string): Site => ({
  type: SchemaType.Site,
  id,
  dataPrivate: true,
  country: {
    type: SchemaType.Term,
    name: ''
  },
  region: {
    type: SchemaType.Term,
    name: ''
  }
});

const defaultCycleData = (id: string): Cycle => ({
  type: SchemaType.Cycle,
  id,
  dataPrivate: true,
  endDate: `${new Date().getFullYear()}`,
  site: {
    type: SchemaType.Site,
    id
  },
  products: [
    {
      type: SchemaType.Product,
      primary: true,
      economicValueShare: 100,
      value: [1],
      term: {
        type: SchemaType.Term,
        name: ''
      }
    }
  ],
  inputs: [
    {
      type: SchemaType.Input,
      value: [1],
      term: {
        type: SchemaType.Term,
        name: ''
      }
    }
  ]
});

const defaultImpactData = (id: string): ImpactAssessment => ({
  type: SchemaType.ImpactAssessment,
  id,
  dataPrivate: true,
  country: {
    type: SchemaType.Term,
    name: ''
  },
  cycle: defaultCycleData(id),
  site: defaultSiteData(id),
  allocationMethod: ImpactAssessmentAllocationMethod.economic,
  endDate: `${new Date().getFullYear()}`,
  functionalUnitQuantity: 1,
  product: {
    type: SchemaType.Product,
    value: [1],
    term: {
      type: SchemaType.Term,
      name: ''
    }
  }
});

@Component({
  selector: 'app-drafts-new-page',
  templateUrl: './drafts-new-page.component.html',
  styleUrls: ['./drafts-new-page.component.scss'],
  standalone: true,
  imports: [RouterLink, FaIconComponent, TitleComponent, DraftsNewFormComponent]
})
export class DraftsNewPageComponent {
  protected readonly nodes: HestiaJson<SchemaType>[] = [defaultImpactData(`draft-${new Date().getTime()}`)];
  protected readonly draft: Partial<IDraft> = {
    id: `draft-${uuidv4()}`
  };
}
