import { Route } from '@angular/router';

import { DraftsEditPageComponent } from './drafts-edit-page/drafts-edit-page.component';
import { DraftsNewPageComponent } from './drafts-new-page/drafts-new-page.component';
import { DraftsUploadPageComponent } from './drafts-upload-page/drafts-upload-page.component';
import { DraftsListPageComponent } from './drafts-list-page/drafts-list-page.component';

export default [
  {
    path: '',
    component: DraftsListPageComponent
  },
  {
    path: 'new',
    component: DraftsNewPageComponent
  },
  {
    path: 'upload',
    component: DraftsUploadPageComponent
  },
  {
    path: ':id',
    component: DraftsEditPageComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
] satisfies Route[];
