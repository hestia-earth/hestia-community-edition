import { NodeType, SchemaType, JSON as HestiaJson } from '@hestia-earth/schema';

import { readFixture } from '../../test-utils';
import {
  findDeepTerms,
  deepReplaceTerms,
  mergeNodesByImpactAssessment,
  buildDependencyTree,
  getIdsFromDependencyTree,
  IDdependencyTree
} from './drafts.model';

const fixtureFolder = 'drafts';

describe('drafts > drafts.model', () => {
  const node = {
    term: { '@type': NodeType.Term, name: 'term1' },
    practices: [{ '@type': SchemaType.Practice, term: { '@type': NodeType.Term, name: 'term2' } }],
    region: { '@type': NodeType.Term, name: 'term3' },
    product: {
      '@type': SchemaType.Product,
      term: { '@type': NodeType.Term, name: 'term 4' }
    }
  };

  describe('findDeepTerms', () => {
    it('should find all the Term name', () => {
      expect(findDeepTerms(node)).toEqual(['term1', 'term2', 'term3']);
    });
  });

  describe('deepReplaceTerms', () => {
    const termsMapping: any = {
      term1: { '@type': NodeType.Term, name: 'term1-updated' },
      'term 4': { '@type': NodeType.Term, name: 'term4-updated' }
    };

    it('should replace all matching terms', () => {
      expect(deepReplaceTerms(node, termsMapping)).toEqual({
        term: { '@type': NodeType.Term, name: 'term1-updated' },
        practices: [{ '@type': SchemaType.Practice, term: { '@type': NodeType.Term, name: 'term2' } }],
        region: { '@type': NodeType.Term, name: 'term3' },
        product: {
          '@type': SchemaType.Product,
          term: { '@type': NodeType.Term, name: 'term4-updated' }
        }
      });
    });
  });

  describe('mergeNodesByImpactAssessment', () => {
    let source: any;
    let expected: any;
    let results: any[];

    describe('pork', () => {
      const testFolder = `${fixtureFolder}/pork`;

      beforeAll(async () => {
        source = await readFixture(`${testFolder}/source.json`);
        expected = (await readFixture(`${testFolder}/results.json`)).nodes;
        results = mergeNodesByImpactAssessment(source, 0, '2020-01-01');
      });

      it('should return 19 ImpactAssessment', () => {
        expect(results).toEqual(expected);
      });
    });

    describe('carrots', () => {
      const testFolder = `${fixtureFolder}/carrots`;

      beforeAll(async () => {
        source = await readFixture(`${testFolder}/source.json`);
        expected = (await readFixture(`${testFolder}/results.json`)).nodes;
        results = mergeNodesByImpactAssessment(source, 0, '2020-01-01');
      });

      it('should return 1 ImpactAssessment', () => {
        expect(results).toEqual(expected);
      });
    });
  });

  describe('buildDependencyTree', () => {
    let nodes: HestiaJson<SchemaType>[];
    let expected: any;

    describe('pork', () => {
      const testFolder = `${fixtureFolder}/pork`;

      beforeAll(async () => {
        nodes = (await readFixture(`${testFolder}/results.json`)).nodes;
        expected = await readFixture(`${testFolder}/dependencies.json`);
      });

      it('should build the dependency tree', () => {
        expect(buildDependencyTree(nodes)).toEqual(expected);
      });
    });

    describe('carrots', () => {
      const testFolder = `${fixtureFolder}/carrots`;

      beforeAll(async () => {
        nodes = (await readFixture(`${testFolder}/results.json`)).nodes;
        expected = await readFixture(`${testFolder}/dependencies.json`);
      });

      it('should build the dependency tree', () => {
        expect(buildDependencyTree(nodes)).toEqual(expected);
      });
    });

    describe('chicken', () => {
      const testFolder = `${fixtureFolder}/chicken`;

      beforeAll(async () => {
        nodes = (await readFixture(`${testFolder}/results.json`)).nodes;
        expected = await readFixture(`${testFolder}/dependencies.json`);
      });

      it('should build the dependency tree', () => {
        expect(buildDependencyTree(nodes)).toEqual(expected);
      });
    });
  });

  describe('getIdsFromDependencyTree', () => {
    let dependencies: IDdependencyTree;
    let expected: string[][];

    describe('pork', () => {
      const testFolder = `${fixtureFolder}/pork`;

      beforeAll(async () => {
        dependencies = await readFixture(`${testFolder}/dependencies.json`);
        expected = (await readFixture(`${testFolder}/ids.json`)).ids;
      });

      it('should return the list of ids', () => {
        expect(getIdsFromDependencyTree(dependencies)).toEqual(expected);
      });
    });

    describe('carrots', () => {
      const testFolder = `${fixtureFolder}/carrots`;

      beforeAll(async () => {
        dependencies = await readFixture(`${testFolder}/dependencies.json`);
        expected = (await readFixture(`${testFolder}/ids.json`)).ids;
      });

      it('should return the list of ids', () => {
        expect(getIdsFromDependencyTree(dependencies)).toEqual(expected);
      });
    });

    describe('chicken', () => {
      const testFolder = `${fixtureFolder}/chicken`;

      beforeAll(async () => {
        dependencies = await readFixture(`${testFolder}/dependencies.json`);
        expected = (await readFixture(`${testFolder}/ids.json`)).ids;
      });

      it('should return the list of ids', () => {
        expect(getIdsFromDependencyTree(dependencies)).toEqual(expected);
      });
    });
  });
});
