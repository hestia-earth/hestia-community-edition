import { Component, computed, effect, inject, input, model, signal, viewChildren } from '@angular/core';
import { NgTemplateOutlet, SlicePipe } from '@angular/common';
import { Router, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { combineLatest, from, Observable, of, throwError } from 'rxjs';
import {
  catchError,
  debounceTime,
  delay,
  distinctUntilChanged,
  filter,
  map,
  mergeMap,
  startWith,
  switchMap,
  tap,
  toArray
} from 'rxjs/operators';
import set from 'lodash.set';
import { DataState } from '@hestia-earth/api';
import { JSON as HestiaJson, NodeType, SchemaType, isExpandable } from '@hestia-earth/schema';
import {
  IValidationError,
  errorText,
  scrollTop,
  baseUrl,
  HeToastService,
  HeGlossaryService,
  PluralizePipe,
  FilesFormEditableComponent,
  IValidationErrorWithIndex,
  distinctUntilChangedDeep,
  FilesFormComponent,
  FilesErrorSummaryComponent,
  ISummaryError,
  hasValidationError
} from '@hestia-earth/ui-components';
import { validator } from '@hestia-earth/schema-validation/validate';
import { NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';

import { ApiService, Status } from '../../api.service';
import { ApiConfigService, defaultConfigId } from '../../api-config.service';
import { ApiSettingsService } from '../../api-settings.service';
import { ApiDraftsService, IDraft, IDraftValidation } from '../api-drafts.service';
import { buildDependencyTree, getIdsFromDependencyTree, calculationSuccessMessage } from '../drafts.model';
import { ReportErrorComponent } from '../../shared/report-error/report-error.component';

interface INodeWithIndex {
  node: HestiaJson<SchemaType>;
  index: number;
  errors?: IValidationError[];
}

const parseError = (error: any) => (typeof error === 'string' ? error : error?.stack ? error.stack : errorText(error));

const schemaValidation = validator(baseUrl(), false);

const openNodeChildren = (node: FilesFormComponent) => {
  const groups = node.ref.nativeElement.querySelectorAll('.open-group:not(.is-open)');
  groups.forEach((group: any) => group.click());
};

const missingPropertyError = (path: string, missingProperty: string, current?: string) =>
  ({
    level: 'error',
    keyword: 'required',
    dataPath: path,
    schemaPath: '#/oneOf/0/required',
    message: `should have required property '${missingProperty}'`,
    params: { missingProperty, current }
  }) as IValidationError;

const isExistingRequired = (node: any) => (node['@type'] || node.type) === NodeType.Term && !node['@id'];

/**
 * Checks if any @id/@type referenced nodes have not been found.
 *
 * @param path
 * @param node
 * @returns
 */
const checkMissingExistingNode = (path = '', node: any) =>
  path !== '' && isExistingRequired(node)
    ? [missingPropertyError(path, '@type'), missingPropertyError(path, '@id', node.id || node.name)]
    : [];

const checkMissingNestedNode = (path: string, nodes: Partial<HestiaJson<SchemaType>>[], node: any) =>
  Object.keys(node)
    .filter(key => isExpandable(node[key]))
    .flatMap(key => {
      const value = node[key];
      return Array.isArray(value)
        ? value.flatMap((val, index) => checkMissingNode(`${path}.${key}[${index}]`, nodes, val))
        : checkMissingNode(`${path}.${key}`, nodes, value);
    });

const checkMissingNode = (path = '', nodes: Partial<HestiaJson<SchemaType>>[], node: any): IValidationError[] => [
  ...checkMissingExistingNode(path, node),
  ...checkMissingNestedNode(path, nodes, node)
];

const noValidationErrors = (errors: IValidationError[][] = []) =>
  errors?.length > 0 && errors?.every(values => values.length === 0);

const nodeErrors = (validationErrors: IValidationError[][], resolvedErrors: ISummaryError[], index: number) => {
  const errors = validationErrors?.[index] ?? [];
  const resolvedIndexes = resolvedErrors
    .filter(({ error }) => error.nodeIndex === index)
    .map(({ error }) => error.index);
  return errors.filter((error, index) => !resolvedIndexes.includes(index));
};

@Component({
  selector: 'app-drafts-new-form',
  templateUrl: './drafts-new-form.component.html',
  styleUrls: ['./drafts-new-form.component.scss'],
  standalone: true,
  imports: [
    NgTemplateOutlet,
    RouterLink,
    FormsModule,
    SlicePipe,
    FaIconComponent,
    NgbTypeaheadModule,
    NgbPaginationModule,
    PluralizePipe,
    FilesFormComponent,
    FilesFormEditableComponent,
    ReportErrorComponent,
    FilesErrorSummaryComponent
  ]
})
export class DraftsNewFormComponent {
  private readonly router = inject(Router);
  private readonly service = inject(ApiService);
  private readonly draftsService = inject(ApiDraftsService);
  private readonly configService = inject(ApiConfigService);
  private readonly settingsService = inject(ApiSettingsService);
  private readonly toastService = inject(HeToastService);
  private readonly glossaryService = inject(HeGlossaryService);

  private readonly forms = viewChildren(FilesFormComponent);

  private readonly migrations = toSignal(this.glossaryService.migrations$());

  protected readonly draft = model.required<Partial<IDraft>>();
  protected readonly nodes = input.required<HestiaJson<SchemaType>[]>();

  private readonly draftId = computed(() => this.draft().id!);
  private readonly draftStatus = computed(() => this.draft().status!);

  private readonly data = signal<HestiaJson<SchemaType>[]>([]);
  private readonly dataWithIndex = computed(() =>
    this.data().map((node, index) => ({ node, index }) as INodeWithIndex)
  );
  private readonly depIds = computed(() => getIdsFromDependencyTree(buildDependencyTree(this.data())));

  protected readonly nodeMap = computed(() =>
    this.data().reduce((prev, curr) => {
      prev[curr.type] = prev[curr.type] || [];
      prev[curr.type].push(curr.id);
      return prev;
    }, {} as any)
  );

  protected readonly showSummary = signal(true);

  protected readonly dataValidation = computed(() => this.draft().validation ?? ({} as IDraftValidation));
  protected readonly dataValidationComplete = computed(() =>
    [Status.completed, Status.error].includes(this.draftStatus())
  );
  protected readonly dataValidating = signal(false);
  /**
   * Validation of terms and schema.
   */
  private readonly validationErrors = signal<IValidationError[][]>([]);

  protected readonly hasValidationError = computed(() => hasValidationError(this.validationErrors(), 'error'));
  protected readonly validationErrorsWithIndex = computed(() =>
    this.validationErrors().map(
      (error, index) =>
        ({
          error,
          index
        }) as IValidationErrorWithIndex
    )
  );
  protected readonly showErrorSummary = computed(() =>
    (this.validationErrors() ?? []).some(errors => errors.length > 0)
  );

  protected readonly resolvedErrors = signal<ISummaryError[]>([]);
  protected readonly unresolvedIndexes = signal<number[]>([]);
  protected readonly nodesWithIndex = toSignal(
    combineLatest([
      toObservable(this.dataWithIndex),
      toObservable(this.unresolvedIndexes),
      toObservable(this.validationErrors),
      toObservable(this.resolvedErrors)
    ]).pipe(
      distinctUntilChangedDeep(),
      map(([dataWithIndex, unresolvedIndexes, validationErrors, resolvedErrors]) =>
        dataWithIndex
          .filter(({ index }) =>
            hasValidationError(validationErrors)
              ? validationErrors?.[index]?.length > 0 && (unresolvedIndexes ?? []).includes(index)
              : true
          )
          .map(data => ({
            ...data,
            errors: nodeErrors(validationErrors, resolvedErrors, data.index)
          }))
      ),
      startWith([] as INodeWithIndex[])
    )
  );

  protected readonly scrollTop = scrollTop;
  protected readonly perPage = 10;
  protected readonly page = signal(1);
  protected readonly progressOffset = computed(() => (this.page() - 1) * this.perPage);
  protected readonly progressLimit = computed(() => this.progressOffset() + this.perPage);
  protected readonly total = computed(() => this.nodesWithIndex()?.length ?? 0);

  protected readonly editing = signal(false);
  protected readonly processing = signal(false);
  protected readonly validating = signal(false);
  /**
   * Draft validated and successful.
   */
  protected readonly validationSuccess = signal(false);

  protected readonly error = signal('');

  private readonly enableQueuing = toSignal(this.settingsService.getSingle$<boolean>('enableQueuing'));
  protected readonly enableGlobalCalculate = computed(() => this.data().length <= 1 || this.enableQueuing());
  protected readonly showSelectConfig = toSignal(
    this.configService.listConfigs$({ limit: 1 }).pipe(map(({ count }) => count > 1))
  );
  protected readonly config = signal(defaultConfigId);

  protected readonly suggestConfig = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(search =>
        from(this.configService.listConfigs({ search })).pipe(map(results => results.results.map(({ id }) => id)))
      )
    );

  constructor() {
    effect(
      () => {
        const nodes = this.nodes();
        this.data.set(nodes);
        setTimeout(() => this.openNodes(), 100);
      },
      { allowSignalWrites: true }
    );

    effect(
      () => {
        if (this.dataValidation()?.success === false) {
          this.validationErrors.set(this.dataValidation().errors ?? []);
        } else if (this.dataValidation()?.success === true) {
          this.validationSuccess.set(true);
        }
      },
      { allowSignalWrites: true }
    );

    effect(
      () => {
        if (this.dataValidationComplete()) {
          this.dataValidating.set(false);
        }
      },
      { allowSignalWrites: true }
    );
  }

  private openNodes() {
    const [form] = this.forms() || [];
    return form && openNodeChildren(form);
  }

  protected nodeChanged({ key, value }: any, index: number) {
    if (key) {
      const nodes = this.data();
      set(nodes[index], key, value);
      this.data.set(nodes);
    }
  }

  protected removeNode(index: number) {
    const nodes = this.data();
    nodes.splice(index, 1);
    this.data.set(nodes);
  }

  protected refreshStatus() {
    this.processing.set(true);
    this.draftsService
      .get$(this.draftId()!, false)
      .pipe(tap(() => this.processing.set(false)))
      .subscribe(({ status, validation }) => {
        const draft = this.draft();
        this.draft.set({ ...draft, status, validation });
      });
  }

  protected readonly canCalculate = computed(() =>
    [this.validationSuccess(), !this.hasValidationError(), !this.validating(), !this.processing()].every(Boolean)
  );

  protected async calculateNode(node: HestiaJson<SchemaType>) {
    this.error.set('');
    this.processing.set(true);
    try {
      await this.service
        .calculateNode$(node.type as any as NodeType, node.id!, this.config(), this.enableQueuing())
        .toPromise();

      this.toastService.success(calculationSuccessMessage(this.enableQueuing()!));

      await this.draftsService.submit$(this.draftId(), [node]).toPromise();

      return this.router.navigate(['/', node.type.toLowerCase(), node.id]);
    } catch (err: any) {
      this.error.set(parseError(err));
    } finally {
      this.processing.set(false);
    }
  }

  protected async calculate() {
    try {
      this.error.set('');
      this.processing.set(true);

      const ids = this.depIds().length === 1 ? undefined : this.depIds().flat();
      await this.draftsService.calculate$(this.draftId(), ids, this.config(), this.enableQueuing()).toPromise();

      this.toastService.success(calculationSuccessMessage(this.enableQueuing()!));

      await this.draftsService.submit$(this.draftId(), this.data()).toPromise();

      return this.router.navigate(
        ['/', ...(this.data().length === 1 ? [this.data()[0].type.toLowerCase(), this.data()[0].id] : [])].filter(
          Boolean
        ),
        {
          queryParams: { dataState: DataState.recalculated }
        }
      );
    } catch (err: any) {
      this.error.set(parseError(err));
    } finally {
      this.processing.set(false);
    }
  }

  protected async saveDraft() {
    this.error.set('');
    return of(this.draft().id)
      .pipe(
        filter(v => !!v),
        tap(() => this.processing.set(true)),
        mergeMap(id => this.draftsService.update$(id!, this.data())),
        tap(() => this.processing.set(false)),
        tap(() => this.editing.set(false)),
        catchError(err => {
          this.error.set(parseError(err));
          return throwError(err);
        })
      )
      .subscribe(() => window.location.reload());
  }

  // Validation

  private validateTerms(nodes: HestiaJson<SchemaType>[]) {
    const errors = nodes
      .map(node => checkMissingNode('', nodes, node))
      .map(values =>
        values.map(v =>
          v.params?.missingProperty && v.params?.current
            ? ({
                ...v,
                params: {
                  ...v.params,
                  ...(this.migrations()?.[v.params.current]
                    ? {
                        expected: this.migrations()![v.params.current]?.newValue,
                        message: this.migrations()![v.params.current]?.insctructions
                      }
                    : {})
                }
              } as IValidationError)
            : v
        )
      );
    // avoid showing "No validation errors detected." if no errors
    return of(noValidationErrors(errors) ? [] : errors);
  }

  private validateSchema(nodes: HestiaJson<SchemaType>[]) {
    return from(nodes).pipe(
      delay(300), // make sure we reload the forms and show a progress
      mergeMap(
        node =>
          from(schemaValidation(node)).pipe(
            map(({ errors }) =>
              errors.map(
                error =>
                  ({
                    ...error,
                    level: 'error'
                  }) as IValidationError
              )
            )
          ),
        1
      ), // limit to 1 as errors would overwrite each other
      toArray()
    );
  }

  private validateData() {
    this.dataValidating.set(true);
    this.draftsService.validate$(this.draftId()).subscribe(() => {
      this.toastService.success('Validation running in the background, please wait...');
      return this.refreshStatus();
    });
  }

  protected validate() {
    // cannot validate when processing
    if (this.processing()) {
      return;
    }

    const nodes = this.data();

    return of(true)
      .pipe(
        tap(() => this.error.set('')),
        tap(() => this.validating.set(true)),
        tap(() => this.validationSuccess.set(false)),
        // step 1: validate terms
        switchMap(() => this.validateTerms(nodes)),
        // step 2: validate schema
        mergeMap(errors => (hasValidationError(errors, 'error') ? of(errors) : this.validateSchema(nodes))),
        tap(() => this.validating.set(false))
      )
      .subscribe(
        errors => {
          this.validationErrors.set(errors);
          // step 3: validate data asynchronously
          !hasValidationError(errors, 'error') && this.validateData();
        },
        error => this.error.set(parseError(error))
      );
  }
}
