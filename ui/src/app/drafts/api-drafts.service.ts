import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JSON as HestiaJson, SchemaType } from '@hestia-earth/schema';
import { filterParams, IValidationError } from '@hestia-earth/ui-components';

import { environment } from '../../environments/environment';
import { IBaseModel, ISearchParams, SearchResult } from '../api.service';

const apiUrl = `${environment.apiUrl}/drafts`;

export interface IDraftValidation {
  vesion: string;
  success: boolean;
  errors?: IValidationError[][];
}

export interface IDraft extends IBaseModel {
  nodes: HestiaJson<any>[];
  /**
   * Version of the CE used to recalculate.
   */
  version?: string;
  /**
   * Version of the Schema used to created the data.
   */
  schema?: string;
  /**
   * Validation result.
   */
  validation?: IDraftValidation;
}

interface IDraftSubmit {
  type: SchemaType;
  id: string;
}

const nodesToForm = (nodes: HestiaJson<any>[]) => {
  const blob = new Blob(['{"nodes":[' + nodes.map(node => JSON.stringify(node)).join(',') + ']}'], {
    type: 'application/json'
  });
  const formData = new FormData();
  formData.append('file', blob, 'data.json');
  return formData;
};

@Injectable({
  providedIn: 'root'
})
export class ApiDraftsService {
  private readonly http = inject(HttpClient);

  public list(params: ISearchParams = {}) {
    return this.http
      .get<SearchResult>(apiUrl, {
        params: filterParams(params)
      })
      .toPromise();
  }

  public get$(id: string, includeNodes = true) {
    return this.http.get<IDraft>(`${apiUrl}/${id}`, {
      params: { include_nodes: includeNodes }
    });
  }

  public get(id: string) {
    return this.get$(id).toPromise();
  }

  /**
   * Upload draft file.
   *
   * @param id The id of the draft (using the source filename)
   * @param file The content of the file
   */
  public create(id: string, file: File) {
    const formData = new FormData();
    formData.append('file', file, 'file.csv');
    formData.append('id', id);
    return this.http.post<void>(`${apiUrl}`, formData).toPromise();
  }

  public update$(id: string, nodes: HestiaJson<any>[]) {
    return this.http.put<{ success: boolean }>(`${apiUrl}/${id}`, nodesToForm(nodes));
  }

  public delete(id: string) {
    return this.http.delete<{ success: boolean }>(`${apiUrl}/${id}`).toPromise();
  }

  public validate$(id: string) {
    return this.http.patch<{ success: boolean }>(`${apiUrl}/${id}/validate`, {});
  }

  /**
   * Calculate Draft data in the background.
   *
   * @param id The id of the Draft.
   * @param ids A list of list of IDs to run the calculations in a specific order.
   * @param config The configuration used to calculate.
   * @returns `{success: true}`
   */
  public calculate$(id: string, ids?: string[], config?: string, enableQueuing?: boolean) {
    return this.http.post<{ success: boolean }>(
      `${apiUrl}/${id}/calculate`,
      {},
      {
        headers: filterParams({ config, ids, 'run-async': enableQueuing })
      }
    );
  }

  /**
   * When calculating data from a Draft, copy the Draft source file over to the data folder.
   *
   * @param id The id of the Draft.
   * @param type Type of the Node.
   * @param id Id of the Node, or list of ids.
   * @returns
   */
  public submit$(id: string, nodes: HestiaJson<SchemaType>[]) {
    return this.http.post<{ success: boolean }>(
      `${apiUrl}/${id}`,
      nodes.map(node => ({ type: node.type, id: node.id }) as IDraftSubmit)
    );
  }
}
