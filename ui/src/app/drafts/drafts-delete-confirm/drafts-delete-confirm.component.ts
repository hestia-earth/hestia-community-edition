import { Component, inject, model, output, signal } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiDraftsService } from '../api-drafts.service';

@Component({
  selector: 'app-drafts-delete-confirm',
  templateUrl: './drafts-delete-confirm.component.html',
  styleUrls: ['./drafts-delete-confirm.component.scss'],
  standalone: true,
  imports: [FaIconComponent]
})
export class DraftsDeleteConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });
  private readonly service = inject(ApiDraftsService);

  public readonly id = model<string>();

  protected readonly closed = output<boolean>();

  protected readonly loading = signal(false);

  protected close(value = false) {
    this.closed.emit(value);
    this.activeModal?.close(value);
  }

  public async confirm() {
    this.loading.set(true);
    await this.service.delete(this.id()!);
    this.loading.set(false);
    this.close(true);
  }
}
