import { Component } from '@angular/core';

import { TitleComponent } from '../../shared/title/title.component';
import { ResultsListComponent } from '../../results/results-list/results-list.component';

@Component({
  selector: 'app-drafts-list-page',
  templateUrl: './drafts-list-page.component.html',
  styleUrl: './drafts-list-page.component.scss',
  standalone: true,
  imports: [TitleComponent, ResultsListComponent]
})
export class DraftsListPageComponent {}
