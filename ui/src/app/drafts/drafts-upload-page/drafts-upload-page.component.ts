import { Component, computed, effect, inject, Signal, signal } from '@angular/core';
import { JsonPipe, KeyValuePipe, NgTemplateOutlet } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { from } from 'rxjs';
import { distinct, filter, map, mergeMap, reduce, startWith } from 'rxjs/operators';
import { replaceInvalidChars, SupportedExtensions, filenameWithoutExt } from '@hestia-earth/api';
import { NodeType, JSON as HestiaJson, ITermJSONLD, SCHEMA_VERSION } from '@hestia-earth/schema';
import { unique } from '@hestia-earth/utils';
import { convertZip as convertOlca } from '@hestia-earth/olca';
import { convert as convertPooreNemecek } from '@hestia-earth/poore-nemecek';
import {
  isChrome,
  gitHome,
  baseUrl,
  HeSearchService,
  matchType,
  matchNameNormalized,
  matchId,
  schemaBaseUrl,
  FilesUploadErrorsComponent,
  ClipboardComponent
} from '@hestia-earth/ui-components';
import { toJson } from '@hestia-earth/schema-convert';
import { loadSchemas } from '@hestia-earth/json-schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faClone as farClone } from '@fortawesome/free-regular-svg-icons';
import { IError } from '@hestia-earth/ui-components/files/files-upload-errors/files-upload-errors.component';

import { deepReplaceTerms, findDeepTerms, mergeNodesByImpactAssessment } from '../drafts.model';
import { ApiService } from '../../api.service';
import { ApiVersionService } from '../../api-version.service';
import { ApiSettingsService, DataFormat } from '../../api-settings.service';
import { ApiDraftsService } from '../api-drafts.service';
import { TitleComponent } from '../../shared/title/title.component';

const supportedExtensions: {
  [format in DataFormat]: string[];
} = {
  [DataFormat.Hestia]: [
    SupportedExtensions.xls,
    SupportedExtensions.xlsx,
    SupportedExtensions.csv,
    SupportedExtensions.json
  ].map(ext => `.${ext}`),
  [DataFormat.OpenLCA]: ['.zip'],
  [DataFormat.PooreNemecek]: [SupportedExtensions.xls, SupportedExtensions.xlsx, SupportedExtensions.csv].map(
    ext => `.${ext}`
  )
};

const isExcel = ({ name }: File) => name?.endsWith(SupportedExtensions.xls) || name?.endsWith(SupportedExtensions.xlsx);

const parseConvertError = (error: any) => {
  try {
    return error?.error?.detail || JSON.parse(error.message);
  } catch (err) {
    return error.stack || error.message;
  }
};

@Component({
  selector: 'app-drafts-upload-page',
  templateUrl: './drafts-upload-page.component.html',
  styleUrls: ['./drafts-upload-page.component.scss'],
  standalone: true,
  imports: [
    NgTemplateOutlet,
    RouterLink,
    JsonPipe,
    KeyValuePipe,
    FormsModule,
    FaIconComponent,
    FilesUploadErrorsComponent,
    ClipboardComponent,
    TitleComponent
  ]
})
export class DraftsUploadPageComponent {
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly searchService = inject(HeSearchService);
  private readonly service = inject(ApiService);
  private readonly draftsService = inject(ApiDraftsService);
  private readonly settingsService = inject(ApiSettingsService);
  private readonly versionService = inject(ApiVersionService);

  protected readonly farClone = farClone;

  private readonly pandasVersion = toSignal(this.versionService.pandasVersion$);
  private readonly enableExcelUpload = computed(() => !!this.pandasVersion());

  protected readonly isChrome = isChrome();
  protected readonly baseUrl = baseUrl();
  protected readonly gitHome = gitHome;
  protected readonly SCHEMA_VERSION = SCHEMA_VERSION;
  protected readonly schemaBaseUrl = schemaBaseUrl();
  protected readonly changelogUrl = `${gitHome}/hestia-schema/-/blob/master/CHANGELOG.md`;

  private readonly id$ = this.route.params.pipe(map(({ id }) => id as string));
  protected readonly id = toSignal(this.id$);

  private readonly file = signal<File>(undefined as any);
  protected readonly filename = computed(() => this.file()?.name ?? '');
  protected readonly filenameWithoutExt = computed(() => filenameWithoutExt(this.filename()));
  protected readonly sheetNameEnabled = computed(() => this.file() && isExcel(this.file()));
  protected readonly sheetName = signal('');
  protected readonly submitted = signal(false);
  protected readonly loading = signal(false);

  protected readonly error = signal<any>(undefined);
  protected readonly hestiaErrors: Signal<IError[]> = computed(() =>
    typeof this.error() === 'object'
      ? Array.isArray(this.error().errors)
        ? (this.error().errors as IError[])
        : [this.error() as IError]
      : []
  );
  protected readonly isInvalidSheetNameError = computed(
    () =>
      typeof this.error() === 'string' &&
      this.error()?.startsWith('Worksheet named') &&
      this.error()?.endsWith('not found')
  );

  protected readonly DataFormat = DataFormat;
  private readonly dataFormat = toSignal(
    this.settingsService.getSingle$<DataFormat>('defaultFormat').pipe(startWith(DataFormat.PooreNemecek))
  );
  protected readonly selectedDataFormat = signal(DataFormat.PooreNemecek);

  protected readonly supportedExtensions = computed(() => {
    const exts = supportedExtensions[this.selectedDataFormat()!].slice();
    const enableExcelUpload = this.enableExcelUpload() && exts.includes(`.${SupportedExtensions.csv}`);
    return enableExcelUpload ? unique([...exts, `.${SupportedExtensions.xls}`, `.${SupportedExtensions.xlsx}`]) : exts;
  });

  constructor() {
    effect(
      () => {
        if (this.dataFormat()) {
          this.selectedDataFormat.set(this.dataFormat()!);
        }
      },
      { allowSignalWrites: true }
    );
  }

  protected addFiles({ files }: any) {
    if (files && files.length) {
      const [file]: File[] = Array.from(files);
      this.file.set(file);
    }
  }

  private async findTerm(value: string) {
    const { results } = await this.searchService.search<ITermJSONLD, NodeType.Term>({
      limit: 1,
      fields: ['@type', '@id', 'name', 'termType', 'units'],
      query: {
        bool: {
          must: [matchType(NodeType.Term)],
          should: [matchId(value), matchNameNormalized(value)],
          minimum_should_match: 1
        }
      }
    });
    return results?.[0];
  }

  private async findTerms(nodes: any[]) {
    return from(nodes)
      .pipe(
        mergeMap(findDeepTerms),
        distinct(),
        mergeMap(
          value =>
            from(this.findTerm(value)).pipe(
              filter(term => !!term),
              map(({ _score, ...term }) => ({ value, term }))
            ),
          10
        ),
        reduce(
          (prev, curr) => {
            prev[curr.value] = curr.term;
            return prev;
          },
          {} as { [value: string]: ITermJSONLD }
        )
      )
      .toPromise();
  }

  private async hestiaJsonContent(content: string) {
    try {
      const data = JSON.parse(content);
      const nodes = Array.isArray(data) ? data : data.nodes || [];
      return nodes as any[];
    } catch (_err) {
      return toJson(loadSchemas(), content);
    }
  }

  private async convertHestia(content: string) {
    const nodes = await this.hestiaJsonContent(content);
    const termsMapping = await this.findTerms(nodes);
    return { nodes: nodes.map(node => deepReplaceTerms(node, termsMapping)) };
  }

  private async convertToFormat(file: File) {
    const converter: {
      [format in DataFormat]: (fileContent: string) => Promise<{ nodes: any[] }>;
    } = {
      [DataFormat.Hestia]: fileContent => this.convertHestia(fileContent),
      [DataFormat.OpenLCA]: () => convertOlca(file).toPromise(),
      [DataFormat.PooreNemecek]: fileContent => convertPooreNemecek(fileContent)
    };
    const content = isExcel(file) ? await this.service.convertExcel(file, this.sheetName()) : await file.text();
    return mergeNodesByImpactAssessment(await converter[this.selectedDataFormat()!](content));
  }

  protected async submit() {
    this.submitted.set(true);
    if (!this.file()) {
      return;
    }

    this.loading.set(true);
    this.error.set(undefined);
    try {
      const id = this.id() || replaceInvalidChars(this.filenameWithoutExt());
      const nodes = await this.convertToFormat(this.file());
      await this.draftsService.create(id, this.file());
      await this.draftsService.update$(id, nodes).toPromise();
      return this.router.navigate(['/', 'drafts', id]);
    } catch (err: any) {
      console.error(err);
      this.error.set(parseConvertError(err));
      console.error(this.error());
    } finally {
      this.loading.set(false);
    }
  }
}
