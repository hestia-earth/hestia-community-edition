import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { versionTag } from './utils';

@Injectable()
export class NodeServiceInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (request.url.includes(versionTag)) {
      const versionTagIndex = request.url.indexOf(versionTag);
      const version = request.url
        .substring(versionTagIndex, request.url.indexOf('/', versionTagIndex))
        .replace(versionTag, '');
      return next.handle(
        request.clone({
          params: request.params.set('version', version),
          url: request.url.replace(/@version[\d.]+/g, '')
        })
      );
    }
    return next.handle(request);
  }
}
