import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ApiVersionService } from './api-version.service';

@Injectable()
export class ApiVersionInterceptor implements HttpInterceptor {
  constructor(private apiVersionService: ApiVersionService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(tap(resp => [this.setVersion(resp as any), this.setPandasVersion(resp as any)]));
  }

  private setVersion(response: HttpResponse<unknown>) {
    const version = 'headers' in response ? response.headers.get('x-api-version') : null;
    return version ? this.apiVersionService.updateVersion(version) : null;
  }

  private setPandasVersion(response: HttpResponse<unknown>) {
    const version = 'headers' in response ? response.headers.get('x-pandas-version') : null;
    return version ? this.apiVersionService.updatePandasVersion(version) : null;
  }
}
