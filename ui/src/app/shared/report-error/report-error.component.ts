import { Component, computed, inject, input } from '@angular/core';
import { Router } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { ClipboardComponent, gitHome, Repository } from '@hestia-earth/ui-components';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faClone as farClone } from '@fortawesome/free-regular-svg-icons';
import { faBug } from '@fortawesome/free-solid-svg-icons';
import { JsonPipe } from '@angular/common';

import { environment } from '../../../environments/environment';
import { ApiVersionService, IVersion } from '../../api-version.service';

const issueLink = (url: string, error: string, versions?: IVersion) =>
  `${gitHome}/${Repository.community}/-/issues/new?issue[title]=${encodeURIComponent(
    'Issue running calculations'
  )}&issue[description]=${encodeURIComponent(
    `
# 🐞 Bug Report

## Error

\`\`\`
${JSON.stringify(error, null, 2)}
\`\`\`

## Details

* UI Version: \`${environment.version}\`
* API Version: \`${versions?.current}\`
* Schema Version: \`${versions?.schema}\`
* Models Version: \`${versions?.models}\`
* Path: \`${url}\`

/label ~bug
/label ~"priority::MEDIUM"
    `.trim()
  )}`;

@Component({
  selector: 'app-report-error',
  templateUrl: './report-error.component.html',
  styleUrls: ['./report-error.component.scss'],
  standalone: true,
  imports: [JsonPipe, FaIconComponent, ClipboardComponent]
})
export class ReportErrorComponent {
  private readonly router = inject(Router);
  private readonly versionService = inject(ApiVersionService);

  private readonly versions = toSignal(this.versionService.versions$);

  protected readonly farClone = farClone;
  protected readonly faBug = faBug;

  protected readonly error = input.required<string>();

  protected readonly issueLink = computed(() =>
    this.error() ? issueLink(this.router.url, this.error(), this.versions()) : null
  );
}
