import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { environment } from '../../../environments/environment';
import { parseVersion, versionUrl } from '../../api-version.service';

@Component({
  selector: 'app-ui-version',
  templateUrl: './ui-version.component.html',
  styleUrls: ['./ui-version.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [FaIconComponent]
})
export class UiVersionComponent {
  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  protected readonly versionUrl = versionUrl;
  protected readonly version = parseVersion(environment.version);
}
