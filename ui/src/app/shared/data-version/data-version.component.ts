import { Component, computed, inject, input, Input } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs/operators';

import { ApiVersionService, parseVersion, versionUrl } from '../../api-version.service';

@Component({
  selector: 'app-data-version',
  templateUrl: './data-version.component.html',
  styleUrls: ['./data-version.component.scss'],
  standalone: true,
  imports: []
})
export class DataVersionComponent {
  private readonly versionService = inject(ApiVersionService);

  protected readonly version = input.required<string>();

  protected readonly latestVersion = toSignal(this.versionService.versions$.pipe(map(({ current }) => current)));

  protected readonly url = computed(() => (this.version ? versionUrl(parseVersion(this.version())) : null));
  protected readonly isLatest = computed(() => this.latestVersion() === this.version());
}
