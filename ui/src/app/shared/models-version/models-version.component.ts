import { ChangeDetectionStrategy, Component, computed, inject, input } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { engineGitBaseUrl } from '@hestia-earth/ui-components';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs/operators';

import { ApiVersionService } from '../../api-version.service';

@Component({
  selector: 'app-models-version',
  templateUrl: './models-version.component.html',
  styleUrls: ['./models-version.component.scss'],
  standalone: true,
  imports: [NgTemplateOutlet],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelsVersionComponent {
  private readonly versionService = inject(ApiVersionService);

  protected readonly version = input.required<string>();
  protected readonly compact = input(false);

  protected readonly latestVersion = toSignal(this.versionService.versions$.pipe(map(({ models }) => models)));

  protected readonly url = computed(() => `${engineGitBaseUrl()}/-/tree/v${this.version()}?ref_type=tags`);
  protected readonly isLatest = computed(() => this.latestVersion() === this.version());
}
