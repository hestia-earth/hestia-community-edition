import { Component, OnDestroy, ChangeDetectionStrategy, effect, inject, input, computed } from '@angular/core';
import { Title } from '@angular/platform-browser';

const defaultTitle = 'HESTIA CE';

@Component({
  selector: 'app-title',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true
})
export class TitleComponent implements OnDestroy {
  private readonly titleService = inject(Title);

  protected readonly title = input('');

  private readonly _title = computed(() => [this.title(), defaultTitle].filter(Boolean).join(' | '));

  constructor() {
    effect(() => this.titleService.setTitle(this._title()));
  }

  ngOnDestroy() {
    this.titleService.setTitle(defaultTitle);
  }
}
