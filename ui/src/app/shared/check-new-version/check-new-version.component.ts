import { ChangeDetectionStrategy, Component, computed, effect, inject, signal } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { ApiVersionService, gitUrl } from '../../api-version.service';

@Component({
  selector: 'app-check-new-version',
  templateUrl: './check-new-version.component.html',
  styleUrls: ['./check-new-version.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent]
})
export class CheckNewVersionComponent {
  private readonly apiVersionService = inject(ApiVersionService);

  protected readonly faExternalLinkAlt = faExternalLinkAlt;
  protected readonly gitUrl = gitUrl;

  private readonly versions = toSignal(this.apiVersionService.versions$);
  protected readonly latest = computed(() => this.versions()?.latest);
  protected readonly visible = signal(false);

  constructor() {
    effect(() => this.visible.set(!!this.versions() && !this.versions()?.isLatest && !this.versions()?.isPrerelease), {
      allowSignalWrites: true
    });
  }
}
