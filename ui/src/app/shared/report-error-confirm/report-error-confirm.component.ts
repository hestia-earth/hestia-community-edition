import { Component, inject, model, output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ReportErrorComponent } from '../report-error/report-error.component';

@Component({
  selector: 'app-report-error-confirm',
  templateUrl: './report-error-confirm.component.html',
  styleUrls: ['./report-error-confirm.component.scss'],
  standalone: true,
  imports: [FormsModule, ReportErrorComponent]
})
export class RepportErrorConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });

  public readonly error = model<string>();

  protected readonly closed = output();

  protected close() {
    this.closed.emit();
    this.activeModal?.close();
  }
}
