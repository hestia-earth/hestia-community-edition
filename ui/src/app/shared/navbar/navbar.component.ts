import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, input, inject, signal } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { gitHome, HeCommonService } from '@hestia-earth/ui-components';

const fixedTopBodyClass = 'has-spaced-navbar-fixed-top';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent, RouterLink]
})
export class NavbarComponent implements OnInit, OnDestroy {
  private readonly router = inject(Router);
  protected readonly commonService = inject(HeCommonService);

  protected readonly isTransparent = input(false);

  protected readonly gitHome = gitHome;
  protected readonly menuActive = signal(false);

  ngOnInit() {
    return document.body.classList.add(fixedTopBodyClass);
  }

  ngOnDestroy() {
    return document.body.classList.remove(fixedTopBodyClass);
  }

  protected isCurrentUrl(url: string) {
    return this.router.url === url;
  }

  protected startsWithUrl(url: string) {
    return this.router.url.startsWith(url);
  }
}
