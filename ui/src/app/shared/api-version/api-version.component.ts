import { ChangeDetectionStrategy, Component, computed, inject } from '@angular/core';
import { toSignal } from '@angular/core/rxjs-interop';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faExternalLinkAlt } from '@fortawesome/free-solid-svg-icons';

import { ApiVersionService, versionUrl } from '../../api-version.service';

@Component({
  selector: 'app-api-version',
  templateUrl: './api-version.component.html',
  styleUrls: ['./api-version.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [FaIconComponent]
})
export class ApiVersionComponent {
  private readonly apiVersionService = inject(ApiVersionService);

  protected readonly faExternalLinkAlt = faExternalLinkAlt;

  protected readonly version = toSignal(this.apiVersionService.versionData$);
  protected readonly versionUrl = computed(() => (this.version() ? versionUrl(this.version()!) : null));
}
