import { Component, input } from '@angular/core';

import { Status } from '../../api.service';

@Component({
  selector: 'app-results-status',
  templateUrl: './results-status.component.html',
  styleUrls: ['./results-status.component.scss'],
  standalone: true,
  imports: []
})
export class ResultsStatusComponent {
  protected readonly status = input.required<Status>();
  protected readonly Status = Status;
}
