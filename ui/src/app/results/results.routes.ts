import { Route } from '@angular/router';

import { ResultsListPageComponent } from './results-list-page/results-list-page.component';

export default [
  {
    path: '',
    component: ResultsListPageComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
] satisfies Route[];
