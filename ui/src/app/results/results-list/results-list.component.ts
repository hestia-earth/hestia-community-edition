import { Component, inject, input, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { DatePipe, NgTemplateOutlet } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NodeType } from '@hestia-earth/schema';
import { scrollToEl, filterParams, PluralizePipe } from '@hestia-earth/ui-components';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faSortAmountUpAlt } from '@fortawesome/free-solid-svg-icons';
import { NgbDropdownModule, NgbModal, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';

import { Status, ApiService, SearchResult, IMetadata, sortByFields, sortByOrder } from '../../api.service';
import { ApiDraftsService } from '../../drafts/api-drafts.service';
import { ResultsStatusComponent } from '../results-status/results-status.component';
import { RepportErrorConfirmComponent } from '../../shared/report-error-confirm/report-error-confirm.component';
import { DataVersionComponent } from '../../shared/data-version/data-version.component';
import { ModelsVersionComponent } from '../../shared/models-version/models-version.component';
import { NodeRecalculateConfirmComponent } from '../../node/node-recalculate-confirm/node-recalculate-confirm.component';
import { NodeDeleteConfirmComponent } from '../../node/node-delete-confirm/node-delete-confirm.component';

@Component({
  selector: 'app-results-list',
  standalone: true,
  imports: [
    NgTemplateOutlet,
    FormsModule,
    RouterLink,
    DatePipe,
    FaIconComponent,
    NgbPaginationModule,
    NgbDropdownModule,
    PluralizePipe,
    ResultsStatusComponent,
    DataVersionComponent,
    ModelsVersionComponent
  ],
  templateUrl: './results-list.component.html',
  styleUrl: './results-list.component.scss'
})
export class ResultsListComponent implements OnInit {
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  protected readonly service = inject(ApiService);
  private readonly draftsService = inject(ApiDraftsService);
  private readonly modalService = inject(NgbModal);

  protected readonly faSortAmountUpAlt = faSortAmountUpAlt;

  protected readonly isDraft = input(false);

  protected loading = true;
  protected readonly NodeType = NodeType;
  protected readonly selectedType = NodeType.ImpactAssessment;

  protected readonly Status = Status;
  protected search = '';
  protected readonly perPage = 10;
  protected page = 1;
  protected showSortBy = false;
  protected sortBy: sortByFields = 'createdOn';
  protected sortOrder: sortByOrder = 'desc';
  protected results = new SearchResult();

  protected showActions?: IMetadata;

  ngOnInit() {
    const params = this.route.snapshot.queryParamMap;
    this.search = params.get('query') || '';
    this.sortBy = (params.get('sortBy') as sortByFields) || 'createdOn';
    this.sortOrder = (params.get('sortOrder') as sortByOrder) || 'desc';
    this.page = +(params.get('page') || '1');
    return this.filter();
  }

  protected scrollTop() {
    setTimeout(() => scrollToEl('search-results'));
  }

  protected trackByResult(_index: number, { id }: IMetadata) {
    return id;
  }

  sort(sortBy: sortByFields) {
    this.sortOrder = this.sortBy === sortBy ? (this.sortOrder === 'asc' ? 'desc' : 'asc') : 'desc';
    this.sortBy = sortBy;
    return this.runSearch();
  }

  private async updateQueryParams() {
    const queryParams = this.queryParams;
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      ...(Object.keys(queryParams).length ? { queryParams } : {})
    });
  }

  protected async runSearch() {
    this.page = 1;
    return this.filter();
  }

  protected async filter() {
    await this.updateQueryParams();
    this.loading = true;
    this.results = this.isDraft()
      ? await this.draftsService.list(this.searchParams)
      : await this.service.list(this.selectedType, this.searchParams);
    this.loading = false;
  }

  protected get queryParams() {
    return filterParams({
      query: this.search,
      page: this.page,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder
    });
  }

  private get searchParams() {
    return {
      search: this.search,
      limit: this.perPage,
      offset: this.offset,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder
    };
  }

  protected get total() {
    return this.results?.count ?? 0;
  }

  protected get offset() {
    return (this.page - 1) * this.perPage;
  }

  protected toggleShowActions(result: IMetadata) {
    this.showActions = this.showActions === result ? undefined : result;
  }

  protected openResult(result: IMetadata) {
    return this.router.navigate(
      this.isDraft() ? ['/drafts', result.id] : [this.selectedType.toLowerCase(), result.id],
      {
        queryParams: this.isDraft() ? {} : result.recalculatedOn ? { dataState: 'recalculated' } : {}
      }
    );
  }

  protected showRecalculate({ id, config }: IMetadata) {
    const instance = this.modalService.open(NodeRecalculateConfirmComponent);
    const component = instance.componentInstance as NodeRecalculateConfirmComponent;
    component.type.set(this.selectedType);
    component.id.set(id);
    component.config.set(config?.config || '');
  }

  protected showDelete({ id }: IMetadata) {
    const instance = this.modalService.open(NodeDeleteConfirmComponent);
    const component = instance.componentInstance as NodeDeleteConfirmComponent;
    component.type.set(this.selectedType);
    component.id.set(id);
    instance.closed.subscribe((deleted: boolean) => (deleted ? setTimeout(() => this.runSearch()) : null));
  }

  protected showError({ error }: IMetadata) {
    const instance = this.modalService.open(RepportErrorConfirmComponent);
    const component = instance.componentInstance as RepportErrorConfirmComponent;
    component.error.set(error?.stack);
  }
}
