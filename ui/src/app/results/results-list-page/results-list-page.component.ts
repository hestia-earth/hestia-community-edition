import { Component } from '@angular/core';

import { TitleComponent } from '../../shared/title/title.component';
import { ResultsListComponent } from '../results-list/results-list.component';

@Component({
  selector: 'app-results-list-page',
  templateUrl: './results-list-page.component.html',
  styleUrls: ['./results-list-page.component.scss'],
  standalone: true,
  imports: [TitleComponent, ResultsListComponent]
})
export class ResultsListPageComponent {}
