import { TestBed, waitForAsync } from '@angular/core/testing';
import { provideRouter } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HE_API_BASE_URL, HE_CALCULATIONS_BASE_URL } from '@hestia-earth/ui-components';
import { LocalStorageService } from 'ngx-webstorage';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, AppComponent],
      providers: [
        provideRouter([]),
        LocalStorageService,
        {
          provide: HE_API_BASE_URL,
          useValue: 'url'
        },
        {
          provide: HE_CALCULATIONS_BASE_URL,
          useValue: 'url'
        }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
