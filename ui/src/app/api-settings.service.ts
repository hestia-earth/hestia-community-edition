import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

import { environment } from '../environments/environment';

const path = `${environment.apiUrl}/settings`;

export enum DataFormat {
  Hestia = 'HESTIA',
  PooreNemecek = 'Poore & Nemecek',
  OpenLCA = 'Open LCA'
}

export interface ISettings {
  /**
   * Default format when uploading a file.
   */
  defaultFormat?: DataFormat;
  /**
   * Calculation using the queuing system enabled/disabled.
   */
  enableQueuing?: boolean;
  /**
   * Enable models using HESTIA aggregated data.
   */
  enableAggregatedModels?: boolean;
}

export const defaultSettings: ISettings = {
  defaultFormat: DataFormat.Hestia,
  enableQueuing: false,
  enableAggregatedModels: false
};

@Injectable({
  providedIn: 'root'
})
export class ApiSettingsService {
  private readonly http = inject(HttpClient);

  public getAll$() {
    return this.http.get<ISettings>(path).pipe(catchError(() => of(defaultSettings)));
  }

  public getAll() {
    return this.getAll$().toPromise();
  }

  public getSingle$<T>(key: keyof ISettings) {
    return this.http.get<T>(`${path}/${key}`);
  }

  public getSingle<T>(key: keyof ISettings) {
    return this.getSingle$<T>(key).toPromise();
  }

  public updateSingle(key: keyof ISettings, value: any) {
    return this.http.put<{ success: boolean }>(`${path}/${key}`, value).toPromise();
  }

  public updateAll(settings: Partial<ISettings>) {
    return this.http.put<{ success: boolean }>(path, settings).toPromise();
  }
}
