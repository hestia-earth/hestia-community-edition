import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ITermJSONLD, NodeType, JSON as HestiaJson, SchemaType } from '@hestia-earth/schema';
import { DataState, nodeTypeToParam } from '@hestia-earth/api';
import { filterParams } from '@hestia-earth/ui-components';

import { environment } from '../environments/environment';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

export enum Status {
  queued = 'queued',
  started = 'started',
  completed = 'completed',
  error = 'error'
}

export interface IBaseModel {
  id: string;
  createdOn?: number;
  /**
   * The status of the result.
   */
  status: Status;
  /**
   * Path of the Source file, if any. Can be used with `/download` endpoint.
   */
  sourcePath?: string;
}

export interface INodeConfig {
  /**
   * Configuration used to recalculate.
   */
  config?: string;
  /**
   * Version of the CE used to recalculate.
   */
  version?: string;
  /**
   * Version of the Schema used to created the data.
   */
  schema?: string;
  /**
   * Version of the models used to recalculate.
   */
  modelsVersion?: string;
}

export interface IMetadata extends IBaseModel {
  recalculatedOn?: number;
  error?: {
    error: string;
    stack?: string;
  };
  config?: INodeConfig;
  versions?: string[];
}

export class SearchResult {
  results: IMetadata[] = [];
  count = 0;
  inProgress = 0;
}

export type sortByFields = 'id' | 'createdOn';
export type sortByOrder = 'asc' | 'desc';

export interface ISearchParams {
  search?: string;
  offset?: number;
  limit?: number;
  sortOrder?: sortByOrder;
  sortBy?: sortByFields;
}

export enum EEType {
  raster = 'raster'
}

export enum EEReducer {
  first = 'first',
  mean = 'mean',
  mode = 'mode',
  sum = 'sum'
}

export enum EECollectionName {
  soilPh = 'Soil pH',
  clayContent = 'Clay content',
  sandContent = 'Sand content',
  organicCarbonPerKgSoil = 'Organic carbon content',
  totalNitrogenPerKgSoil = 'Nitrogen content',
  totalPhosphorusPerKgSoil = 'Phosphorous content',
  slopeLength = 'Slope length',
  slope = 'Slope',
  drainageClass = 'Drainage class',
  nutrientLossToAquaticEnvironment = 'Nutrient loss aquatic',
  erodibility = 'Erodibility',
  heavyWinterPrecipitation = 'Heavy winter precipitation',
  ecoClimateZone = 'Eco-climate Zone',
  histosol = 'Histosol'
}

export interface ICollection {
  name: EECollectionName;
  collection: string;
  reducer: EEReducer;
}

export interface ICollectionExtended {
  ee_type: EEType;
  collections: ICollection[];
  'gadm-ids'?: string[];
  coordinates?: {
    latitude?: number;
    longitude?: string;
  }[];
}

export interface IRunner {
  id: string;
  status: string;
  connectedAt?: number;
  active?: boolean;
}

export interface IRunnerQueue {
  messages: {
    total: number;
    /**
     * How many messages are currently being processed.
     */
    current: number;
    /**
     * How many messages are still left to be processed.
     */
    remaining: number;
  };
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private http = inject(HttpClient);

  public list(type = NodeType.ImpactAssessment, params: ISearchParams = {}) {
    return this.http
      .get<SearchResult>(`${environment.apiUrl}/${nodeTypeToParam(type)}`, {
        params: filterParams(params)
      })
      .toPromise();
  }

  public get$<T>(type = NodeType.ImpactAssessment, id: string, dataState = DataState.original, version?: string) {
    return this.http
      .get<T>(`${environment.apiUrl}/${nodeTypeToParam(type)}/${id}`, {
        params: filterParams({ dataState, version })
      })
      .pipe(catchError(() => of({} as T)));
  }

  public get<T>(type = NodeType.ImpactAssessment, id: string, dataState = DataState.original, version?: string) {
    return this.get$<T>(type, id, dataState, version)
      .toPromise()
      .then(result => result || ({} as T));
  }

  public getMetadata$(type = NodeType.ImpactAssessment, id: string, version?: string) {
    return this.http
      .get<IMetadata>(`${environment.apiUrl}/${nodeTypeToParam(type)}/${id}/metadata`, {
        params: filterParams({ version })
      })
      .pipe(catchError(() => of({} as IMetadata)));
  }

  public getMetadata(type = NodeType.ImpactAssessment, id: string, version?: string) {
    return this.getMetadata$(type, id, version).toPromise();
  }

  /**
   * Calculate some data in the background.
   *
   * @param data The data containing the nodes.
   * @param ids A list of list of IDs to run the calculations in a specific order.
   * @param config The configuration used to calculate.
   * @returns `{success: true}`
   */
  public calculate$(nodes: HestiaJson<SchemaType>[], ids: string[], config?: string, enableQueuing?: boolean) {
    return this.http.post<{ success: boolean }>(`${environment.apiUrl}/recalculate`, nodes, {
      headers: filterParams({ config, ids, 'run-async': enableQueuing })
    });
  }

  public calculateNode$(type = NodeType.ImpactAssessment, id: string, config?: string, enableQueuing?: boolean) {
    return this.http.post<{ success: boolean }>(
      `${environment.apiUrl}/${nodeTypeToParam(type)}/${id}/pipeline`,
      {},
      {
        headers: filterParams({ config, 'run-async': enableQueuing })
      }
    );
  }

  public delete(type = NodeType.ImpactAssessment, id: string) {
    return this.http.delete<{ success: boolean }>(`${environment.apiUrl}/${nodeTypeToParam(type)}/${id}`).toPromise();
  }

  public getTerm(id: string) {
    return this.http.get<ITermJSONLD>(`${environment.apiUrl}/terms/${id}`).toPromise();
  }

  public uploadFile(type: NodeType, id: string, file: File) {
    const formData = new FormData();
    formData.append('file', file, 'file.csv');
    formData.append('id', id);
    formData.append('type', type);
    return this.http.post<void>(`${environment.apiUrl}/upload`, formData).toPromise();
  }

  public convertExcel(file: File, sheet?: string) {
    const formData = new FormData();
    formData.append('file', file, file.name);
    return this.http
      .post<string>(`${environment.apiUrl}/upload/excel`, formData, {
        headers: filterParams({ sheet })
      })
      .toPromise();
  }

  public ee(type: 'gadm' | 'coordinates' | 'boundary', data: ICollectionExtended) {
    return this.http.post<(string | number)[]>(`${environment.apiUrl}/ee/${type}`, data).toPromise();
  }

  // --- Runners

  public runnersStatus$() {
    return this.http.get<{ enabled: boolean }>(`${environment.apiUrl}/runners/status`);
  }

  public runnersStatus() {
    return this.runnersStatus$().toPromise();
  }

  public runnersQueue() {
    return this.http.get<IRunnerQueue>(`${environment.apiUrl}/runners/queue`).toPromise();
  }

  public listRunners() {
    return this.http.get<IRunner[]>(`${environment.apiUrl}/runners`).toPromise();
  }
}
