import { Route } from '@angular/router';
import { NodeType } from '@hestia-earth/schema';

export default [
  {
    path: ':id',
    loadComponent: () => import('../node/node-show-page/node-show-page.component').then(m => m.NodeShowPageComponent),
    data: { type: NodeType.ImpactAssessment }
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/'
  }
] satisfies Route[];
