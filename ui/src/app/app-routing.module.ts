import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { NodeType } from '@hestia-earth/schema';

import { TermsGuard } from './terms.guard';
import { path as termsPath } from './terms.model';

const routes: Routes = [
  {
    path: 'drafts',
    loadChildren: () => import('./drafts/drafts.routes'),
    canActivate: [TermsGuard]
  },
  {
    path: `${NodeType.ImpactAssessment.toLowerCase()}`,
    loadChildren: () => import('./impact-assessments/impact-assessments.routes')
  },
  {
    path: 'results',
    loadChildren: () => import('./results/results.routes'),
    canActivate: [TermsGuard]
  },
  {
    path: 'glossary',
    loadChildren: () => import('./glossary/glossary.routes'),
    canActivate: [TermsGuard]
  },
  {
    path: 'geospatial',
    loadComponent: () =>
      import('./geospatial-search-page/geospatial-search-page.component').then(m => m.GeospatialSearchPageComponent),
    canActivate: [TermsGuard]
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.routes'),
    canActivate: [TermsGuard]
  },
  {
    path: termsPath,
    loadComponent: () => import('./terms-page/terms-page.component').then(m => m.TermsPageComponent),
    canActivate: [TermsGuard]
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'results'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      anchorScrolling: 'enabled',
      useHash: false,
      enableTracing: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
