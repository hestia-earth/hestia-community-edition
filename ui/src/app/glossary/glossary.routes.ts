import { Route } from '@angular/router';

import { GlossaryPageComponent } from './glossary-page/glossary-page.component';

export default [
  {
    path: '',
    component: GlossaryPageComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
] satisfies Route[];
