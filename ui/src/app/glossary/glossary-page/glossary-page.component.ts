import { Component, OnInit } from '@angular/core';
import { NgTemplateOutlet } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import {
  ClipboardComponent,
  CompoundPipe,
  HeNodeService,
  KeyToLabelPipe,
  NodeLinkComponent,
  SkeletonTextComponent,
  TermsPropertyContentComponent,
  TermsSubClassOfContentComponent,
  filterParams,
  glossaryBaseUrl,
  scrollToEl
} from '@hestia-earth/ui-components';
import { NodeType, ITermJSONLD, TermTermType } from '@hestia-earth/schema';
import { isEmpty, isIri } from '@hestia-earth/utils';
import {
  termTypeLabel,
  termLocationName,
  termProperties,
  HeSearchService,
  ISearchParams,
  ISearchResults,
  ISearchResultExtended,
  MAX_RESULTS,
  suggestQuery,
  sortOrders,
  matchType,
  matchTermType,
  matchExactQuery,
  matchNameNormalized,
  matchPhrasePrefixQuery,
  matchQuery,
  multiMatchQuery,
  wildcardQuery
} from '@hestia-earth/ui-components';
import { NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';

import { ApiVersionService } from '../../api-version.service';
import { FormsModule } from '@angular/forms';

const MIN_TYPEAHEAD_LENGTH = 2;

@Component({
  selector: 'app-glossary-page',
  templateUrl: './glossary-page.component.html',
  styleUrls: ['./glossary-page.component.scss'],
  standalone: true,
  imports: [
    FormsModule,
    NgTemplateOutlet,
    NgbTypeaheadModule,
    NgbPaginationModule,
    FaIconComponent,
    SkeletonTextComponent,
    TermsPropertyContentComponent,
    TermsSubClassOfContentComponent,
    NodeLinkComponent,
    ClipboardComponent,
    KeyToLabelPipe,
    CompoundPipe
  ]
})
export class GlossaryPageComponent implements OnInit {
  public glossaryBaseUrl = glossaryBaseUrl(false);
  public TermTermType = TermTermType;
  public termTypes = Object.values(TermTermType).sort();
  public termType?: TermTermType;
  public termTypeLabel = termTypeLabel;
  public isEmpty = isEmpty;

  public MAX_RESULTS = MAX_RESULTS;
  public loading = false;
  public query?: string = '';
  public results?: ISearchResults<ITermJSONLD, NodeType.Term>;
  public perPage = 50;
  public page = 1;
  public versionDate = '';
  public reportIssue = false;

  public suggest = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(term => (term.length < MIN_TYPEAHEAD_LENGTH ? [] : this.suggestTerm(term)))
    );

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private nodeService: HeNodeService,
    private searchService: HeSearchService,
    private versionService: ApiVersionService
  ) {}

  public scrollTop() {
    setTimeout(() => scrollToEl('search-results'));
  }

  async ngOnInit() {
    const params = this.route.snapshot.queryParamMap;
    this.query = params.get('query') || '';
    this.page = +params.get('page')! || 1;
    this.termType = (params.get('termType') as TermTermType) || undefined;
    this.versionDate = (await this.versionService.getVersions())?.date || '';
    return this.filter();
  }

  public trackByNode(_index: number, item: ITermJSONLD) {
    return item['@id'];
  }

  public get terms() {
    return this.results?.results || [];
  }

  public get termTypeSelected() {
    return (
      (this.termType as any) !== 'undefined' && this.termType !== undefined && typeof this.termType !== 'undefined'
    );
  }

  public get queryParams() {
    return filterParams({
      query: this.query,
      page: this.page,
      termType: this.termType
    });
  }

  private get sort() {
    return this.query ? undefined : [{ 'name.keyword': 'asc' as sortOrders }];
  }

  public get defaultSearchParams() {
    const params: ISearchParams = {
      limit: this.perPage,
      offset: this.offset,
      sort: this.sort,
      fields: ['@type', '@id', 'name', 'location', 'termType'],
      query: {
        bool: {
          must: [matchType(NodeType.Term)]
        }
      }
    };
    if (this.query) {
      params.query.bool.should = [
        // make sure exact matches come first
        matchExactQuery('@id', this.query, 1100),
        matchExactQuery('name', this.query, 1000),
        // improve country search results by matching with `gadmName` / `gadmCountry`
        multiMatchQuery(this.query, ['gadmName', 'gadmCountry'], 'cross_fields', 60, 'standard'),
        matchQuery('gadmName', this.query, 50),
        matchPhrasePrefixQuery(
          this.query,
          16,
          // TODO: use fields from Term schema
          '@id',
          'name',
          'synonyms',
          'scientificName',
          'openLCAId',
          'defaultProperties.value'
        ),
        multiMatchQuery(this.query, ['name', 'nameNormalized'], undefined, 15),
        matchQuery('name', this.query, 10),
        matchNameNormalized(this.query, 8), // match lowercase ascii
        wildcardQuery(this.query, 1, 'name') // default search to give some results
      ];
      params.query.bool.minimum_should_match = 1;
    }
    if (this.queryParams.termType) {
      params.query.bool.must.push(matchTermType(this.queryParams.termType));
    }
    return params;
  }

  private get searchParams() {
    const { query, ...params } = {
      ...this.defaultSearchParams
    };
    return {
      ...params,
      query
    };
  }

  private get suggestTermTypeQueries() {
    const queries = [];
    if (this.queryParams.termType) {
      queries.push(matchTermType(this.queryParams.termType));
    }
    return queries;
  }

  private suggestTerm(term: string) {
    return this.searchService
      .suggest$(term, NodeType.Term, [], suggestQuery(term, NodeType.Term, this.suggestTermTypeQueries))
      .pipe(map(results => results.map(({ name }) => name)));
  }

  private async updateQueryParams() {
    const queryParams = this.queryParams;
    return this.router.navigate(['/glossary'], Object.keys(queryParams).length ? { queryParams } : undefined);
  }

  public async filter() {
    await this.updateQueryParams();
    this.loading = true;
    const results = await this.searchService.search<ITermJSONLD, NodeType.Term>(this.searchParams);
    this.updateResults(results);
    this.loading = false;
  }

  public get total() {
    return this.results?.count || 0;
  }

  public get offset() {
    return (this.page - 1) * this.perPage;
  }

  /**
   * Updatings results from another source
   */
  public updateResults(results: ISearchResults<ITermJSONLD, NodeType.Term>) {
    this.results = results;
    (this.results?.results ?? []).map(res => {
      res.properties = termProperties(res);
    });
  }

  public selectResult({ '@id': id }: ITermJSONLD) {
    this.results?.results?.map((node: any) => (node.selected = node['@id'] === id));
  }

  public selectSuggestion() {
    setTimeout(() => this.runSearch());
  }

  /**
   * Click on a Type in the menu
   *
   * @param termType
   */
  public updateTermType(termType?: TermTermType) {
    if (!termType) {
      return;
    }
    this.termType = termType;
    this.scrollTop();
    return this.runSearch();
  }

  /**
   * Re-run the query
   */
  public runSearch() {
    this.page = 1;
    return this.filter();
  }

  private async extendResult(result: ITermJSONLD & ISearchResultExtended) {
    result.loading = true;
    const data = result.extended
      ? result
      : await this.nodeService.get<ITermJSONLD>({ '@type': NodeType.Term, '@id': result['@id'] });
    Object.assign(result, data);
    result.properties = termProperties(result);
    result.extended = true;
    result.loading = false;
    return data;
  }

  public async expand(result: ITermJSONLD & ISearchResultExtended) {
    result.expanded = !result.expanded;
    return this.extendResult(result);
  }

  public synonyms(term?: any) {
    return term?.synonyms ? (typeof term.synonyms === 'string' ? term.synonyms : term.synonyms.join('; ')) : null;
  }

  public isIri(value: any) {
    return typeof value === 'object' && isIri(value['@id'] || '');
  }

  public toIri(value: any) {
    return encodeURI(value['@id']);
  }
}
