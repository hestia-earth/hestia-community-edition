import { Component, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { JsonPipe } from '@angular/common';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ClipboardComponent, baseUrl } from '@hestia-earth/ui-components';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faClone as farClone } from '@fortawesome/free-regular-svg-icons';

import { ApiService, ICollection, EECollectionName, EEReducer, EEType } from '../api.service';
import { TitleComponent } from '../shared/title/title.component';

const rasters: ICollection[] = [
  {
    name: EECollectionName.soilPh,
    collection: 'users/hestiaplatform/T_PH_H2O',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.clayContent,
    collection: 'users/hestiaplatform/T_CLAY',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.sandContent,
    collection: 'users/hestiaplatform/T_SAND',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.organicCarbonPerKgSoil,
    collection: 'users/hestiaplatform/T_OC',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.totalNitrogenPerKgSoil,
    collection: 'users/hestiaplatform/T_N',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.totalPhosphorusPerKgSoil,
    collection: 'users/hestiaplatform/soil_phosphorus_concentration',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.slopeLength,
    collection: 'users/hestiaplatform/slope_length_factor',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.slope,
    collection: 'users/hestiaplatform/GMTEDSLOPE',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.drainageClass,
    collection: 'users/hestiaplatform/drainage-xy1',
    reducer: EEReducer.mode
  },
  {
    name: EECollectionName.nutrientLossToAquaticEnvironment,
    collection: 'users/hestiaplatform/r_fraction_loss_water',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.erodibility,
    collection: 'users/hestiaplatform/erodibility',
    reducer: EEReducer.mean
  },
  {
    name: EECollectionName.heavyWinterPrecipitation,
    collection: 'users/hestiaplatform/correction_winter-type_precipitation',
    reducer: EEReducer.mode
  },
  {
    name: EECollectionName.ecoClimateZone,
    collection: 'users/hestiaplatform/climate_zone',
    reducer: EEReducer.mode
  },
  {
    name: EECollectionName.histosol,
    collection: 'users/hestiaplatform/histosols_perc',
    reducer: EEReducer.mean
  }
];

interface IResult {
  name: EECollectionName;
  value: string | number;
}

const defaultValue = (value: string | number) => value;

const percentValue = (value: number) => value / 100;

const roundDecimals =
  (decimals = 1) =>
  (value: number) => {
    const multiplier = Math.pow(10, decimals);
    return Math.round(value * multiplier) / multiplier;
  };

const convertValueField: {
  [name in EECollectionName]?: (value: number) => number;
} = {
  [EECollectionName.clayContent]: percentValue,
  [EECollectionName.sandContent]: percentValue,
  [EECollectionName.organicCarbonPerKgSoil]: percentValue,
  [EECollectionName.slope]: percentValue,
  [EECollectionName.ecoClimateZone]: Math.round,
  [EECollectionName.soilPh]: roundDecimals(7)
};

const convertValue = (value: any, name: EECollectionName) => (convertValueField[name] || defaultValue)(value);

@Component({
  selector: 'app-geospatial-search-page',
  templateUrl: './geospatial-search-page.component.html',
  styleUrls: ['./geospatial-search-page.component.scss'],
  standalone: true,
  imports: [FormsModule, JsonPipe, FaIconComponent, ClipboardComponent, TitleComponent]
})
export class GeospatialSearchPageComponent {
  private readonly domSanitizer = inject(DomSanitizer);
  private readonly service = inject(ApiService);

  protected readonly farClone = farClone;

  protected readonly baseUrl = baseUrl();
  protected readonly collections = [...rasters];
  protected readonly selectedCollection = [...rasters];
  protected regionId?: string;
  protected latitude?: number;
  protected longitude?: string;

  protected submitted = false;
  protected loading = false;
  protected results: IResult[] = [];
  protected csvContent?: SafeResourceUrl;
  protected error?: any;

  protected get downloadFilename() {
    const name = this.regionId || [this.latitude, this.longitude].join('-');
    return `${name}-data.csv`;
  }

  protected get hasError() {
    return !this.regionId && (!this.latitude || !this.longitude);
  }

  private get locationData() {
    return !this.regionId
      ? {
          coordinates: [
            {
              latitude: this.latitude,
              longitude: this.longitude
            }
          ]
        }
      : { 'gadm-ids': [this.regionId] };
  }

  private get csvData() {
    return [
      `${this.regionId ? 'Region ID' : 'Coordinates'},${this.results.map(r => r.name).join(',')}`,
      `${this.regionId || [this.latitude, this.longitude].join(';')},${this.results.map(r => r.value).join(',')}`
    ].join('\n');
  }

  protected async submit() {
    this.submitted = true;
    if (this.hasError) {
      return;
    }

    this.loading = true;
    this.error = null;
    this.results = [];

    try {
      const collections = this.selectedCollection || [];
      const results = await this.service.ee(this.regionId ? 'gadm' : 'coordinates', {
        ee_type: EEType.raster,
        collections,
        ...this.locationData
      });
      this.results = collections.map(({ name }, index) => ({
        name,
        value: convertValue(results[index], name)
      }));

      this.csvContent = this.domSanitizer.bypassSecurityTrustResourceUrl(
        `data:text/html;charset=utf-8,${encodeURIComponent(this.csvData)}`
      );
    } catch (err: any) {
      console.error(err);
      this.error = `<span>There was an error:</span> <code>${err.message}</code>`;
    } finally {
      this.loading = false;
    }
  }
}
