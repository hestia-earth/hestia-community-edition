import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';

import { storageKey, versionDate } from '../terms.model';
import { TitleComponent } from '../shared/title/title.component';

@Component({
  selector: 'app-terms-page',
  templateUrl: './terms-page.component.html',
  styleUrls: ['./terms-page.component.scss'],
  standalone: true,
  imports: [TitleComponent]
})
export class TermsPageComponent {
  private readonly router = inject(Router);
  private readonly localStorage = inject(LocalStorageService);

  protected readonly versionDate = versionDate;
  protected readonly year = new Date().getFullYear();

  protected accept() {
    this.localStorage.store(storageKey, versionDate);
    return this.router.navigate(['/']);
  }
}
