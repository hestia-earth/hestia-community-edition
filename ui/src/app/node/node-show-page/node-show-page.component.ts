import { Component, OnDestroy, TemplateRef, computed, effect, inject, viewChild } from '@angular/core';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { DatePipe, JsonPipe, NgTemplateOutlet } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { from } from 'rxjs';
import { map, mergeAll, mergeMap, take, toArray } from 'rxjs/operators';
import {
  BlankNodesKey,
  ICycleJSONLD,
  IImpactAssessmentJSONLD,
  ISiteJSONLD,
  NodeType,
  TermTermType
} from '@hestia-earth/schema';
import { DataState } from '@hestia-earth/api';
import {
  ClipboardComponent,
  CollapsibleBoxComponent,
  CycleNodesKeyGroup,
  CyclesCompletenessComponent,
  CyclesNodesComponent,
  EllipsisPipe,
  EngineModelsStageComponent,
  EngineModelsVersionLinkComponent,
  HeNodeStoreService,
  INavigationMenuLink,
  ImpactAssessmentsGraphComponent,
  ImpactAssessmentsProductsComponent,
  KeyToLabelPipe,
  NodeLogsFileComponent,
  NodeMissingLookupFactorsComponent,
  ResponsiveService,
  SitesNodesComponent,
  scrollToEl,
  scrollTop
} from '@hestia-earth/ui-components';
import { filterParams, isEmpty } from '@hestia-earth/utils';
import { propertyValue } from '@hestia-earth/utils/dist/term';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import {
  faCaretDown,
  faDownload,
  faExclamationTriangle,
  faSync,
  faTimesCircle
} from '@fortawesome/free-solid-svg-icons';
import { faClone as farClone } from '@fortawesome/free-regular-svg-icons';
import { NgbDropdownModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import get from 'lodash.get';

import { environment } from '../../../environments/environment';
import { ApiService } from '../../api.service';
import { defaultConfigId } from '../../api-config.service';
import { ApiVersionService } from '../../api-version.service';
import { versionTag } from '../../utils';
import { TitleComponent } from '../../shared/title/title.component';
import { ModelsVersionComponent } from '../../shared/models-version/models-version.component';
import { RepportErrorConfirmComponent } from '../../shared/report-error-confirm/report-error-confirm.component';
import { NodeRecalculateConfirmComponent } from '../node-recalculate-confirm/node-recalculate-confirm.component';
import { NodeDeleteConfirmComponent } from '../node-delete-confirm/node-delete-confirm.component';
import { NavigationService } from '../../navigation.service';

enum Sections {
  siteMeasurements = 'Site: Measurements',
  siteManagement = 'Site: Management',
  completeness = 'Cycle: Data Completeness',
  activity = 'Cycle: Inputs & Products',
  animal = 'Cycle: Animals',
  animalActivity = 'Cycle Animals: Inputs',
  animalPractices = 'Cycle Animals: Practices',
  practices = 'Cycle: Practices',
  emissions = 'Cycle: Emissions',
  transformationActivity = 'Cycle Transformations: Inputs & Products',
  transformationPractices = 'Cycle Transformations: Practices',
  transformationEmissions = 'Cycle Transformations: Emissions',
  impactEmissions = 'Impact Assessment: Emissions',
  impactResourceUse = 'Impact Assessment: Resource Uses',
  impactImpacts = 'Impact Assessment: Impact Indicators',
  impactEndpoints = 'Impact Assessment: Endpoints',
  impactDriverChart = 'Impact Assessment: Driver Chart'
}

const groupToSection: {
  [group: string]: Sections[];
} = {
  Site: [Sections.siteMeasurements, Sections.siteManagement],
  Cycle: [
    Sections.completeness,
    Sections.activity,
    Sections.animal,
    Sections.animalActivity,
    Sections.animalPractices,
    Sections.practices,
    Sections.emissions,
    Sections.transformationActivity,
    Sections.transformationEmissions,
    Sections.transformationPractices
  ],
  'Impact Assessment': [
    Sections.impactDriverChart,
    Sections.impactEmissions,
    Sections.impactEndpoints,
    Sections.impactImpacts,
    Sections.impactResourceUse
  ]
};

const sectionGroup = (section: Sections) =>
  Object.keys(groupToSection).find(key => groupToSection[key].includes(section)) || '';

const hasBlankNodes = (nodes: (ICycleJSONLD | ISiteJSONLD)[], blankNodeKey: BlankNodesKey) =>
  nodes.some(node => get(node, blankNodeKey, []).length > 0);

const hasBlankSubvalues = (
  nodes: (ICycleJSONLD | ISiteJSONLD)[],
  blankNodeKey: BlankNodesKey.animals,
  field?: 'inputs' | 'practices'
) => nodes.some(node => get(node, blankNodeKey, []).some(a => (a[field!] as any)?.length > 0));

const getNodeValue = (node: any, arrayKey: string, term: string, factor = 1, method?: string) => {
  const blankNode = (node[arrayKey] || []).find(
    (v: any) => v.term['@id'] === term && (!method || !v.methodModel || v.methodModel['@id'] === method)
  );
  const value = propertyValue(blankNode?.value ?? [], blankNode?.term['@id']) as number;
  return value === null ? '' : value * factor;
};

const sumValues = (...values: (number | '')[]) => {
  const numbers = values.filter(v => v !== '') as number[];
  return numbers.length > 0 ? numbers.reduce((p, c) => p + c, 0) : '';
};

const convertToCsv = (impact: IImpactAssessmentJSONLD, date?: number) =>
  [
    [
      'ID',
      'Date',
      'Land Use (m2*yr)',
      'Biodiv. (PDF.yr*10^14)',
      'GHG Emis Farm (kg CO2eq)',
      'Eutr Pot Farm (kg PO4 eq)',
      'Freshwtr Withdr Farm (L)',
      'Wtr Sc Farm (L eq)'
    ].join(','),
    [
      impact['@id'],
      date ? new Date(date).toJSON() : '',
      sumValues(
        getNodeValue(impact, 'emissionsResourceUse', 'landOccupationDuringCycle'),
        getNodeValue(impact, 'emissionsResourceUse', 'landOccupationInputsProduction')
      ),
      getNodeValue(impact, 'impacts', 'damageToTerrestrialEcosystemsTotalLandUseEffects', Math.pow(10, 14)) ||
        getNodeValue(impact, 'impacts', 'damageToTerrestrialEcosystemsLandOccupation', Math.pow(10, 14)),
      getNodeValue(impact, 'impacts', 'gwp100', 1),
      getNodeValue(impact, 'impacts', 'eutrophicationPotentialExcludingFate'),
      sumValues(
        getNodeValue(impact, 'emissionsResourceUse', 'freshwaterWithdrawalsDuringCycle'),
        getNodeValue(impact, 'emissionsResourceUse', 'freshwaterWithdrawalsInputsProduction')
      ),
      getNodeValue(impact, 'impacts', 'scarcityWeightedWaterUse')
    ]
  ].join('\n');

const updateByVersion = (version?: string) => (data: any) =>
  version && !isEmpty(data)
    ? {
        ...data,
        '@id': [data['@id'], versionTag, version].join(''),
        name: `Version ${version}`
      }
    : data;

@Component({
  selector: 'app-node-show-page',
  templateUrl: './node-show-page.component.html',
  styleUrls: ['./node-show-page.component.scss'],
  standalone: true,
  imports: [
    NgTemplateOutlet,
    DatePipe,
    JsonPipe,
    FormsModule,
    RouterLink,
    FaIconComponent,
    EllipsisPipe,
    KeyToLabelPipe,
    NgbDropdownModule,
    ClipboardComponent,
    CollapsibleBoxComponent,
    CyclesCompletenessComponent,
    CyclesNodesComponent,
    SitesNodesComponent,
    ImpactAssessmentsProductsComponent,
    ImpactAssessmentsGraphComponent,
    NodeMissingLookupFactorsComponent,
    NodeLogsFileComponent,
    EngineModelsStageComponent,
    EngineModelsVersionLinkComponent,
    TitleComponent,
    ModelsVersionComponent
  ]
})
export class NodeShowPageComponent implements OnDestroy {
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly domSanitizer = inject(DomSanitizer);
  private readonly modalService = inject(NgbModal);
  private readonly nodeStoreService = inject(HeNodeStoreService);
  private readonly service = inject(ApiService);
  private readonly apiVersionService = inject(ApiVersionService);
  private readonly navigationService = inject(NavigationService);
  protected readonly responsiveService = inject(ResponsiveService);

  protected readonly farClone = farClone;
  protected readonly faCaretDown = faCaretDown;
  protected readonly faExclamationTriangle = faExclamationTriangle;
  protected readonly faTimesCircle = faTimesCircle;
  protected readonly faDownload = faDownload;
  protected readonly faSync = faSync;

  private readonly rightDrawerContent = viewChild<TemplateRef<any>>('rightDrawerContent');

  protected readonly DataState = DataState;
  protected readonly NodeType = NodeType;
  protected readonly TermTermType = TermTermType;
  protected readonly BlankNodesKey = BlankNodesKey;
  protected readonly CycleNodesKeyGroup = CycleNodesKeyGroup;

  protected readonly dataStates = [DataState.original, DataState.recalculated];

  protected readonly Sections = Sections;
  private readonly showSection = computed(() => ({
    [Sections.completeness]: this.cycles().length > 0,
    [Sections.siteMeasurements]: this.sites().length > 0,
    [Sections.siteManagement]: this.sites().length > 0,
    [Sections.activity]: this.cycles().length > 0,
    [Sections.animal]: hasBlankNodes(this.cycles(), BlankNodesKey.animals),
    [Sections.animalActivity]: hasBlankSubvalues(this.cycles(), BlankNodesKey.animals, 'inputs'),
    [Sections.animalPractices]: hasBlankSubvalues(this.cycles(), BlankNodesKey.animals, 'practices'),
    [Sections.practices]: this.cycles().length > 0,
    [Sections.emissions]: this.cycles().length > 0,
    [Sections.transformationActivity]: hasBlankNodes(this.cycles(), BlankNodesKey.transformations),
    [Sections.transformationPractices]: hasBlankNodes(this.cycles(), BlankNodesKey.transformations),
    [Sections.transformationEmissions]: hasBlankNodes(this.cycles(), BlankNodesKey.transformations),
    [Sections.impactEmissions]: this.impactAssessments().length > 0,
    [Sections.impactResourceUse]: this.impactAssessments().length > 0,
    [Sections.impactImpacts]: this.impactAssessments().length > 0,
    [Sections.impactEndpoints]: this.impactAssessments().length > 0,
    [Sections.impactDriverChart]: this.impactAssessments().length > 0
  }));
  /* eslint-disable-next-line */
  // @ts-ignore
  private readonly sectionsMap = computed(() =>
    Object.entries(Sections).filter(([key]) => (this.showSection() as any)[(Sections as any)[key]])
  );
  protected readonly sections = computed(() => this.sectionsMap().map(([key]) => key));
  private readonly sectionsGroups = computed(() =>
    this.sectionsMap().reduce(
      (prev, [key, value], index) => {
        const group = sectionGroup(value as Sections);
        prev[group] = [
          ...(prev[group] || []),
          {
            title: `${index + 1}. ${value.replace(group, '').replace(':', '').trim()}`,
            url: this.currentUrl,
            fragment: key,
            queryParams: filterParams({
              dataState: this.selectedDataState()
            })
          }
        ];
        return prev;
      },
      {} as { [group: string]: INavigationMenuLink[] }
    )
  );
  private readonly links = computed(() =>
    Object.entries(this.sectionsGroups()).flatMap(([group, sections]) =>
      group
        ? {
            title: group,
            expanded: true,
            links: sections
          }
        : sections
    )
  );

  private readonly type$ = this.route.data.pipe(map(({ type }) => type as NodeType));
  protected readonly type = toSignal(this.type$);
  protected readonly id = toSignal(this.route.params.pipe(map(({ id }) => id as string)));
  protected readonly selectedDataState = toSignal(
    this.route.queryParams.pipe(
      map(({ dataState }) => dataState as DataState),
      map(dataState => dataState || DataState.original)
    )
  );
  protected readonly fragment = toSignal(this.route.fragment);

  protected readonly apiVersions$ = this.apiVersionService.versions$;
  protected readonly latestApiVersion = toSignal(this.apiVersions$.pipe(map(({ current }) => current)));

  protected readonly cycleMetadata = toSignal(
    this.route.params.pipe(mergeMap(params => this.service.getMetadata$(NodeType.Cycle, params.id)))
  );
  protected readonly siteMetadata = toSignal(
    this.route.params.pipe(mergeMap(params => this.service.getMetadata$(NodeType.Site, params.id)))
  );
  protected readonly impactAssessmentMetadata = toSignal(
    this.route.params.pipe(mergeMap(params => this.service.getMetadata$(NodeType.ImpactAssessment, params.id)))
  );

  protected readonly errors = computed(() =>
    [this.cycleMetadata()?.error, this.siteMetadata()?.error, this.impactAssessmentMetadata()?.error].filter(Boolean)
  );

  private readonly allNodes = toSignal(this.nodeStoreService.sortedNodes$());
  protected readonly cycles = computed(
    () =>
      this.allNodes()
        ?.[NodeType.Cycle]?.map(data => data[DataState.recalculated] as ICycleJSONLD)
        ?.filter(Boolean) || []
  );
  protected readonly sites = computed(
    () =>
      this.allNodes()
        ?.[NodeType.Site]?.map(data => data[DataState.recalculated] as ISiteJSONLD)
        ?.filter(Boolean) || []
  );
  protected readonly impactAssessments = computed(
    () =>
      this.allNodes()
        ?.[NodeType.ImpactAssessment]?.map(data => data[DataState.recalculated] as IImpactAssessmentJSONLD)
        ?.filter(Boolean) || []
  );
  protected readonly impactAssessment = computed(() => this.impactAssessments()?.[0] as IImpactAssessmentJSONLD);

  protected readonly dataVersions = computed(() => this.cycleMetadata()?.versions ?? []);
  private readonly dataVersions$ = toObservable(this.dataVersions);

  protected readonly config = computed(() => this.cycleMetadata()?.config);
  protected readonly isDefaultConfig = computed(
    () => !this.config()?.config || this.config()?.config === defaultConfigId
  );
  protected readonly currentVersion = computed(() => this.config()?.version);

  protected readonly sourceUrl = computed(() =>
    this.cycleMetadata()?.sourcePath
      ? `${environment.apiUrl}/download?path=${this.cycleMetadata()?.sourcePath}`
      : undefined
  );
  protected readonly impactsCSV = computed(() =>
    this.impactAssessment()
      ? this.domSanitizer.bypassSecurityTrustResourceUrl(
          `data:text/html;charset=utf-8,${encodeURIComponent(
            convertToCsv(this.impactAssessment(), this.impactAssessmentMetadata()?.recalculatedOn)
          )}`
        )
      : undefined
  );

  constructor() {
    // after fetching the list of available versions, load all the nodes by version
    this.dataVersions$.subscribe(() => this.init());

    effect(() => this.navigationService.links.set(this.links()), { allowSignalWrites: true });
    effect(
      () =>
        this.navigationService.rightDrawerContent.set(
          this.responsiveService.isTouch() ? (undefined as any) : this.rightDrawerContent()!
        ),
      {
        allowSignalWrites: true
      }
    );
  }

  ngOnDestroy() {
    this.nodeStoreService.clearNodes();
    this.navigationService.links.set([]);
    this.navigationService.rightDrawerContent.set(undefined as any);
  }

  private get currentUrl() {
    return this.router.url.split('?')[0];
  }

  private loadNodesByVersion(version: string) {
    return from([NodeType.Cycle, NodeType.ImpactAssessment, NodeType.Site]).pipe(
      mergeMap(nodeType =>
        this.nodeStoreService.addNode({ '@type': nodeType, '@id': this.id()! }, { version }, updateByVersion(version))
      )
    );
  }

  private init() {
    this.nodeStoreService.clearNodes();

    return this.dataVersions$
      .pipe(
        take(1),
        mergeAll(),
        mergeMap(version => this.loadNodesByVersion(version)),
        toArray()
      )
      .subscribe(() => {
        this.fragment() ? scrollToEl(this.fragment()!) : scrollTop();
      });
  }

  protected isFragment(fragment: string) {
    return this.fragment() === fragment;
  }

  protected sectionTitle(section: string) {
    return (Sections as any)[section];
  }

  protected selectDataState(dataState: DataState) {
    const queryParams = dataState === DataState.original ? {} : filterParams({ dataState });
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      ...(Object.keys(queryParams).length ? { queryParams } : {}),
      preserveFragment: true
    });
  }

  protected showRecalculate() {
    const instance = this.modalService.open(NodeRecalculateConfirmComponent);
    const component = instance.componentInstance as NodeRecalculateConfirmComponent;
    component.type.set(this.type()!);
    component.id.set(this.id()!);
    component.config.set(this.config()?.config || '');
  }

  protected showDelete() {
    const instance = this.modalService.open(NodeDeleteConfirmComponent);
    const component = instance.componentInstance as NodeDeleteConfirmComponent;
    component.type.set(this.type()!);
    component.id.set(this.id());
    instance.closed.subscribe((deleted: boolean) => (deleted ? this.router.navigate(['/results']) : null));
  }

  protected showError() {
    const instance = this.modalService.open(RepportErrorConfirmComponent);
    const component = instance.componentInstance as RepportErrorConfirmComponent;
    component.error.set(this.errors()?.[0]?.stack);
  }
}
