import { Component, inject, model, output, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { from, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { HeToastService } from '@hestia-earth/ui-components';
import { NodeType } from '@hestia-earth/schema';
import { NgbActiveModal, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';

import { ApiService } from '../../api.service';
import { ApiConfigService, defaultConfigId } from '../../api-config.service';
import { ApiSettingsService } from '../../api-settings.service';
import { calculationSuccessMessage } from '../../drafts/drafts.model';
import { ReportErrorComponent } from '../../shared/report-error/report-error.component';

@Component({
  selector: 'app-node-recalculate-confirm',
  templateUrl: './node-recalculate-confirm.component.html',
  styleUrls: ['./node-recalculate-confirm.component.scss'],
  standalone: true,
  imports: [RouterLink, FormsModule, FaIconComponent, NgbTypeaheadModule, ReportErrorComponent]
})
export class NodeRecalculateConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });
  private readonly service = inject(ApiService);
  private readonly configService = inject(ApiConfigService);
  private readonly settingsService = inject(ApiSettingsService);
  private readonly toastService = inject(HeToastService);

  public readonly type = model(NodeType.ImpactAssessment);
  public readonly id = model<string>('');
  public readonly config = model<string>(defaultConfigId);

  protected readonly closed = output<boolean>();

  protected readonly loading = signal(false);
  protected readonly error = signal('');

  protected readonly suggestConfig = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap(search =>
        from(this.configService.listConfigs({ search })).pipe(map(results => results.results.map(({ id }) => id)))
      )
    );

  protected close(value = false) {
    this.closed.emit(value);
    this.activeModal?.close(value);
  }

  protected async confirm() {
    this.loading.set(true);
    try {
      const enableQueuing = await this.settingsService.getSingle<boolean>('enableQueuing');
      await this.service.calculateNode$(this.type(), this.id(), this.config(), enableQueuing).toPromise();
      this.toastService.success(calculationSuccessMessage(enableQueuing));
      this.close(true);
    } catch (err: any) {
      this.error?.set(err?.error?.detail);
    }
  }
}
