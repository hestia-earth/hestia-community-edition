import { Component, inject, model, output } from '@angular/core';
import { NodeType } from '@hestia-earth/schema';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { errorText } from '@hestia-earth/ui-components';

import { ApiService } from '../../api.service';

@Component({
  selector: 'app-node-delete-confirm',
  templateUrl: './node-delete-confirm.component.html',
  styleUrls: ['./node-delete-confirm.component.scss'],
  standalone: true,
  imports: [FaIconComponent]
})
export class NodeDeleteConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });
  private readonly service = inject(ApiService);

  public readonly type = model(NodeType.ImpactAssessment);
  public readonly id = model<string>();

  protected readonly closed = output<boolean>();

  protected loading = false;
  protected error?: string;

  protected close(value = false) {
    this.closed.emit(value);
    this.activeModal?.close(value);
  }

  protected async confirm() {
    this.loading = true;
    try {
      this.id() && (await this.service.delete(this.type(), this.id()!));
      this.close(true);
    } catch (err) {
      this.error = errorText(err);
    }
  }
}
