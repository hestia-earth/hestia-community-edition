import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { of, ReplaySubject } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { gitHome, Repository } from '@hestia-earth/ui-components';

import { environment } from '../environments/environment';

export interface IVersion {
  current: string;
  date?: string;
  latest: string;
  isLatest: boolean;
  isPrerelease: boolean;
  schema: string;
  models: string;
  validation: string;
}

export interface IVersionData {
  version: string;
  isPrerelease?: boolean;
  commitSha?: string;
  date?: string;
}

export const gitUrl = [gitHome, Repository.community].join('/');

export const versionUrl = ({ isPrerelease, version, commitSha }: IVersionData) =>
  isPrerelease
    ? `${gitUrl}/-/tree/v${version}`
    : commitSha
      ? `${gitUrl}/-/commit/${commitSha}`
      : `${gitUrl}/-/releases/v${version}`;

const parseDate = (date: string) => [date.substring(0, 4), date.substring(4, 6), date.substring(6, 8)].join('-');

export const parseVersion = (version: string): IVersionData => {
  const parts = version.split('-');
  return parts.length === 2
    ? {
        version,
        isPrerelease: true
      }
    : parts.length === 3
      ? {
          version: parts[0],
          isPrerelease: false,
          commitSha: parts[1],
          date: parseDate(parts[2])
        }
      : {
          version,
          isPrerelease: false
        };
};

@Injectable({
  providedIn: 'root'
})
export class ApiVersionService {
  private http = inject(HttpClient);

  private _version = new ReplaySubject<string>(1);
  private _pandasVersion = new ReplaySubject<string>(1);

  private versionsLoading = false;
  private versionsLoaded = false;
  private _versions = new ReplaySubject<IVersion>(1);

  private loadVersions() {
    this.versionsLoading = true;
    return this.http.get<IVersion>(`${environment.apiUrl}/version`).pipe(
      catchError(() => of({} as IVersion)),
      map(data => {
        this._versions.next(data);
        this.versionsLoading = false;
        this.versionsLoaded = true;
        return data;
      })
    );
  }

  public get versions$() {
    return this.versionsLoading || this.versionsLoaded ? this._versions.asObservable() : this.loadVersions();
  }

  public getVersions() {
    return this.versions$.pipe(take(1)).toPromise();
  }

  public updateVersion(version: string) {
    this._version.next(version);
  }

  public get version$() {
    return this._version.asObservable();
  }

  public get versionData$() {
    return this.version$.pipe(map(parseVersion));
  }

  public getVersionData() {
    return this.versionData$.pipe(take(1)).toPromise();
  }

  public updatePandasVersion(version: string) {
    this._pandasVersion.next(version);
  }

  public get pandasVersion$() {
    return this._pandasVersion.asObservable();
  }
}
