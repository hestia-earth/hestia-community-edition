import { Component, computed, inject } from '@angular/core';
import { IsActiveMatchOptions, RouterOutlet } from '@angular/router';
import {
  DrawerContainerComponent,
  INavigationMenuLink,
  IssueConfirmComponent,
  Repository,
  ToastComponent
} from '@hestia-earth/ui-components';
import { toSignal } from '@angular/core/rxjs-interop';
import { map } from 'rxjs/operators';
import { faBug } from '@fortawesome/free-solid-svg-icons';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { NavbarComponent } from './shared/navbar/navbar.component';
import { CheckNewVersionComponent } from './shared/check-new-version/check-new-version.component';
import { ApiService } from './api.service';
import { NavigationService } from './navigation.service';

const pageActiveMatchOptions: IsActiveMatchOptions = {
  matrixParams: 'subset',
  queryParams: 'ignored',
  fragment: 'ignored',
  paths: 'exact'
};

const subpageActiveMatchOptions: IsActiveMatchOptions = {
  matrixParams: 'subset',
  queryParams: 'ignored',
  fragment: 'ignored',
  paths: 'subset'
};

const resultsLink: INavigationMenuLink = {
  title: 'Results',
  icon: 'folder',
  url: '/results',
  activeMatchOptions: subpageActiveMatchOptions,
  queryParams: { page: '1', sortBy: 'createdOn', sortOrder: 'desc' }
};

const links: INavigationMenuLink[] = [
  resultsLink,
  {
    title: 'Drafts',
    expanded: true,
    links: [
      {
        title: 'List',
        url: '/drafts',
        activeMatchOptions: pageActiveMatchOptions,
        queryParams: { page: '1', sortBy: 'createdOn', sortOrder: 'desc' }
      },
      {
        title: 'New Draft',
        url: '/drafts/new'
      },
      {
        title: 'Upload',
        icon: 'far-upload',
        url: '/drafts/upload'
      }
    ]
  },
  {
    title: 'Glossary of Terms',
    url: '/glossary',
    icon: 'glossary'
  },
  {
    title: 'Geospatial Search',
    url: '/geospatial',
    icon: 'map'
  }
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  standalone: true,
  imports: [
    RouterOutlet,
    ToastComponent,
    FaIconComponent,
    NavbarComponent,
    CheckNewVersionComponent,
    DrawerContainerComponent
  ]
})
export class AppComponent {
  private readonly modalService = inject(NgbModal);
  private readonly service = inject(ApiService);
  protected readonly navigationService = inject(NavigationService);

  protected readonly faBug = faBug;

  private readonly runnersEnabled = toSignal(this.service.runnersStatus$().pipe(map(({ enabled }) => enabled)));

  protected readonly links = computed(() =>
    this.navigationService.links()?.length
      ? [resultsLink, ...this.navigationService.links()]
      : [
          ...links,
          {
            title: 'Settings',
            url: '/settings',
            icon: 'far-gear',
            links: [
              {
                title: 'Configurations',
                url: '/settings/config',
                activeMatchOptions: subpageActiveMatchOptions
              },
              this.runnersEnabled()
                ? {
                    title: 'Runners',
                    url: '/settings/runners'
                  }
                : null
            ].filter(Boolean)
          }
        ]
  );

  protected showReportIssue() {
    const instance = this.modalService.open(IssueConfirmComponent);
    const component = instance.componentInstance as IssueConfirmComponent;
    component.isCommunity.set(true);
    component.repository.set(Repository.community);
  }
}
