import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { PluralizePipe, filterParams, scrollToEl } from '@hestia-earth/ui-components';
import orderBy from 'lodash.orderby';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe, SlicePipe } from '@angular/common';
import { faSortAmountUpAlt, faSync } from '@fortawesome/free-solid-svg-icons';

import { ApiService, IRunner, IRunnerQueue } from '../../api.service';
import { TitleComponent } from '../../shared/title/title.component';

type sortByFields = keyof IRunner;
type sortByOrder = 'asc' | 'desc';

@Component({
  selector: 'app-settings-runners-page',
  templateUrl: './settings-runners-page.component.html',
  styleUrls: ['./settings-runners-page.component.scss'],
  standalone: true,
  imports: [RouterLink, DatePipe, FaIconComponent, NgbPaginationModule, SlicePipe, PluralizePipe, TitleComponent]
})
export class SettingsRunnersPageComponent implements OnInit {
  protected readonly faSortAmountUpAlt = faSortAmountUpAlt;
  protected readonly faSync = faSync;

  public loading = true;
  public enabled?: boolean;
  public runners: IRunner[] = [];
  public queue?: IRunnerQueue;
  public perPage = 10;
  public page = 1;
  public showSortBy = false;
  public sortBy: sortByFields = 'active';
  public sortOrder: sortByOrder = 'desc';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: ApiService
  ) {}

  async ngOnInit() {
    this.enabled = (await this.service.runnersStatus()).enabled;
    const params = this.route.snapshot.queryParamMap;
    this.sortBy = (params.get('sortBy') as sortByFields) || 'active';
    this.sortOrder = (params.get('sortOrder') as sortByOrder) || 'desc';
    this.page = +(params.get('page') || '1');
    return this.runSearch();
  }

  public scrollTop() {
    setTimeout(() => scrollToEl('search-results'));
  }

  public trackByRunner(_index: number, { id }: IRunner) {
    return id;
  }

  sort(sortBy: sortByFields) {
    this.sortOrder = this.sortBy === sortBy ? (this.sortOrder === 'asc' ? 'desc' : 'asc') : 'desc';
    this.sortBy = sortBy;
    return this.runSearch();
  }

  private get queryParams() {
    return filterParams({
      page: this.page,
      sortBy: this.sortBy,
      sortOrder: this.sortOrder
    });
  }

  private async updateQueryParams() {
    const queryParams = this.queryParams;
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      ...(Object.keys(queryParams).length ? { queryParams } : {})
    });
  }

  public async runSearch() {
    this.page = 1;
    return this.filter();
  }

  public async filter() {
    await this.updateQueryParams();

    this.loading = true;
    const runners = await this.service.listRunners();
    this.runners = orderBy(runners, [this.sortBy], [this.sortOrder]);
    this.queue = await this.service.runnersQueue();
    this.loading = false;
  }

  public get total() {
    return this.runners.length;
  }

  public get offset() {
    return (this.page - 1) * this.perPage;
  }

  public get limit() {
    return this.offset + this.perPage;
  }
}
