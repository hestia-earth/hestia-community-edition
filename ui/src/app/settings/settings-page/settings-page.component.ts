import { Component, effect, inject, signal } from '@angular/core';
import { KeyValuePipe } from '@angular/common';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { map, startWith, tap } from 'rxjs/operators';

import { ApiSettingsService, DataFormat, defaultSettings } from '../../api-settings.service';
import { ApiService } from '../../api.service';
import { TitleComponent } from '../../shared/title/title.component';
import { ApiVersionComponent } from '../../shared/api-version/api-version.component';
import { UiVersionComponent } from '../../shared/ui-version/ui-version.component';

@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss'],
  standalone: true,
  imports: [
    RouterLink,
    KeyValuePipe,
    ReactiveFormsModule,
    FaIconComponent,
    TitleComponent,
    ApiVersionComponent,
    UiVersionComponent
  ]
})
export class SettingsPageComponent {
  private readonly formBuilder = inject(FormBuilder);
  private readonly service = inject(ApiService);
  private readonly settingsService = inject(ApiSettingsService);

  protected readonly runnersEnabled = toSignal(this.service.runnersStatus$().pipe(map(({ enabled }) => enabled)));

  protected readonly DataFormat = DataFormat;
  protected readonly loading = signal(true);
  protected readonly settings = toSignal(
    this.settingsService.getAll$().pipe(
      tap(() => this.loading.set(false)),
      startWith(defaultSettings)
    )
  );
  protected readonly form = this.formBuilder.nonNullable.group({
    defaultFormat: [DataFormat.Hestia],
    enableQueuing: [false],
    enableAggregatedModels: [false]
  });
  protected readonly saving = signal(false);
  protected readonly error = signal('');

  constructor() {
    effect(() => this.form.patchValue(this.settings() || defaultSettings));
  }

  protected async save() {
    this.saving.set(true);
    this.error.set('');
    try {
      await this.settingsService.updateAll(this.form.value);
    } catch (err: any) {
      console.error(err);
      this.error.set(err.stack || err.message);
    } finally {
      this.saving.set(false);
    }
  }
}
