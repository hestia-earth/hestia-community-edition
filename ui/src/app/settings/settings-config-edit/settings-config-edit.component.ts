import { Component, computed, effect, input, model, output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { JsonEditorOptions, JsonEditorComponent } from 'ang-jsoneditor';
import { create, formatters } from 'jsondiffpatch';
import { IOrchestratorConfig } from '@hestia-earth/engine-models';

const diff = create();

@Component({
  selector: 'app-settings-config-edit',
  templateUrl: './settings-config-edit.component.html',
  styleUrls: ['./settings-config-edit.component.scss'],
  standalone: true,
  imports: [FormsModule, JsonEditorComponent]
})
export class SettingsConfigEditComponent {
  protected readonly oldConfig = input.required<IOrchestratorConfig>();
  protected readonly newConfig = model.required<IOrchestratorConfig>();
  protected readonly editable = input(true);

  protected readonly configChanged = output<IOrchestratorConfig>();

  protected readonly editorOptions = new JsonEditorOptions();
  protected readonly diffHtml = computed(() =>
    formatters.html.format(diff.diff(this.oldConfig(), this.newConfig())!, this.oldConfig())
  );

  constructor() {
    effect(() => (this.editorOptions.mode = this.editable() ? 'code' : 'text'));
  }

  protected onConfigChange(value: IOrchestratorConfig) {
    this.newConfig.set(value);
    this.configChanged.emit(value);
  }
}
