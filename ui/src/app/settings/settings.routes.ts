import { Route } from '@angular/router';

import { SettingsPageComponent } from './settings-page/settings-page.component';
import { SettingsConfigListPageComponent } from './settings-config-list-page/settings-config-list-page.component';
import { SettingsConfigEditPageComponent } from './settings-config-edit-page/settings-config-edit-page.component';
import { SettingsConfigShowPageComponent } from './settings-config-show-page/settings-config-show-page.component';
import { SettingsRunnersPageComponent } from './settings-runners-page/settings-runners-page.component';

export default [
  {
    path: '',
    component: SettingsPageComponent
  },
  {
    path: 'config',
    component: SettingsConfigListPageComponent
  },
  {
    path: 'config/:id',
    component: SettingsConfigShowPageComponent
  },
  {
    path: 'config/:id/edit',
    component: SettingsConfigEditPageComponent
  },
  {
    path: 'runners',
    component: SettingsRunnersPageComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: ''
  }
] satisfies Route[];
