import { Component, computed, inject, signal } from '@angular/core';
import { KeyValuePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';
import { toSignal } from '@angular/core/rxjs-interop';
import { NodeType } from '@hestia-earth/schema';
import { isUndefined } from '@hestia-earth/utils';
import { IOrchestratorConfig } from '@hestia-earth/engine-models';
import { JsonEditorComponent } from 'ang-jsoneditor';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiConfigService, defaultConfigId } from '../../api-config.service';
import { TitleComponent } from '../../shared/title/title.component';
import { SettingsConfigEditComponent } from '../settings-config-edit/settings-config-edit.component';
import { SettingsConfigDeleteConfirmComponent } from '../settings-config-delete-confirm/settings-config-delete-confirm.component';

const copy = (data: any) => JSON.parse(JSON.stringify(data));

const isEqual = (a1: any, a2: any) => JSON.stringify(a1) === JSON.stringify(a2);

@Component({
  selector: 'app-settings-config-edit-page',
  templateUrl: './settings-config-edit-page.component.html',
  styleUrls: ['./settings-config-edit-page.component.scss'],
  standalone: true,
  imports: [
    FormsModule,
    RouterLink,
    FaIconComponent,
    KeyValuePipe,
    JsonEditorComponent,
    TitleComponent,
    SettingsConfigEditComponent
  ]
})
export class SettingsConfigEditPageComponent {
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly modalService = inject(NgbModal);
  private readonly configService = inject(ApiConfigService);

  protected readonly faAngleDown = faAngleDown;
  protected readonly faAngleRight = faAngleRight;

  private readonly id$ = this.route.params.pipe(map(({ id }) => id as string));
  protected readonly id = toSignal(this.id$);
  protected readonly configs = toSignal(
    this.id$.pipe(
      filter(v => !!v),
      mergeMap(id => this.configService.getConfigs$(id))
    )
  );

  protected readonly loading = computed(() => isUndefined(this.configs()));
  protected readonly canEdit = computed(() => this.id() !== defaultConfigId);

  protected configClosed: any = {};
  protected readonly saving = signal(false);

  protected showDelete() {
    const instance = this.modalService.open(SettingsConfigDeleteConfirmComponent);
    const component = instance.componentInstance as SettingsConfigDeleteConfirmComponent;
    component.id.set(this.id()!);
    instance.closed.subscribe(confirmed => confirmed && this.router.navigate(['/settings']));
  }

  protected setConfig(config: IOrchestratorConfig, type: NodeType) {
    (this.configs() as any)[type].config = config;
  }

  protected hasOverride(type: NodeType) {
    return !isEqual((this.configs() as any)[type].default, (this.configs() as any)[type].config);
  }

  protected async reset(type: NodeType) {
    (this.configs() as any)[type].config = copy((this.configs() as any)[type].default);
  }

  protected async update() {
    this.saving.set(true);
    await Promise.all(
      Object.entries(this.configs()!).map(([type, value]) =>
        this.configService.updateConfig(this.id()!, type as NodeType, value.config)
      )
    );
    this.saving.set(false);
  }
}
