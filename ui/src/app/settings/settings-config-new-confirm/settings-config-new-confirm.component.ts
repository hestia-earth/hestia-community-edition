import { Component, inject, output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiConfigService, defaultConfigId } from '../../api-config.service';

@Component({
  selector: 'app-settings-config-new-confirm',
  templateUrl: './settings-config-new-confirm.component.html',
  styleUrls: ['./settings-config-new-confirm.component.scss'],
  standalone: true,
  imports: [FormsModule, FaIconComponent]
})
export class SettingsConfigNewConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });
  private readonly configService = inject(ApiConfigService);

  protected readonly closed = output<string>();

  protected loading = false;
  protected id = '';
  protected submitted = false;

  protected get formValid() {
    return this.id !== defaultConfigId;
  }

  protected close(value = '') {
    this.closed.emit(value);
    this.activeModal?.close(value);
  }

  protected async confirm() {
    this.submitted = true;
    if (!this.id || !this.formValid) {
      return;
    }

    this.loading = true;
    await this.configService.createConfig(this.id);
    return this.close(this.id);
  }
}
