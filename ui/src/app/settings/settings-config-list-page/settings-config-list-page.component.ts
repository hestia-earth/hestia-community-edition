import { Component, computed, effect, inject, signal } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { DatePipe, NgTemplateOutlet } from '@angular/common';
import { scrollToEl, PluralizePipe, distinctUntilChangedDeep } from '@hestia-earth/ui-components';
import { NgbDropdownModule, NgbModal, NgbPaginationModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faSortAmountUpAlt } from '@fortawesome/free-solid-svg-icons';

import { sortByFields, sortByOrder } from '../../api.service';
import { ApiConfigService, IConfigListResult, defaultConfigId } from '../../api-config.service';
import { TitleComponent } from '../../shared/title/title.component';
import { ApiVersionComponent } from '../../shared/api-version/api-version.component';
import { UiVersionComponent } from '../../shared/ui-version/ui-version.component';
import { SettingsConfigNewConfirmComponent } from '../settings-config-new-confirm/settings-config-new-confirm.component';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';
import { map, mergeMap, startWith, tap } from 'rxjs/operators';

@Component({
  selector: 'app-settings-config-list-page',
  templateUrl: './settings-config-list-page.component.html',
  styleUrls: ['./settings-config-list-page.component.scss'],
  standalone: true,
  imports: [
    NgTemplateOutlet,
    FormsModule,
    RouterLink,
    DatePipe,
    FaIconComponent,
    NgbPaginationModule,
    NgbTooltipModule,
    NgbDropdownModule,
    PluralizePipe,
    TitleComponent,
    ApiVersionComponent,
    UiVersionComponent,
    SettingsConfigNewConfirmComponent
  ]
})
export class SettingsConfigListPageComponent {
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly modalService = inject(NgbModal);
  private readonly configService = inject(ApiConfigService);

  protected readonly faSortAmountUpAlt = faSortAmountUpAlt;

  protected readonly defaultConfigId = defaultConfigId;
  protected readonly perPage = 10;

  protected search? = '';
  protected page = 1;

  private readonly query = toSignal(this.route.queryParams.pipe(map(({ query }) => query as string)));
  protected readonly sortBy = toSignal(
    this.route.queryParams.pipe(map(({ sortBy }) => (sortBy as sortByFields) || 'createdOn'))
  );
  protected readonly sortOrder = toSignal(
    this.route.queryParams.pipe(map(({ sortOrder }) => (sortOrder as sortByOrder) || 'desc'))
  );
  protected readonly currentPage = toSignal(
    this.route.queryParams.pipe(
      map(({ page }) => +page || 1),
      startWith(1)
    )
  );
  protected readonly offset = computed(() => (this.currentPage()! - 1) * this.perPage);

  private readonly searchParams = computed(() => ({
    search: this.query(),
    limit: this.perPage,
    offset: this.offset(),
    sortBy: this.sortBy(),
    sortOrder: this.sortOrder()
  }));

  protected readonly loading = signal(false);
  private readonly results = toSignal(
    toObservable(this.searchParams).pipe(
      distinctUntilChangedDeep(),
      tap(() => this.loading.set(true)),
      mergeMap(searchParams => this.configService.listConfigs$(searchParams)),
      tap(() => this.loading.set(false))
    )
  );
  protected readonly total = computed(() => this.results()?.count ?? 0);
  protected readonly configs = computed(() => this.results()?.results || []);

  constructor() {
    effect(() => (this.search = this.query()));
  }

  public scrollTop() {
    setTimeout(() => scrollToEl('search-results'));
  }

  public trackByResult(_index: number, { id }: IConfigListResult) {
    return id;
  }

  private updateQueryParams(queryParams: any = {}) {
    return this.router.navigate(['./'], {
      relativeTo: this.route,
      queryParams,
      queryParamsHandling: 'merge'
    });
  }

  protected sort(sortBy: sortByFields) {
    const sortOrder = this.sortBy() === sortBy ? (this.sortOrder() === 'asc' ? 'desc' : 'asc') : 'desc';
    return this.updateQueryParams({
      sortOrder,
      sortBy,
      page: 1
    });
  }

  protected updatePage(page: number) {
    return this.updateQueryParams({
      page
    });
  }

  protected runSearch(params = {}) {
    return this.updateQueryParams({
      query: this.search,
      page: 1,
      ...params
    });
  }

  protected showNew() {
    const instance = this.modalService.open(SettingsConfigNewConfirmComponent);
    instance.closed.subscribe(
      (id: string) =>
        id &&
        this.router.navigate(['./', id, 'edit'], {
          relativeTo: this.route
        })
    );
  }
}
