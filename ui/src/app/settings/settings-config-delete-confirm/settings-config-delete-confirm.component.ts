import { Component, inject, model, output, signal } from '@angular/core';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiConfigService } from '../../api-config.service';

@Component({
  selector: 'app-settings-config-delete-confirm',
  templateUrl: './settings-config-delete-confirm.component.html',
  styleUrls: ['./settings-config-delete-confirm.component.scss'],
  standalone: true,
  imports: [FaIconComponent]
})
export class SettingsConfigDeleteConfirmComponent {
  private readonly activeModal = inject(NgbActiveModal, { optional: true });
  private readonly configService = inject(ApiConfigService);

  public readonly id = model.required<string>();

  protected readonly closed = output<boolean>();

  protected readonly loading = signal(false);

  protected close(value = false) {
    this.closed.emit(value);
    this.activeModal?.close(value);
  }

  protected async confirm() {
    this.loading.set(true);
    await this.configService.deleteConfig(this.id());
    this.loading.set(false);
    return this.close(true);
  }
}
