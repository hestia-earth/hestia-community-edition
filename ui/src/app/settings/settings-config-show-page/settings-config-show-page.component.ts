import { Component, computed, effect, inject, signal } from '@angular/core';
import { KeyValuePipe } from '@angular/common';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';
import { map, mergeMap } from 'rxjs/operators';
import { EngineOrchestratorEditComponent } from '@hestia-earth/ui-components';
import { FaIconComponent } from '@fortawesome/angular-fontawesome';
import { faAngleDown, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { isEmpty } from '@hestia-earth/utils';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ApiConfigService, defaultConfigId } from '../../api-config.service';
import { TitleComponent } from '../../shared/title/title.component';
import { SettingsConfigDeleteConfirmComponent } from '../settings-config-delete-confirm/settings-config-delete-confirm.component';

@Component({
  selector: 'app-settings-config-show-page',
  templateUrl: './settings-config-show-page.component.html',
  styleUrls: ['./settings-config-show-page.component.scss'],
  standalone: true,
  imports: [RouterLink, KeyValuePipe, FaIconComponent, EngineOrchestratorEditComponent, TitleComponent]
})
export class SettingsConfigShowPageComponent {
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);
  private readonly modalService = inject(NgbModal);
  private readonly configService = inject(ApiConfigService);

  protected readonly faAngleDown = faAngleDown;
  protected readonly faAngleRight = faAngleRight;

  private readonly id$ = this.route.params.pipe(map(({ id }) => id as string));
  protected readonly id = toSignal(this.id$);

  protected readonly configs = toSignal(this.id$.pipe(mergeMap(id => this.configService.getConfigs(id))));
  protected readonly loading = computed(() => isEmpty(this.configs()));

  protected readonly configClosed = signal<Record<string, boolean>>({});

  protected readonly canEdit = computed(() => this.id() !== defaultConfigId);

  constructor() {
    effect(
      () => this.configClosed.set(Object.fromEntries(Object.keys(this.configs()! ?? {}).map(key => [key, false]))),
      { allowSignalWrites: true }
    );
  }

  protected toggleConfig(type: string) {
    const configClosed = { ...this.configClosed() };
    configClosed[type] = !configClosed[type];
    this.configClosed.set(configClosed);
  }

  protected showDelete() {
    const instance = this.modalService.open(SettingsConfigDeleteConfirmComponent);
    const component = instance.componentInstance as SettingsConfigDeleteConfirmComponent;
    component.id.set(this.id()!);
    instance.closed.subscribe(confirmed => confirmed && this.router.navigate(['/settings']));
  }
}
