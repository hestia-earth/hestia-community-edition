import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';

import { storageKey, versionDate, path } from './terms.model';

@Injectable({
  providedIn: 'root'
})
export class TermsGuard {
  constructor(
    private router: Router,
    private localStorage: LocalStorageService
  ) {}

  canActivate(route: ActivatedRouteSnapshot) {
    const url = route.url.slice().pop()?.path;
    const version = this.localStorage.retrieve(storageKey);
    const redirectToTerms = url !== path && (!version || version !== versionDate);
    return redirectToTerms ? this.router.parseUrl(`/${path}`) : true;
  }
}
