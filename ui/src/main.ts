import { enableProdMode, importProvidersFrom, SecurityContext } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { BrowserModule, bootstrapApplication } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, withInterceptorsFromDi, provideHttpClient, HttpClient } from '@angular/common/http';
import { provideNgxWebstorage, withNgxWebstorageConfig, withLocalStorage } from 'ngx-webstorage';
import { provideMarkdown } from 'ngx-markdown';
import { HE_API_BASE_URL, HE_CALCULATIONS_BASE_URL, HeSvgIconsModule } from '@hestia-earth/ui-components';

import { environment } from './environments/environment';

import { AppRoutingModule } from './app/app-routing.module';
import { FontawesomeModule } from './app/fontawesome/fontawesome.module';
import { AppComponent } from './app/app.component';

import { NodeServiceInterceptor } from './app/node-service.interceptor';
import { ApiVersionInterceptor } from './app/api-version.interceptor';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    // include first so its available in every component
    provideNgxWebstorage(withNgxWebstorageConfig({ prefix: 'he_ce', separator: '-' }), withLocalStorage()),
    importProvidersFrom(
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      HeSvgIconsModule,
      AppRoutingModule,
      FontawesomeModule
    ),
    provideMarkdown({
      loader: HttpClient,
      sanitize: SecurityContext.NONE
    }),
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiVersionInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: NodeServiceInterceptor,
      multi: true
    },
    {
      provide: HE_API_BASE_URL,
      useValue: environment.apiUrl
    },
    {
      provide: HE_CALCULATIONS_BASE_URL,
      useValue: environment.apiUrl
    },
    provideHttpClient(withInterceptorsFromDi()),
    provideAnimationsAsync()
  ]
}).catch(err => console.error(err));
