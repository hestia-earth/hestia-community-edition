export const readFixture = async (url: string) => {
  const content = await import(`../fixtures/${url}`);
  return 'default' in content ? content.default : content;
};
